#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS

mkdir -p build_123
cd build_123
acmSetup --sourcedir=../source AnalysisBase,21.2.123
cd ..
lsetup rucio
lsetup panda
