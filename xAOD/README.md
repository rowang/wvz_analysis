xAOD to Minitree Framework
==================================

1.Basic info

------------------------------------------
shuzhouz@umich.edu
NOTE: Code under development. Do not change release or sample tags, etc.
Otherwise could have quite some technical WARNING/ERROR.

This framework used to be based on QuickAna. Please refer : `https://gitlab.cern.ch/ziwang/quickana`
Now we have a new framework which is indepenent of QuickAna, but you can learn how to use athena tool in QuickAna.


Rel. 21.2.115, CMake based.

Support p-tag > 40xx

If you want to add new branch to minitree, please modify `source/MyAnalysis/share/MiniTree.txt`

cutflow tool is under `source/GenAnaUtilities`
pileup files are under `source/MyAnalysis/share/prw`

General info, refer to
https://twiki.cern.ch/twiki/bin/view/AtlasComputing/RootCoreToCMake


2.Structure:

---------------------------------------------
 - `./source: analysis code`
 - `./build: compilation`
 - `./run: run folder`


3.First time setup:

----------------------------------------------

 - `rm -rf build` 
 - `mkdir build`
 - `source setup.sh`

Compilation
 - `cd build
 - `make`


4.How to run

------------------------------------------------
 - `cd run`
 - `runTest.py -i input -o output -m maxEvt [-d] [-C] [-s]`
 -  -d for debug, -C for condor running, -s for files scanning
 -  For detail information, you can check run script file in `/source/MyAnalysis/scripts`
 -  Please refer to `input_345071_16d.list` to see the input format
 -  Check your results under the output path you set
 -  Trees under `data-ANALYSIS`

GoodRun list and luminosity please refer to: `https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/GoodRunListsForAnalysisRun2`



How to get new pileup files: `https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ExtendedPileupReweighting`








