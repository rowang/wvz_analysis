#include <AnaAlgorithm/AnaAlgorithmConfig.h>

#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/DiskListLocal.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/CondorDriver.h"
#include "EventLoop/LocalDriver.h"
#include "EventLoop/TorqueDriver.h"
// #include "EventLoopGrid/PrunDriver.h"
#include <EventLoop/OutputStream.h>

#include <PATInterfaces/SystematicCode.h>
#include "PATInterfaces/CorrectionCode.h"
#include "PATInterfaces/SystematicsUtil.h"

#include "TChain.h"

#include "MyAnalysis/MyxAODAnalysis.h"

#include <iostream>     // std::cout
#include <fstream>

// using namespace std;

bool cmdline(int argc, char** argv, std::map<std::string, std::string> &opts);
void usage();

int main (int argc, char **argv) {
  std::map<std::string, std::string> opts;
  if (!cmdline(argc, argv, opts)) return 0;

  std::string outDir_in = opts["outdir"];
  if (outDir_in == "") {
    std::cout << "Name of output directory required!" << std::endl;
    return 1;
  }
  //string outDir = string(getenv("WORK")) + "/workarea/outData/" + outDir_in; // Laser was here
  std::string outDir = outDir_in;

  xAOD::Init().ignore();

  SH::SampleHandler sh;

  if (opts["in"] == "") {
    std::cout << "Name of input file(s) required!" << std::endl;
    return 2;
  }

  std::vector<std::string> ins;

  //<Get input list>
  std::ifstream filelist;
  filelist.open(opts["in"]);
  if(!filelist.good()) {
    std::cout<<"ERROR: Cannot find the input filelist, now quit!"<<std::endl;
    return 2;
  }
  std::string file;
  while(!filelist.eof()) {
    getline(filelist, file);        
    if(file.size() == 0) continue; //remove the blank lines
    std::cout<<"Add file \""<<file<<"\""<<std::endl;
    if(file.substr(0, 1) == "#") continue;
    //if dcache, use dcap://dcap.aglt2.org as prefix
    if(file.find("/pnfs") != std::string::npos)
      file =  "dcache:" + file;
    // TODO: make it smarter
    if(file.find("/eos/atlas/") != std::string::npos)
      file =  "root://eosatlas/" + file;
    // else if(file.find("/eos/")!=string::npos)
      // file =  "root://eosuser/" + file;

    ins.push_back(file);
  } 

  TChain chain("CollectionTree");
  for (auto &f : ins) {
    chain.Add(f.c_str());
  }
  sh.add(SH::makeFromTChain("xAOD", chain));

  sh.setMetaString("nc_tree", "CollectionTree");

  sh.print();

  EL::Job job;
  job.sampleHandler(sh);


  EL::AnaAlgorithmConfig alg;
  alg.setType ("MyxAODAnalysis");
  alg.setName ("WVZAlg");
  // MyxAODAnalysis* alg = new MyxAODAnalysis("WVZAlg", );

  // EL::OutputStream output1("tree_output");
  // job.outputAdd (output1);

  job.outputAdd (EL::OutputStream("tree_output"));
  job.outputAdd (EL::OutputStream("hist_output"));

  // EL::OutputStream output3("cutflow");
  // job.outputAdd (output3);
  // xAOD::Init( "MyxAODAnalysis" ).ignore(); // call before opening first file


  job.useXAOD ();

  // let's initialize the algorithm to use the xAODRootAccess package
  xAOD::Init( "MyxAODAnalysis" ).ignore(); // call before opening first file

  job.algsAdd( alg );

  EL::DirectDriver driver;
  driver.submit(job, outDir);

  return 0;
}

bool cmdline(int argc, char** argv, std::map<std::string, std::string> &opts) {
  opts.clear();

  // defaults
  opts["outdir"] = "";
  opts["in"] = "";
  opts["debug"] = "0";

  for (int i=1;i<argc;i++) {

    std::string opt=argv[i];

    if (opt=="--help") {usage(); return false;}

    if (0 != opt.find("-")) {
      std::cout<<"ERROR: options start with '-'!"<<std::endl;
      return false;
    }
    opt.erase(0,1);
    if (opts.find(opt) == opts.end()) {
      std::cout<<"ERROR: invalid option '"<<opt<<"'!"<<std::endl;
      return false;
    }
    std::string nxtopt=argv[i+1];
    if (0 == nxtopt.find("-") || i+1 >= argc) {
      std::cout<<"ERROR: option '"<<opt<<"' requires value!"<<std::endl;
      return false;
    }

    opts[opt] = nxtopt;
    i++;
  }

  return true;
}

void usage()
{
  std::cout<<"USAGE: run [-option value]\n\n"
    <<"options [default]:\n\n"
    <<"-outdir (required!)\n"
    <<"-in (required!)\n"
    <<"-debug <0/1> [0]\n"
    <<std::endl;

  return;
}
