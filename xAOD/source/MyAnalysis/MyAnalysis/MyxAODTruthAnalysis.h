#ifndef MyAnalysis_MyxAODTruthAnalysis_H
#define MyAnalysis_MyxAODTruthAnalysis_H

// https://atlassoftwaredocs.web.cern.ch/AnalysisTools/ana_alg_migrate_el/
// #include <EventLoop/Algorithm.h>
#include <AnaAlgorithm/AnaAlgorithm.h>
#include "AsgTools/ToolHandle.h"
#include <EventLoop/Algorithm.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
// #include "AnalysisVar.h"
#include "MyAnalysis/AnalysisVar.h"
#include "MyAnalysis/OBJ_Base.h"


#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicCode.h"
#include "PATInterfaces/CorrectionCode.h"
#include "PATInterfaces/SystematicsUtil.h"
#include "PATInterfaces/SystematicVariation.h" 


#include <TTree.h>
#include <TH1.h>
#include <TLorentzVector.h>
#include <time.h>
#include <TMacro.h>

#include <AsgTools/StatusCode.h>


#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgAnalysisInterfaces/IPileupReweightingTool.h>
// Interface for Muon tool
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
#include <MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h>

#include <MuonAnalysisInterfaces/IMuonEfficiencyScaleFactors.h>
// Interface for Electron tool
#include <EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h>
#include <EgammaAnalysisInterfaces/IEgammaCalibrationAndSmearingTool.h>
#include <EgammaAnalysisInterfaces/IAsgElectronEfficiencyCorrectionTool.h>
// Interface for Isolation tool
#include <IsolationSelection/IIsolationSelectionTool.h>
#include <IsolationCorrections/IIsolationCorrectionTool.h>
//OR tools
#include <AssociationUtils/ToolBox.h>
#include <AssociationUtils/OverlapRemovalInit.h>


#include "AsgTools/AnaToolHandle.h"
#include <JetCalibTools/IJetCalibrationTool.h>
#include "JetCPInterfaces/ICPJetUncertaintiesTool.h"
#include "JetInterface/IJetUpdateJvt.h"
#include "JetAnalysisInterfaces/IJetJvtEfficiency.h"
#include <FTagAnalysisInterfaces/IBTaggingSelectionTool.h>
#include <FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h>


// namespace ana{
  // class QuickAna;
// }

class MyxAODTruthAnalysis : public AnalysisVar, public EL::AnaAlgorithm {
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
  public:







#ifndef __CINT__
    //  CP::MuonCalibrationAndSmearingTool *m_muonCalibrationAndSmearingTool; //! 
    //  AsgElectronLikelihoodTool* myLikelihood; //!
    // TrigConf::xAODConfigTool *configTool;//!
    // Trig::TrigDecisionTool *trigDecTool; //!
    //OverlapRemovalTool *orTool;//!
    // ORUtils::EleMuSharedTrkOverlapTool *orTool;//!
#endif // not __CINT__


    // variables that don't get filled at submission time should be
    // protected from being send from the submission node to the worker
    // node (done by the //!)
  public:
    
    time_t start, end; //!

    const xAOD::EventInfo* m_eventInfo; //!

    int m_eventCounter; //!

    // this is a standard constructor
    MyxAODTruthAnalysis (const std::string& name, ISvcLocator *pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual EL::StatusCode initialize ();
    virtual EL::StatusCode beginInputFile ();
    virtual EL::StatusCode execute ();
    virtual EL::StatusCode finalize ();


    // EL::StatusCode getTruthJet(); 
    // void fillTruthVar(); 
    // void fillRecoVar(Quad& ); 


    // void MyInitTree(std::string varlist, TFile *file1, bool isMC);
    // void MyInitTree2(std::string varlist, TFile *file1);
    // double GetFF(double FFE[5][4], double FFM[5][4], double eta, double pt, int type);
    // bool Match(TLorentzVector p1, TLorentzVector p2);
    // void MyAddVarIntoTree(TTree *tree, std::string treename);
   // bool myTrigger(xAOD::EventInfo *eventInfo, OBJ_ELECTRON eleInfo,OBJ_MUON muonInfo, bool isMC, int run, int RadNum, int type, int* Ttype);
};

#endif
