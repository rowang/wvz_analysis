//<AnalysisVar.h>
//<Aim to provide variables and objects for analysis>
//<Yusheng WU, April 2011, Ann Arbor>

#ifndef AnalysisVar_h
#define AnalysisVar_h

#include <EventLoop/Algorithm.h>
#include <AsgTools/StatusCode.h>
#include <xAODRootAccess/tools/TReturnCode.h>

#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <math.h> 
#include <string>
#include <map>
#include <vector>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TTree.h>
#include <TLorentzVector.h>

#include "MyAnalysis/AnalysisVar.h"
#include "GenAnaUtilities/CutFlowTool.h"

//<Use STD map datatypes to manage counting and histograms>
// using namespace std;

// Forward declarations


//<Use this struct for base element in cut flow, {num,error}>
typedef struct {
  double num;
  double wnum;
  double err;
} COUNT; 

//<Map type STRING:COUNT...>
typedef std::map<std::string, COUNT> MapType_Counting;
typedef std::map<std::string, std::map<std::string,COUNT> > MapType2_Counting;
typedef std::map<std::string, std::map<std::string,std::map<std::string, COUNT> > > MapType3_Counting;

//<Map type STRING:INT...>
typedef std::map<std::string, int> MapType_Int;
typedef std::map<std::string, std::map<std::string,int> > MapType2_Int;
typedef std::map<std::string, std::map<std::string, std::vector<int> > > MapType2_VInt;

typedef std::map<std::string, unsigned long> MapType_Long;
typedef std::map<std::string, std::map<std::string, unsigned long> > MapType2_Long;

typedef std::map<std::string, bool> MapType_Bool;
typedef std::map<std::string, std::map<std::string, bool> > MapType2_Bool;

//<Map type STRING:STRING...>
typedef std::map<std::string, std::string> MapType_String;
typedef std::map<std::string, std::vector<std::string> > MapType_VString;

typedef std::map<std::string, float> MapType_Float;
typedef std::map<std::string, std::map<std::string, float> > MapType2_Float;
typedef std::map<std::string, std::map<std::string, std::vector<float> > > MapType2_VFloat;

typedef std::map<std::string, std::map<std::string, std::vector<bool> > > MapType2_VBool;


//<Map type STRING:DOUBLE...>
typedef std::map<std::string, double> MapType_Double;
typedef std::map<std::string, std::vector<double> > MapType_VDouble;
typedef std::map<std::string, std::vector<double>* > MapType_VDoubleStar;
typedef std::map<std::string, std::map<std::string, double> > MapType2_Double;
typedef std::map<std::string, std::map<std::string, std::vector<double> > > MapType2_VDouble;

typedef std::map<std::string, std::map<std::string, std::pair<double,double> > > MapType2_Double2D;
typedef std::map<std::string, std::map<std::string, std::vector<std::pair<double,double> > > > MapType2_V2DDouble;

//<Map type STRING:TH1F...>
typedef std::map<std::string, TH1F*> MapType_TH1F;
typedef std::map<std::string, std::map<std::string,TH1F*> > MapType2_TH1F;
typedef std::map<std::string, std::map<std::string,std::map<std::string,TH1F*> > > MapType3_TH1F;
typedef std::map<std::string, std::map<std::string,std::map<std::string,std::map<std::string,TH1F*> > > > MapType4_TH1F;
typedef std::map<std::string, std::map<std::string,std::map<std::string,std::map<std::string,TH1D*> > > > MapType4_TH1D;
typedef std::map<std::string, std::vector<TH1F*> > MapType_VTH1F;
typedef std::map<std::string, std::map<std::string,std::vector<TH1F*> > > MapType2_VTH1F;
typedef std::map<std::string, std::map<std::string,std::map<std::string,std::vector<TH1F*> > > > MapType3_VTH1F;

//<Map type STRING:TH2F...>
typedef std::map<std::string, TH2F*> MapType_TH2F;
typedef std::map<std::string, std::map<std::string,TH2F*> > MapType2_TH2F;
typedef std::map<std::string, std::map<std::string,std::map<std::string,TH2F*> > > MapType3_TH2F;
typedef std::map<std::string, std::map<std::string,std::map<std::string,std::map<std::string, TH2F*> > > > MapType4_TH2F;
typedef std::map<std::string, std::map<std::string,std::map<std::string,std::map<std::string, TH2D*> > > > MapType4_TH2D;

//<Map type STRING:TTree...>
typedef std::map<std::string, TTree*> MapType_TTree;

//<Map type STRING:TLorentzVector...>
typedef std::map<std::string, std::map<std::string, TLorentzVector> > MapType2_TLorentzVector;
typedef std::map<std::string, std::map<std::string, std::vector<TLorentzVector> > > MapType2_VTLorentzVector;

//<Options for Variables to determine using which event weight to fill into histograms>
//<Both means both histograms and trees>
enum {InBoth,InBothNoPileup,InBothNoSF,InBothNoPileupNoSF,InTreeOnly,InHistoOnly};

namespace quadType{
  enum {
    _4mu,
    _2e2mu,
    _4e
  };
}


namespace prodType{
  enum {
    ggF,
    VBF,
    VHLep,
    VHHad   
  };
}

namespace massRange{ 
  enum {
    lowMass,
    highMass
  }; 
}


//<AnalysisVar Class>
class AnalysisVar {

  protected:        

    // just some global setting for 
    // analysis SETTING
    EL::StatusCode InitSetting(std::string setname, std::string settings);

    // set all the flag to 0
    void ClearFlags(MapType2_Int& map);
    // set all the weight to 1.0
    void ClearWeight(MapType2_Double& map);

    CutFlowTool& cutflow(std::string n = "NOMINAL", bool ini = false); 


    // TODO: remove blank space
    // put all occurance of substd::string separated by delimiter
    void InitStrVec(std::vector<std::string>& out, std::string in, std::string de=",");

    // object cut names
    void InitObjSTEP(MapType_VString& STEP, std::string obj, std::string steps);

    // TODO: replace Hist entirely with (book)
    // save the histogram obj as HistVar
    // for each histogram, those four are needed
    void InitHistVar(std::string varlist, int nbin, double xmin, double xmax, std::string cutstep="");
    void InitVVar(std::string varlist, int nbin=0, double xmin=0, double xmax=0, std::string cutstep="xAOD");
    void InitHist2DVar(std::string varlist, int nxbin, double xmin, double xmax,
        int nybin, double ymin, double ymax, std::string cutstep="");
    void InitHist2DVVar(std::string varlist, int nxbin, double xmin, double xmax,
        int nybin, double ymin, double ymax, std::string cutstep=""); 

    // read the trigger used from the varfile! 
    // and fill to TRIG_list
    // based on STEP_obj, 
    void ReadTriggers(std::string& TRIG_list, std::string varfile);

    // TODO: addCutFlow
    void CreateCountingMap();

    // register HistVar, Hist2DVar, VVar to
    // histo, etc... (TH1D/TH2D)
    void BookHistoMap(TFile *file);
    
    // init Tree
    void MyInitTree2(std::string CHN, std::string BR, TFile *file1);

    // Register the TTree var with actual TTree
    // where Branch take place
    void AddVarIntoTreeFile(TTree *tree, std::string fname, 
        std::string SYS="VVLOOSE", bool isMC=true);

    void AddVarIntoTree(TTree *tree, std::string typeStr,
        std::string varname);

    // fill HistVar, Hist2DVar, VVar, 
    // TODO:
      // the dependent on FLAG_cut is a bit sick..
      // need way out
    void FillHistograms(std::string sysname);

    void ClearVariables(MapType_Int& map) {
      clearVar(map);
    }
    void ClearVariables(MapType2_Int& map) {
      clearVar2(map);
    };
    void ClearVariables(MapType_Long& map) {
      clearVar(map);
    }
    void ClearVariables(MapType2_Long& map) {
      clearVar2(map);
    };
    void ClearVariables(MapType_Float& map) {
      clearVar(map);
    }
    void ClearVariables(MapType2_Float& map) {
      clearVar2(map);
    };
    void ClearVariables(MapType_Double& map) {
      clearVar(map);
    }
    void ClearVariables(MapType2_Double& map) {
      clearVar2(map);
    };
    void ClearVariables(MapType2_Bool& map) {
      for(const auto & it : map)
        map[it.first]["Value"] = 0;
    };


    void ClearVariables(MapType2_Double2D& map) {
      MapType2_Double2D::iterator it;
      std::pair<double, double> init (-9999.0, -9999.0);
      for(it=map.begin(); it!=map.end(); it++) {
        std::string varname = (*it).first;
        map[varname]["Value"] = init;
      }
    }
    void ClearVariables(MapType2_VDouble& map) {
      MapType2_VDouble::iterator it;
      for(it=map.begin(); it!=map.end(); it++) {
        std::string varname = (*it).first;
        map[varname]["Value"].clear();
        map[varname]["Weight"].clear();
      }
    }
    void ClearVariables(MapType2_VBool& map) {
      clearVec2(map);
    }
    void ClearVariables(MapType2_VFloat& map) {
      clearVec2(map);
    }
    void ClearVariables(MapType2_VInt& map) {
      clearVec2(map);
    }
    void ClearVariables(MapType2_VTLorentzVector& map) {
      clearVec2(map);
    }
    void ClearVariables(MapType2_TLorentzVector& map) {

      MapType2_TLorentzVector::iterator it;
      for(it=map.begin(); it!=map.end(); it++) {
        std::string varname = (*it).first;
        TLorentzVector *tlv = new TLorentzVector();
        tlv->SetPtEtaPhiM(0.,0.,0.,0.);
        map[varname]["Value"]=*tlv;
      }
    }


    // Variables

    std::map<std::string, CutFlowTool> m_CutFlowTool;



    //<Settings in the analysis>
    std::vector<std::string> SETNAME, SYSNAME;
    //<SETTING => setting name: {set1:var1, set2:var2}>
    MapType2_Int SETTING;
    //<CUTVAR => setting name: {set1:var1, set2:var2}>
    MapType2_Double CUTVAR;

    //<Define analysis channels>
    std::vector<std::string> CHN;       

    //<Define analysis steps / cut steps>
    std::vector<std::string> Background;
    std::vector<std::string> Ztype;
    std::vector<std::string> STEP_cut; // For event selection
    //MapType_VString STEP_cut; // For event selection
    MapType_VString STEP_obj; // For object selection
    MapType_String  m_TRIG_vec; // For trigger
    MapType_String  SYS_leaf;
    MapType_Double  SYS_weight;


    //<Define interested variables, to be filled in trees or histograms>
    //setting name: Name: {NBins, Xmin, Xmax, CutStep, Option,VectorOrNot}
    //NBins, Xmin, Xmax -> used to create histogram for this variable
    //CutStep -> used to decide at which step the variable will be filled in histograms,
    //positive number means this step and after, negative number means exactly at this step
    //Option -> in histogram/in tree/ using which weight...
    //VectorOrNot -> this variable shall be vector or not
    MapType2_Double VarName; 

    //<Define Cut Flow Maps>
    //1. Event Counting Map: CNT_XX
    //"setting name" : "Channel" : "CutName" : {number of events, statistic errors}
    //"setting name" : "obj" : "CutName" : {number of events, statistic errors}
    //2. PassFlag for Selection: FLAG_XX
    //"Channel/Obj" : "CutName" : 0 or 1
    //*. Use FLAG_XX_temp first to compute decision
    MapType3_Counting CNT_cut, CNT_obj; 

    //MapType2_Counting CNT_cut, CNT_obj; 
    MapType2_Int     FLAG_cut,FLAG_cut_temp; 
    //MapType2_Counting CNT_obj;  //changed by Cong 
    MapType2_Int     FLAG_obj,FLAG_obj_temp;

    //<Define the event weight map>
    //i.e. event weights can be different at different cut stages
    //setting name : CutName : (Generator Weight, Pileup Weight, Reco SF, Trigger SF, Bunch/Train Weight ...)
    //Make sure you understand exactly what each element of the array means
    MapType2_Double Evt_Weight;

    //<Define Tree Variables and Histogram Maps>        
    //Fill the variables defined in "VarName" into both trees and histograms
    //1. Tree Variable
    //"Variable" : double
    //2. Histogram Map
    //<- TH1F>
    //"SettingName" : "Channel" : "CutName" : "Variable" : TH1F*
    //<- TH1F slices>
    //"SettingName" : "Channel" : "CutName" : "Var1_Var2Bin#" : TH1F*
    //<- TH2F>
    //"SeetingName" : "Channel" : "CutName" : "Var1_Var2" : TH2F*
    //<- TH2F Helper>
    //"Var1_Var2" : "Var1"{1,xbins,xmin,xmax},"Var2"{2,ybins,ymin,ymax}
    //MapType_Double Var;

    // histogramming:
    // has binning def
    // V ones are filled with weight
    MapType4_TH1D histo;
    MapType2_Double HistVar;
    MapType2_VDouble VVar;
    MapType4_TH2D histo_2D;
    MapType2_Double2D Hist2DVar;
    // TODO: this filling not implemeneted
    MapType2_V2DDouble V2DVar;

    // TTree Vars
    MapType2_Int TreeIntVar;
    MapType2_Float TreeFltVar;
    MapType2_Double TreeDouVar;
    MapType2_Long  TreeLngVar;
    MapType2_Bool  TreeBoolVar;
    MapType2_VBool TreeBoolVVar;
    MapType2_VFloat TreeFltVVar;
    MapType2_VDouble  TreeDouVVar;
    MapType2_VInt TreeIntVVar;
    MapType2_TLorentzVector  TreeTLVVar;
    MapType2_VTLorentzVector  TreeTLVVVar;

    MapType2_VDouble helper_2D, VarSlices;

    //<Eventually, you'd like to create root files to store
    //trees of pre-selected events and histograms on disk>
    TFile *TreeFile, *TreeFile_original, *TreeFile_selection, *HistoFile;      
    MapType_TTree Tree;

  protected:
    template <typename T> 
      std::vector<T> & GetTreeVVar(std::string name) {
        if (std::is_same<T, float>::value) 
#ifdef FLAG_DEBUG
          if (TreeFltVVar.find(name) == TreeFltVVar.end()) {
            std::cerr << "non-existant TreeFltVVar name: " 
              << name
              << std::endl;
            exit(1);
          }
#endif
          return TreeFltVVar[name]["Value"];
      }

    template <typename T> 
    void fillTreeVVar(
        std::string name,
        T value) {
      GetTreeVVar<T>(name).push_back(value);
    }

    // template <typename T1, typename T2> 
    // void fillTreeVVar(
        // std::string name,
        // T2 value) {
      // GetTreeVVar<T1>(name).push_back(value);
    // }

    // template <typename T> 
      // void fillTreeVVar(
      // std::string name,
      // T value) ;
    bool m_debug;

    void clearAll() {
      ClearFlags(FLAG_cut_temp);
      ClearFlags(FLAG_cut);
      ClearWeight(Evt_Weight);

      // ClearVariables(HistVar);
      // ClearVariables(Hist2DVar);
      ClearVariables(VVar);
      ClearVariables(TreeIntVar);
      ClearVariables(TreeIntVVar);
      ClearVariables(TreeDouVar);
      ClearVariables(TreeLngVar);
      ClearVariables(TreeFltVar);
      ClearVariables(TreeBoolVar);
      ClearVariables(TreeBoolVVar);
      ClearVariables(TreeFltVVar);
      ClearVariables(TreeDouVVar);
      ClearVariables(TreeTLVVar);
      ClearVariables(TreeTLVVVar);
    }


  private:
    template <typename T> void clearVar(T& map){
      for(const auto & it : map)
        map[it.first] = -9999;
    }
    template <typename T> void clearVar2(T& map){
      for(const auto & it : map)
        map[it.first]["Value"] = -9999;
    }
    template <typename T> void clearVec(T& map){
      for(const auto & it : map)
        map[it.first].clear();
    }
    template <typename T> void clearVec2(T& map){
      for(const auto & it : map)
        map[it.first]["Value"].clear();
    }
    // TTree Vars with corresponding map
    void InitTreeVar(std::string varlist, std::string type);


};




#endif
