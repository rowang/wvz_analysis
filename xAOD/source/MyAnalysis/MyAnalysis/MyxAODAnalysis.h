#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

// https://atlassoftwaredocs.web.cern.ch/AnalysisTools/ana_alg_migrate_el/
// #include <EventLoop/Algorithm.h>
#include <AnaAlgorithm/AnaAlgorithm.h>
#include "AsgTools/ToolHandle.h"
#include <EventLoop/Algorithm.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
// #include "AnalysisVar.h"
#include "MyAnalysis/AnalysisVar.h"
#include "MyAnalysis/OBJ_Base.h"

#include "GenAnaUtilities/Utilities.h"


#include "xAODBase/IParticle.h"

#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicCode.h"
#include "PATInterfaces/CorrectionCode.h"
#include "PATInterfaces/SystematicsUtil.h"
#include "PATInterfaces/SystematicVariation.h" 


#include <TTree.h>
#include <TH1.h>
#include <TLorentzVector.h>
#include <time.h>
#include <TMacro.h>

#include <AsgTools/StatusCode.h>


#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgAnalysisInterfaces/IPileupReweightingTool.h>
// Interface for Muon tool
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
#include <MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h>

#include <MuonAnalysisInterfaces/IMuonEfficiencyScaleFactors.h>

#include "xAODEgamma/Electron.h"
// Interface for Electron tool
#include <EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h>
#include <EgammaAnalysisInterfaces/IEgammaCalibrationAndSmearingTool.h>
#include <EgammaAnalysisInterfaces/IAsgElectronEfficiencyCorrectionTool.h>
// Interface for Photon tool
#include <EgammaAnalysisInterfaces/IAsgPhotonEfficiencyCorrectionTool.h>
#include <EgammaAnalysisInterfaces/IAsgPhotonIsEMSelector.h>
#include <EgammaAnalysisInterfaces/IElectronPhotonShowerShapeFudgeTool.h>
// Interface for FSR tools
#include "FsrUtils/IFsrPhotonTool.h"
// Interface for Isolation tool
#include <IsolationSelection/IIsolationSelectionTool.h>
#include <IsolationCorrections/IIsolationCorrectionTool.h>
// Interface for MissingET tool
#include <METInterface/IMETMaker.h>
#include <METInterface/IMETSystematicsTool.h>
#include <METInterface/IMETSignificance.h>
#include <METUtilities/METHelpers.h>
//OR tools
#include <AssociationUtils/ToolBox.h>
#include <AssociationUtils/OverlapRemovalInit.h>

// Trigger tool
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigConfInterfaces/ITrigConfigTool.h"

#include "AsgTools/AnaToolHandle.h"
#include <JetCalibTools/IJetCalibrationTool.h>
#include "JetCPInterfaces/ICPJetUncertaintiesTool.h"
#include "JetInterface/IJetUpdateJvt.h"
#include "JetInterface/IJetModifier.h"
#include "JetAnalysisInterfaces/IJetJvtEfficiency.h"
#include <FTagAnalysisInterfaces/IBTaggingSelectionTool.h>
#include <FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h>

// namespace TrigConf {class ITrigConfigTool;}
// namespace Trig {
    // class TrigDecisionTool;
// }

class MyxAODAnalysis : public AnalysisVar, public EL::AnaAlgorithm {
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
  public:


    double m_gen_weight;
    double m_weight, m_PUWgt;
    double m_lumi;
    double m_xs;


    uint32_t m_run;
    unsigned int m_radNum;
    unsigned long long m_event;
    float m_ave_mu;


    time_t m_start, m_end; //!

    // xAOD::TEvent *m_event;  //!
    const xAOD::EventInfo* m_eventInfo; //!
    // count number of events
    bool m_isMC;
    bool m_passTrig;
    bool m_noTruthAlg;
    std::string m_treename;


    int m_eventCounter; //!

    float m_sumOfWeights; //!
    float m_sumOfWeightsSquared; //!

    std::vector<CP::SystematicSet> m_sysList; //!

    std::vector<std::string> m_v_hist;



    // ToolHandle<Trig::TrigDecisionTool> m_trigDec;
    asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDec;
    asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool;
    // asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDecTool;


    ToolHandle<IGoodRunsListSelectionTool> m_grl;
    std::vector<ToolHandle<CP::IPileupReweightingTool>> m_pileReweight;

    // muon
    ToolHandle<CP::IMuonSelectionTool> m_muonSelection;
    std::vector<ToolHandle<CP::IMuonCalibrationAndSmearingTool>> m_muonCalib;
    std::vector<ToolHandle<CP::IMuonEfficiencyScaleFactors>> m_muonSF;

    ToolHandle<CP::IMuonSelectionTool> m_muonSelLowpt;
    ToolHandle<CP::IMuonEfficiencyScaleFactors> m_muonLowptSF;

    // electron
    ToolHandle<IAsgElectronLikelihoodTool> m_eleSelection;
    ToolHandle<IAsgElectronLikelihoodTool> m_eleSelectionTight;
    ToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_eleCalib;
    std::vector<ToolHandle<IAsgElectronEfficiencyCorrectionTool>> m_eleSF;

    ToolHandle<IAsgElectronLikelihoodTool> m_eleSelectionFwd;
    ToolHandle<IAsgElectronEfficiencyCorrectionTool> m_eleFwdIDSF;

    // photon
    asg::AnaToolHandle<IAsgPhotonIsEMSelector> m_phoSelection;
    ToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_phoCalib;
    ToolHandle<IElectronPhotonShowerShapeFudgeTool> m_phoFudgeMC;
    std::vector<ToolHandle<IAsgPhotonEfficiencyCorrectionTool>> m_phoSF;

    // jet
    ToolHandle<IJetCalibrationTool> m_jetCalib;
    ToolHandle<ICPJetUncertaintiesTool> m_jes;
    // asg::AnaToolHandle<IJetUpdateJvt> m_jvt;
    // asg::AnaToolHandle<IJetModifier>  m_fjvt;
    ToolHandle<IJetUpdateJvt> m_jvt;
    ToolHandle<CP::IJetJvtEfficiency> m_jvtEff;
    ToolHandle<IJetModifier> m_fjvt;
    std::vector<ToolHandle<IBTaggingSelectionTool>> m_bTagging;
    std::vector<ToolHandle<IBTaggingEfficiencyTool>> m_bTaggingEff;

    // MissingET
    ToolHandle<IMETMaker> m_metMaker;
    ToolHandle<IMETSystematicsTool> m_metSys;
    ToolHandle<IMETSignificance> m_metSigni;

    // The accessor to the selection
    SG::AuxElement::Accessor<char> m_accessor;

    // isolation
    ToolHandle<CP::IIsolationSelectionTool> m_isolation;
    ToolHandle<CP::IIsolationSelectionTool> m_isolationVarRad;
    ToolHandle<CP::IIsolationCorrectionTool> m_isoCorr;

    // Fsr tool
    //ToolHandle<FSR::IFsrPhotonTool> m_fsrTool;
    asg::AnaToolHandle<FSR::IFsrPhotonTool> m_fsrTool;

    // overlap removal
    // here we use standard working point
    ORUtils::ORFlags m_orFlags;
    ORUtils::ToolBox m_toolBox;

    // variables that don't get filled at submission time should be
    // protected from being send from the submission node to the worker
    // node (done by the //!)

    std::unique_ptr<GenUtilities> m_util; // for forward declaration

    // customize stuffs
    bool m_fillIsoVar;
    std::vector<std::string> m_isolationVar;


    enum MCPeriod {
      mc16a=0, mc16d, mc16e
    };
    enum EleType {
      regular=0, fwd
    };
    int m_mcPeriod;
    
    // helper functions
    // double z0sintheta(xAOD::Electron& p);
    // double z0sintheta(xAOD::Muon& p);
    // double d0(xAOD::Electron& p);
    // double d0(xAOD::Muon& p);
    // double d0sig(xAOD::Electron& p);
    // double d0sig(xAOD::Muon& p);

    // this is a standard constructor
    MyxAODAnalysis (const std::string& name, ISvcLocator *pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual EL::StatusCode initialize ();
    virtual EL::StatusCode beginInputFile ();
    virtual EL::StatusCode execute ();
    virtual EL::StatusCode finalize ();

    EL::StatusCode preSelection(std::string sysname);
    EL::StatusCode getSFs(std::string sysname);

    void fillMuon(const xAOD::MuonContainer* container,
        std::string postfix);
    void fillLeptonIso(const xAOD::IParticle* part, 
        std::string prefix,
        std::string postfix);

    EL::StatusCode eachSys(CP::SystematicSet& );

    EL::StatusCode elSel(
        xAOD::Electron *electron, 
        std::string cfName, int type,
        std::string sysname); 

    // EL::StatusCode getTruthJet(); 


    // void MyInitTree(std::string varlist, TFile *file1, bool isMC);
    // void MyInitTree2(std::string varlist, TFile *file1);
    // double GetFF(double FFE[5][4], double FFM[5][4], double eta, double pt, int type);
    // bool Match(TLorentzVector p1, TLorentzVector p2);
    // void MyAddVarIntoTree(TTree *tree, std::string treename);
   // bool myTrigger(xAOD::EventInfo *eventInfo, OBJ_ELECTRON eleInfo,OBJ_MUON muonInfo, bool isMC, int run, int RadNum, int type, int* Ttype);
  protected:
    void checkTrigger(const int& run, const std::string &status,
    bool& passTrig, std::string trig_name,
    std::string matcher);
};

#endif
