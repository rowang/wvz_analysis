#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <stdlib.h>
#include <math.h> 
#include <string>
#include <map>
#include <vector>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TTree.h>
#include <TLorentzVector.h>
#include "PathResolver/PathResolver.h"

#include "MyAnalysis/AnalysisVar.h"
#include "AsgTools/MessageCheck.h"

#include <type_traits>


CutFlowTool& AnalysisVar::cutflow(std::string n, bool ini) {
  // if (ini) m_CutFlowTool.emplace(std::make_pair(n, CutFlowTool(n)));
  // if (ini) m_CutFlowTool.emplace(n, CutFlowTool(n));
  if (ini) m_CutFlowTool.emplace(n, n);
  return m_CutFlowTool.at(n);
}


EL::StatusCode AnalysisVar::InitSetting(std::string setname, std::string settings) {
  std::vector<std::string> vsettings;
  InitStrVec(vsettings, settings, ",");
  for(int i=0; i<(int)vsettings.size(); i++) {
    std::vector<std::string> pairs;
    InitStrVec(pairs,vsettings[i],"=");
    if(pairs.size()<2) {
      // use official Error
      std::cerr << "ERROR!! AnalysisVar: InitSetting can not parse " << vsettings[i] << std::endl;;
      // ANA_MSG_ERROR("InitSetting can not parse " << vsettings[i]);
      // std::cout<<"Error in setting: can not parse "<<vsettings[i]<<endl;
      return EL::StatusCode::FAILURE;
    }
    SETTING[setname][pairs[0]]=atoi(pairs[1].c_str());
  }
  return EL::StatusCode::SUCCESS;
}

void AnalysisVar::InitStrVec(std::vector<std::string>& out, std::string in, std::string delimiter) {
  int pos = 0, pos_pre = 0;
  while(true) {
    pos = in.find(delimiter, pos_pre);
    if(pos == -1) {
      out.push_back(in.substr(pos_pre, in.size()-pos_pre)); 
      break;
    } else {
      out.push_back(in.substr(pos_pre, pos-pos_pre));
    }
    pos_pre = pos + 1;
  }
}

void AnalysisVar::InitObjSTEP(MapType_VString& STEP, std::string obj, std::string steps) {
  std::vector<std::string> str, objstr;
  InitStrVec(str, steps, ",");
  for(int i=0; i<(int)str.size(); i++) {
    std::string objs = str[i];
    objstr.push_back(objs);
  }

  STEP[obj]=objstr;
}

void AnalysisVar::ClearFlags(MapType2_Int& map) {
  MapType2_Int::iterator it;
  for(it=map.begin(); it!=map.end(); it++) {
    std::string chn=(*it).first;
    MapType_Int::iterator it2;
    for(it2=(*it).second.begin(); it2!=(*it).second.end(); it2++) {
      std::string cut=(*it2).first;
      map[chn][cut] = 0;
    }
  }
}

void AnalysisVar::ClearWeight(MapType2_Double& map) {
  MapType2_Double::iterator it;

  MapType_Double::iterator iit;

  for(it=map.begin(); it!=map.end(); it++)
    for(iit=(*it).second.begin(); iit!=(*it).second.end(); iit ++)
      map[(*it).first][(*iit).first] = 1.0;
}

void AnalysisVar::InitHistVar(std::string varlist, int nbin, 
    double xmin, double xmax, std::string cutstep) {
  std::vector<std::string> variables;
  InitStrVec(variables, varlist, ",");

  std::vector<std::string> cuts;
  if(cutstep=="") cuts.clear();
  else if(cutstep=="All") cuts = STEP_cut;
  else if(cutstep.compare(0,1,"-")==0) {
    cutstep = cutstep.erase(0,1);
    for(int i=0; i<(int)STEP_cut.size(); i++) {
      if(cutstep != STEP_cut[i]) cuts.push_back(STEP_cut[i]);
      else if(cutstep == STEP_cut[i]) {cuts.push_back(STEP_cut[i]); break;}
    }
  }else {
    InitStrVec(cuts, cutstep, ",");
  }

  for(int i=0; i<(int)variables.size(); i++) {

    HistVar[variables[i]]["Value"]=-9999.0;
    HistVar[variables[i]]["NBins"]=nbin;
    HistVar[variables[i]]["Xmin"]=xmin;
    HistVar[variables[i]]["Xmax"]=xmax;
    //Var[variables[i]]["std::vector"]=0;

    for(int j = 0; j < (int)cuts.size(); j++) {
      HistVar[variables[i]][cuts[j]] = 1;
    }
  }

}

void AnalysisVar::InitVVar(std::string varlist, int nbin, double xmin, double xmax, std::string cutstep) {
  std::vector<std::string> variables;
  InitStrVec(variables, varlist, ",");

  std::vector<std::string> cuts;
  if(cutstep=="") cuts.clear();
  else if(cutstep=="All") cuts = STEP_cut;
  else if(cutstep.compare(0,1,"-")==0) {
    cutstep = cutstep.erase(0,1);
    for(int i=0; i<(int)STEP_cut.size(); i++) {
      if(cutstep != STEP_cut[i]) cuts.push_back(STEP_cut[i]);
      else if(cutstep == STEP_cut[i]) {cuts.push_back(STEP_cut[i]); break;}
    }
  }else {
    InitStrVec(cuts, cutstep, ",");
  }

  for(int i=0; i<(int)variables.size(); i++) {

    VVar[variables[i]]["Value"].clear();
    VVar[variables[i]]["NBins"].push_back(nbin);
    VVar[variables[i]]["Xmin"].push_back(xmin);
    VVar[variables[i]]["Xmax"].push_back(xmax);
    VVar[variables[i]]["Weight"].clear();

    for(int j=0; j<(int)cuts.size(); j++) {
      VVar[variables[i]][cuts[j]].push_back(1);
    }
  }
}

void AnalysisVar::ReadTriggers(std::string& TRIG_list, 
    std::string fname) {

  std::string varfile = PathResolverFindCalibFile(fname);
  std::ifstream file;
  // std::string varfile = PathResolverFindCalibFile("MyAnalysis/Triggers_shuzhou.txt");
  file.open(varfile.c_str(), std::ios::out);

  //FILE* fp;
  //char result_buf[1000];
  //fp = popen("find $ROOTCOREBIN/data/MyAnalysis/ -name Triggers.txt", "r");
  //fgets(result_buf, sizeof(result_buf), fp);

  //std::string varfile(result_buf);
  //size_t len = varfile.size();
  //varfile.erase(len-1);
  //ifstream file;
  //file.open(varfile.c_str(), std::ios::out);
  std::string sline;
  if (file.is_open())  {
    std::cerr << "TriggerFile Open"  << std::endl;
    //char line[500];
    while (getline(file, sline))  {
      std::string varname, type;
      std::cerr << "TriggerLine == " << sline  << std::endl;

      //file.getline (line,500);
      // string sline(line);

      if(sline.find("HLT") != std::string::npos) {
        size_t spa_pos = sline.find(" ");
        std::string hltname = sline.substr(0, spa_pos);
        std::string status = sline.substr(spa_pos);
        boost::algorithm::trim(hltname);
        boost::algorithm::trim(status);
        TRIG_list = TRIG_list+" "+hltname;
        m_TRIG_vec[hltname] = status;
      }
    }
  } else {
    std::cerr << "TriggerFile NOT Open"  << std::endl;
  }

  boost::algorithm::trim(TRIG_list);
}

void AnalysisVar::CreateCountingMap() {
  MapType_VString::iterator it;
  for(it = STEP_obj.begin(); it!=STEP_obj.end(); it++) {
    std::string obj = (*it).first;
    for(int i = 0; i < (int)(*it).second.size(); i++) {
      std::string cut = STEP_obj[obj][i];
      COUNT ini = {0., 0., 0.};
      for(int j = 0; j < (int)SYSNAME.size(); j++) {
        CNT_obj[SYSNAME[j]][obj][cut] = ini;
      }
    }
  }

  for(int i = 0; i < (int) CHN.size(); ++i) {
    std::string chn=CHN[i];
    for(int j=0; j<(int)STEP_cut.size(); j++) {
      std::string cut=STEP_cut[j];
      FLAG_cut_temp[chn][cut]=0;
      FLAG_cut[chn][cut]=0;
      COUNT ini = {0., 0., 0.};
      Evt_Weight[chn][cut]=1.0;

      for(int k=0; k<(int)SYSNAME.size(); k++) {
        CNT_cut[SYSNAME[k]][chn][cut]=ini;
      }
    }
  }
}

// Must Use for Hist2DVar
void AnalysisVar::InitHist2DVar(std::string varlist, int nxbin, double xmin, double xmax,
    int nybin, double ymin, double ymax, std::string cutstep) {
  std::vector<std::string> variables;
  InitStrVec(variables, varlist, ",");

  std::vector<std::string> cuts;
  if(cutstep=="") cuts.clear();
  else if(cutstep=="All") cuts = STEP_cut;
  else if(cutstep.compare(0,1,"-")==0) {
    cutstep = cutstep.erase(0,1);
    for(int i=0; i<(int)STEP_cut.size(); i++) {
      if(cutstep != STEP_cut[i]) cuts.push_back(STEP_cut[i]);
      else if(cutstep == STEP_cut[i]) {cuts.push_back(STEP_cut[i]); break;}
    }
  }else {
    InitStrVec(cuts, cutstep, ",");
  }

  std::pair<double, double> init (-9999.0, -9999.0);
  for(int i=0; i<(int)variables.size(); i++) {

    Hist2DVar[variables[i]]["Value"]=init;
    Hist2DVar[variables[i]]["NBins"]=std::make_pair(nxbin, nybin);
    Hist2DVar[variables[i]]["Min"]=std::make_pair(xmin, ymin);
    Hist2DVar[variables[i]]["Max"]=std::make_pair(xmax, ymax);

    for(int j=0; j<(int)cuts.size(); j++) {
      Hist2DVar[variables[i]][cuts[j]]=std::make_pair(1,1);
    }
  }
}


void AnalysisVar::InitHist2DVVar(std::string varlist, int nxbin, double xmin, double xmax,int nybin, double ymin, double ymax, std::string cutstep) {
  std::vector<std::string> variables;
  InitStrVec(variables, varlist, ",");

  std::vector<std::string> cuts;
  if(cutstep=="") cuts.clear();
  else if(cutstep=="All") cuts = STEP_cut;
  else if(cutstep.compare(0,1,"-")==0) {
    cutstep = cutstep.erase(0,1);
    for(int i=0; i<(int)STEP_cut.size(); i++) {
      if(cutstep != STEP_cut[i]) cuts.push_back(STEP_cut[i]);
      else if(cutstep == STEP_cut[i]) {cuts.push_back(STEP_cut[i]); break;}
    }
  }else {
    InitStrVec(cuts, cutstep, ",");
  }

  for(int i = 0; i < (int) variables.size(); i++) {

    V2DVar[variables[i]]["Value"].clear();
    V2DVar[variables[i]]["NBins"].push_back(std::make_pair(nxbin, nybin));
    V2DVar[variables[i]]["Min"].push_back(std::make_pair(xmin, ymin));
    V2DVar[variables[i]]["Max"].push_back(std::make_pair(xmax, ymax));

    for(int j=0; j<(int)cuts.size(); j++) {
      V2DVar[variables[i]][cuts[j]].push_back(std::make_pair(1,1));
    }
  }
}

void AnalysisVar::BookHistoMap(TFile* file) {
  for(int k = 0; k < (int)SYSNAME.size(); k++) {
    std::string sysname = SYSNAME[k];

    // if(sysname != "NOCORR" && (!SETTING["physics"]["docorr"])) continue;
    // if(sysname != "NOMINAL" && SETTING["physics"]["docorr"] && (!SETTING["physics"]["dosys"])) continue;
    // if(sysname == "NOCORR" && SETTING["physics"]["docorr"] && SETTING["physics"]["dosys"]) continue;

    for(auto it : HistVar) {
      std::string varname = it.first;
      int nbin = (int) it.second["NBins"];
      double xlow = (double)it.second["Xmin"];
      double xhigh = (double)it.second["Xmax"];

      if(nbin == 0) continue;

      for(int i = 0; i<(int)CHN.size(); i++) {
        std::string chn = CHN[i];

        for(int j = 0; j <  (int) STEP_cut.size(); j++) {
          std::string cut = STEP_cut[j];
          if( ((int) it.second[cut]) != 1) continue;

          std::string histo_name = sysname + "_" + chn + "_" + cut + "_" + varname;
          TH1D *histo_pointer = new TH1D(histo_name.c_str(),histo_name.c_str(),nbin,xlow,xhigh);
          histo[sysname][chn][cut][varname] = histo_pointer;
          histo[sysname][chn][cut][varname]->SetDirectory(file);
        }
      }
    }

    MapType2_Double2D::iterator it2D;
    for(it2D=Hist2DVar.begin(); it2D!=Hist2DVar.end(); it2D++) {
      std::string varname=(*it2D).first;

      int nxbin = int((*it2D).second["NBins"].first);
      double xlow = double((*it2D).second["Min"].first);
      double xhigh = double((*it2D).second["Max"].first);
      int nybin = int((*it2D).second["NBins"].second);
      double ylow = double((*it2D).second["Min"].second);
      double yhigh = double((*it2D).second["Max"].second);

      if(nxbin == 0) continue;

      for(int i = 0; i<(int)CHN.size(); i++) {
        std::string chn=CHN[i];

        for(int j = 0; j < (int) STEP_cut.size(); j++) {
          std::string cut = STEP_cut[j];

          std::pair<double, double> ncuts = (*it2D).second[cut];
          if(int(ncuts.first)!=1) continue;

          std::string histo_name = sysname + "_" + chn + "_" + cut + "_" + varname;
          TH2D *histo_pointer = new TH2D(histo_name.c_str(),histo_name.c_str(),nxbin,xlow,xhigh,nybin,ylow,yhigh);
          histo_2D[sysname][chn][cut][varname]=histo_pointer;
          histo_2D[sysname][chn][cut][varname]->SetDirectory(file);
        }
      }
    }

    MapType2_VDouble::iterator itv;
    for(itv=VVar.begin(); itv!=VVar.end(); itv++) {
      std::string varname=(*itv).first;
      int nbin = int((*itv).second["NBins"][0]);
      double xlow = double((*itv).second["Xmin"][0]);
      double xhigh = double((*itv).second["Xmax"][0]);

      if(nbin == 0) continue;

      for(int i=0; i<(int)CHN.size(); i++) {
        std::string chn=CHN[i];

        for(int j=0; j<(int)STEP_cut.size(); j++) {
          std::string cut=STEP_cut[j];
          if(int((*itv).second[cut].size())!=1) continue;
          if(int((*itv).second[cut][0])!=1) continue;

          std::string histo_name = sysname + "_" + chn + "_" + cut + "_" + varname;
          TH1D *histo_pointer = new TH1D(histo_name.c_str(),histo_name.c_str(),nbin,xlow,xhigh);
          histo[sysname][chn][cut][varname]=histo_pointer;
          histo[sysname][chn][cut][varname]->SetDirectory(file);
        }
      }
    }
  }
}

void AnalysisVar::MyInitTree2(std::string CHN, std::string BR, 
    TFile *file1) {
  std::vector<std::string> channel;
  std::vector<std::string> branch;
  std::string type;
  std::string branchname;
  InitStrVec(channel, CHN, ",");
  InitStrVec(branch, BR, ",");
  for(int i=0; i<(int)channel.size();i++){
    std::string name = channel[i];
    Tree[name.c_str()] = new TTree(name.c_str(), name.c_str());
    Tree[name.c_str()]->SetDirectory(file1);
    for(int j = 0; j < (int) branch.size(); ++j){
      // channel weight? + error
      std::string branchname = name + branch[j];
      type = branchname + "/" + "D";
      TreeDouVar[branchname.c_str()]["Value"] = 0.;
      Tree[name.c_str()]->Branch(
          branchname.c_str(),
          &TreeDouVar[branchname]["Value"],
          type.c_str());
    }
  }
}

void AnalysisVar::AddVarIntoTreeFile(TTree *tree, std::string fname, std::string SYS, bool isMC) {

  std::string varfile = PathResolverFindCalibFile(fname);
  std::ifstream file;
  file.open(varfile.c_str(), std::ios::out);

  //FILE* fp;
  //char result_buf[1000];
  //fp = popen("find $ROOTCOREBIN/data/MyAnalysis/ -name MiniTree.txt", "r");
  //fgets(result_buf, sizeof(result_buf), fp);

  //std::string varfile(result_buf);
  //size_t len = varfile.size();
  //varfile.erase(len-1);
  //ifstream file;
  //file.open(varfile.c_str(), std::ios::out);

  if (file.is_open())  {
    char line[256];
    while (!file.eof() )  {
      std::string varname;

      file.getline (line,100);
      std::string sline(line);

      if(sline.find("#")!=std::string::npos) continue; // use # for comment line in MiniTree.txt
      if (sline == "") continue;
      std::size_t pos = sline.find_last_of(" ");
      varname = sline.substr(pos+1);

      AddVarIntoTree(tree, sline, varname);
    }
  }
  if(isMC && SYS=="NOMINAL") {
    MapType_String::iterator it;
    for(it=SYS_leaf.begin(); it!=SYS_leaf.end(); it++) {
      std::string varname=(*it).first;
      if(SYS_leaf[varname]=="OFF") continue;
      std::string leafname = "weight_" + varname;
      std::string type = leafname+"/D";
      tree->Branch(leafname.c_str(), &SYS_weight[varname], type.c_str());
    }
  }
}

void AnalysisVar::AddVarIntoTree(
    TTree *tree, 
    std::string typeStr, 
    std::string varname) {
  std::string type;
  if(typeStr.find("Int_t")!=std::string::npos) {
    type = "I";
    //varname = typeStr.substr(9);
    type = varname+"/"+type;
    InitTreeVar(varname, "I");
    tree->Branch(varname.c_str(),&TreeIntVar[varname]["Value"], type.c_str());
  } else if(typeStr.find("Float_t")!=std::string::npos) {
    type = "F";
    //varname = typeStr.substr(9);
    type = varname+"/"+type;
    InitTreeVar(varname, "F");
    tree->Branch(varname.c_str(),&TreeFltVar[varname]["Value"], type.c_str());
  } else if(typeStr.find("Double_t")!=std::string::npos) {
    type = "D";
    //varname = typeStr.substr(9);
    type = varname+"/"+type;
    InitTreeVar(varname, "D");
    tree->Branch(varname.c_str(),&TreeDouVar[varname]["Value"], type.c_str());
  } else if(typeStr.find("Bool_t")!=std::string::npos) {
    type = "O";
    //varname = typeStr.substr(9);
    type = varname+"/"+type;
    InitTreeVar(varname, "O");
    tree->Branch(varname.c_str(),&TreeBoolVar[varname]["Value"], type.c_str());
  } else if(typeStr.find("TLorentzVector_t")!=std::string::npos) {
    type = "TLV";
    //varname = typeStr.substr(9);
    type = varname+"/"+type;
    InitTreeVar(varname, "TLV");
    //tree->Branch(varname.c_str(),&TreeTLVVar[varname]["Value"], type.c_str());
    tree->Branch(varname.c_str(),&TreeTLVVar[varname]["Value"]);
  } else if(typeStr.find("ULong")!=std::string::npos) {
    type = "l";
    //varname = typeStr.substr(9);
    type = varname+"/"+type;
    InitTreeVar(varname, "l");
    tree->Branch(varname.c_str(),&TreeLngVar[varname]["Value"], type.c_str());
  } else if(typeStr.find("vector<F>")!=std::string::npos) {
    //varname = typeStr.substr(11);
    InitTreeVar(varname, "vector<F>");
    tree->Branch(varname.c_str(), &TreeFltVVar[varname]["Value"]);
  } else if(typeStr.find("vector<D>")!=std::string::npos) {
    //varname = typeStr.substr(11);
    InitTreeVar(varname, "vector<D>");
    tree->Branch(varname.c_str(), &TreeDouVVar[varname]["Value"]);
  } else if(typeStr.find("vector<B>")!=std::string::npos) {
    //varname = typeStr.substr(11);
    InitTreeVar(varname, "vector<B>");
    tree->Branch(varname.c_str(), &TreeBoolVVar[varname]["Value"]);
  } else if(typeStr.find("vector<I>")!=std::string::npos) {
    //varname = typeStr.substr(11);
    InitTreeVar(varname, "vector<I>");
    tree->Branch(varname.c_str(), &TreeIntVVar[varname]["Value"]);
  } else if(typeStr.find("vector<TLV>")!=std::string::npos) {
    //varname = typeStr.substr(11);
    InitTreeVar(varname, "vector<TLV>");
    tree->Branch(varname.c_str(), &TreeTLVVVar[varname]["Value"]);
  }
}


void AnalysisVar::FillHistograms(std::string sysname) {
  // fill 1D (without weight)
  MapType2_Double::iterator it;
  for(it=HistVar.begin(); it!=HistVar.end(); it++) {
    std::string varname=(*it).first;

    if((*it).second["NBins"] == 0) continue;

    for(int i=0; i<(int)CHN.size(); i++) {
      std::string chn=CHN[i];
      for(int j=0; j<(int)STEP_cut.size(); j++) {
        std::string cut=STEP_cut[j];
        if(int((*it).second[cut])==1 && FLAG_cut[chn][cut]) 
          histo[sysname][chn][cut][varname]->Fill(HistVar[varname]["Value"]);
      } 
    }
  } 

  MapType2_Double2D::iterator it2D;
  for(it2D=Hist2DVar.begin(); it2D!=Hist2DVar.end(); it2D++) {
    std::string varname=(*it2D).first;

    std::pair<double, double> nbins = (*it2D).second["NBins"];
    if(nbins.first == 0) continue;

    for(int i=0; i<(int)CHN.size(); i++) {
      std::string chn=CHN[i];

      for(int j=0; j<(int)STEP_cut.size(); j++) {
        std::string cut=STEP_cut[j];
        std::pair<double, double> ncuts = (*it2D).second[cut];
        if(int(ncuts.first)==1 && FLAG_cut[chn][cut]) {
          std::pair<double, double> values = (*it2D).second["Value"];
          histo_2D[sysname][chn][cut][varname]->Fill(values.first, values.second);
        }
      }
    }
  }

  MapType2_VDouble::iterator itv;
  for(itv=VVar.begin(); itv!=VVar.end(); itv++) {
    std::string varname=(*itv).first;

    if((*itv).second["NBins"][0] == 0) continue;

    for(int i=0; i<(int)CHN.size(); i++) {
      std::string chn=CHN[i];

      for(int j=0; j<(int)STEP_cut.size(); j++) {
        std::string cut=STEP_cut[j];
        if(int((*itv).second[cut].size())!=1) continue;
        if(int((*itv).second[cut][0])==1 && FLAG_cut[chn][cut]) {
          for(int k=0; k<(int)VVar[varname]["Value"].size(); k++)
            if(VVar[varname]["Weight"].size()>0)
              histo[sysname][chn][cut][varname]->Fill(VVar[varname]["Value"][k], VVar[varname]["Weight"][k]);
            else histo[sysname][chn][cut][varname]->Fill(VVar[varname]["Value"][k]);
        }
      }
    }
  }

}

void AnalysisVar::InitTreeVar(std::string varlist, std::string type) {
  std::vector<std::string> variables;
  InitStrVec(variables, varlist, ",");

  if(type=="I") {
    for(int i=0; i<(int)variables.size(); i++) {
      TreeIntVar[variables[i]]["Value"]=-9999;
    }
  }

  if(type=="F") {
    for(int i=0; i<(int)variables.size(); i++) {
      TreeFltVar[variables[i]]["Value"]=-9999.0;
    }
  }

  if(type=="D") {
    for(int i=0; i<(int)variables.size(); i++) {
      TreeDouVar[variables[i]]["Value"]=-9999.0;
    }
  }

  if(type=="O") {
    for(int i=0; i<(int)variables.size(); i++) {
      TreeBoolVar[variables[i]]["Value"]=false;
    }
  }

  if(type=="TLV") {
    for(int i=0; i<(int)variables.size(); i++) {
      TLorentzVector *tlv = new TLorentzVector();
      tlv->SetPtEtaPhiM(0.,0.,0.,0.);
      TreeTLVVar[variables[i]]["Value"]=*tlv;
    }
  }

  if(type=="vector<TLV>") {
    for(int i=0; i<(int)variables.size(); i++) {
      TreeTLVVVar[variables[i]]["Value"].clear();
    }
  }

  if(type=="l" || type=="L") {
    for(int i=0; i<(int)variables.size(); i++) {
      TreeLngVar[variables[i]]["Value"]=-9999;
    }
  }

  if(type=="vector<B>") {
    for(int i=0; i<(int)variables.size(); i++) {
      TreeBoolVVar[variables[i]]["Value"].clear();
    }
  }

  if(type=="vector<F>") {
    for(int i=0; i<(int)variables.size(); i++) {
      TreeFltVVar[variables[i]]["Value"].clear();
    }
  }

  if(type=="vector<D>") {
    for(int i=0; i<(int)variables.size(); i++) {
      TreeDouVVar[variables[i]]["Value"].clear();
    }
  }

  if(type=="vector<I>") {
    for(int i=0; i<(int)variables.size(); i++) {
      TreeIntVVar[variables[i]]["Value"].clear();
    }
  }



}


// template <typename T>
// void AnalysisVar::fillTreeVVar(
    // std::string name,
    // T value) {
  // GetTreeVVar<T>(name).push_back(value);
// }
