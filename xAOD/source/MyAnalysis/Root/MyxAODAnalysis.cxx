#define FLAG_DEBUG

#include "MyAnalysis/MyxAODAnalysis.h"

#include <EventLoop/IWorker.h>
// #include <EventLoop/Job.h>
// #include <EventLoop/StatusCode.h>
// #include <EventLoop/Worker.h>
// #include <EventLoopAlgs/NTupleSvc.h>
// #include <EventLoopAlgs/AlgSelect.h>



#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODJet/JetContainer.h"

#include <xAODMuon/Muon.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODMuon/MuonAuxContainer.h>

#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"

#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"

#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"

#include "xAODEventInfo/EventInfo.h"

#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthVertexContainer.h"

// #include "xAODRootAccess/TStore.h"
#include <xAODBase/IParticleHelpers.h>
#include <xAODBase/IParticle.h>
#include <xAODCore/ShallowCopy.h>

#include "PileupReweighting/PileupReweightingTool.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "AssociationUtils/EleMuSharedTrkOverlapTool.h"

#include <ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h>
#include <ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h>
#include <ElectronPhotonSelectorTools/egammaPIDdefs.h>

// #include "Initialize.h"
// #include "Counting.h"

#include "FsrUtils/FsrPhotonTool.h"
#include "PathResolver/PathResolver.h"
#include "AsgTools/MessageCheck.h"

#include "TrigConfxAOD/xAODConfigTool.h"

#include "TSystem.h"
#include <TH1F.h>
#include <TH1D.h>

#include <utility>
#include <vector>

// define my own electron

class FsrComposite {
  public:
    FsrComposite(xAOD::IParticle* _part, FSR::FsrCandidate _fsr) : part(_part), fsr(_fsr) {};
    FsrComposite(const FsrComposite& other) : part(other.part), fsr(other.fsr) {};
    ~FsrComposite() { 
      // if (fsr) {
        // delete fsr;
        // fsr = 0;
      // }
    };
    xAOD::IParticle  * part;
    FSR::FsrCandidate fsr;
};

// this is needed to distribute the algorithm to the workers
// ClassImp(MyxAODAnalysis)

// namespace ana{
  // QUICK_ANA_ELECTRON_DEFINITION_MAKER ("wvz_loose", makeHZZElectronTool (args, "LooseAndBLayerLLH", 1, "FixedCutPflowLoose"))

  // QUICK_ANA_ELECTRON_DEFINITION_MAKER ("wvz_tight", makeHZZElectronTool (args, "TightLLH", 1, "FixedCutPflowLoose"))

  // QUICK_ANA_MUON_DEFINITION_MAKER     ("wvz_loose", makeHZZMuonTool (args, xAOD::Muon::Loose, 1, "FixedCutPflowLoose"))
// }

namespace AnaLocalHelper {
    template <typename T> 
      void fillTriggerVar(MapType2_Int &TreeIntVar, const T & evtInfo) {
        TreeIntVar["HLT_e26_lhtight_nod0_ivarloose"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_e26_lhtight_nod0_ivarloose_passTrig");
        TreeIntVar["HLT_e60_lhmedium_nod0"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_e60_lhmedium_nod0_passTrig");
        TreeIntVar["HLT_e140_lhloose_nod0"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_e140_lhloose_nod0_passTrig");
        TreeIntVar["HLT_e60_lhmedium_nod0"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_e60_lhmedium_nod0_passTrig");
        TreeIntVar["HLT_e140_lhloose_nod0"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_e140_lhloose_nod0_passTrig");
        TreeIntVar["HLT_e300_etcut"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_e300_etcut_passTrig");
        TreeIntVar["HLT_2e17_lhvloose_nod0_L12EM15VHI"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_2e17_lhvloose_nod0_L12EM15VHI_passTrig");
        TreeIntVar["HLT_2e24_lhvloose_nod0"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_2e24_lhvloose_nod0_passTrig");
        TreeIntVar["HLT_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH_passTrig");
        TreeIntVar["HLT_mu26_ivarmedium"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_mu26_ivarmedium_passTrig");
        TreeIntVar["HLT_mu50"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_mu50_passTrig");
        TreeIntVar["HLT_mu60_0eta105_msonly"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_mu60_0eta105_msonly_passTrig");
        TreeIntVar["HLT_2mu14"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_2mu14_passTrig");
        TreeIntVar["HLT_mu22_mu8noL1"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_mu22_mu8noL1_passTrig");
        TreeIntVar["HLT_mu22_mu8noL1_calotag_0eta010"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_mu22_mu8noL1_calotag_0eta010_passTrig");
        TreeIntVar["HLT_mu20_2mu4noL1"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_mu20_2mu4noL1_passTrig");
        TreeIntVar["HLT_3mu6"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_3mu6_passTrig");
        TreeIntVar["HLT_3mu6_msonly"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_3mu6_msonly_passTrig");
        TreeIntVar["HLT_4mu4"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_4mu4_passTrig");
        TreeIntVar["HLT_e17_lhloose_nod0_mu14"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_e17_lhloose_nod0_mu14_passTrig");
        TreeIntVar["HLT_e26_lhmedium_nod0_mu8noL1"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_e26_lhmedium_nod0_mu8noL1_passTrig");
        TreeIntVar["HLT_e7_lhmedium_nod0_mu24"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_e7_lhmedium_nod0_mu24_passTrig");
        TreeIntVar["HLT_e12_lhloose_nod0_2mu10"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_e12_lhloose_nod0_2mu10_passTrig");
        TreeIntVar["HLT_2e12_lhloose_nod0_mu10"]["Value"] = 
          evtInfo->template auxdata<bool>("HLT_2e12_lhloose_nod0_mu10_passTrig");
      }

    template <typename T> 
      void checkTrigger(const int& run, const std::string &status, 
          bool& passTrig, const T & auxCarrier, std::string trig_name, 
          std::string matcher) {
        if(run<290000) {
          if(status.find("2015")!=std::string::npos && status.find(matcher)!=std::string::npos) {
            passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);
          }
        }else if(run>290000 && run<=300287) {

          if(run==298687 && status.find("2016_298687")!=std::string::npos)
            passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);

          if(status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
              (status.find("_6ALL")!=std::string::npos || status.find("_6A_")!=std::string::npos)) {
            passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);
          }
        }else if(run>=300345&&run<=302393) {
          if(run==300540 && status.find("2016_300540")!=std::string::npos)
            passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);

          if(status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
              (status.find("_6ALL")!=std::string::npos || status.find("_6B_6C_")!=std::string::npos)) {
            passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);
          }
        }
        else if(run >= 302737 && run <= 303560){
          if( run <= 302872 && status.find("2016") != std::string::npos && status.find(matcher) != std::string::npos &&
              (status.find("_6ALL")!=std::string::npos || status.find("_6D_")!=std::string::npos||status.find("DESMALLER_302872") != std::string::npos)) {
            // D1 - D3
            passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);
          } else if (run>302872&&status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
              (status.find("_6ALL")!=std::string::npos || status.find("_6D_")!=std::string::npos||status.find("DBIGGER_302872")!=std::string::npos)) {
            // D4 - D8
            passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);
          }
        }
        else if(run>=303638&&run<=304494){
          if(status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
              (status.find("_6ALL")!=std::string::npos || status.find("_6E_6F_")!=std::string::npos)) {
            passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);
          }

        }
        else if(run>=305291&&run<=306714){
          if(run<305293&&status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
              (status.find("_6ALL")!=std::string::npos || status.find("_6G_")!=std::string::npos||status.find("GESMALLER_305293")!=std::string::npos)) {
            passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);
          }
          if(run>=305293&&status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
              (status.find("_6ALL")!=std::string::npos || status.find("_6G_")!=std::string::npos)) {
            passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);
          }

        }
        else if(run==305359||run==309314||run==309346||run==310216){
          if(status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
              (status.find("_6ALL")!=std::string::npos || status.find("_6H_")!=std::string::npos)) {
            passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);
          }

        }
        else if(run>=307124&&run<=308084){
          if(run<=307601&&status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
              (status.find("_6ALL")!=std::string::npos || status.find("_6I_")!=std::string::npos)) {
            passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);
          }
          if(run>307601&&status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
              (status.find("_6ALL")!=std::string::npos || status.find("_6I_")!=std::string::npos||status.find("IBIGGER_307601")!=std::string::npos)) {
            passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);
          }

        }
        else if((run>=308979&&run<=309166)||(run>=309311&&run<=309759)||(run>=310015&&run<=311481)){
          // J K L all mixed together
          if(status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
              (status.find("_6ALL")!=std::string::npos || status.find("_6I_")!=std::string::npos||status.find("IBIGGER_307601")!=std::string::npos)) {
            passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);
          }

        }
        else if(run>=324320&&run<=341649){
          if(status.find("2017")!=std::string::npos && status.find(matcher)!=std::string::npos &&
              (status.find("_7ALL")!=std::string::npos || status.find("_7A")!=std::string::npos)) {
            // HLT_2e17_lhvloose_nod0_L12EM15VHI was prescaled, not use
            if (!(status.find("SPECIAL1") != std::string::npos && 
                  run >= 326834 && run <= 328393)) 
              passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);
          }
        }
        // TODO: check if it fail sometimes for our trigger matching?
        else if( run >= 348885 && run <= 364292){
          if(status.find("2018")!=std::string::npos && status.find(matcher)!=std::string::npos &&
              (status.find("_8ALL")!=std::string::npos || status.find("_8A")!=std::string::npos)) {
            // 2018 trigger SF not available
            try{
              passTrig = passTrig || auxCarrier->template auxdata<bool>(trig_name);
            } catch(std::exception& e) {}
          }
        }
      }

  }

void MyxAODAnalysis::checkTrigger(const int& run, const std::string &status, 
    bool& passTrig, std::string trig_name, 
    std::string matcher) {
  if(run<290000) {
    if(status.find("2015")!=std::string::npos && status.find(matcher)!=std::string::npos) {
      passTrig = passTrig || m_trigDec->isPassed(trig_name);
    }
  }else if(run>290000 && run<=300287) {

    if(run==298687 && status.find("2016_298687")!=std::string::npos)
      passTrig = passTrig || m_trigDec->isPassed(trig_name);

    if(status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
        (status.find("_6ALL")!=std::string::npos || status.find("_6A_")!=std::string::npos)) {
      passTrig = passTrig || m_trigDec->isPassed(trig_name);
    }
  }else if(run>=300345&&run<=302393) {
    if(run==300540 && status.find("2016_300540")!=std::string::npos)
      passTrig = passTrig || m_trigDec->isPassed(trig_name);

    if(status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
        (status.find("_6ALL")!=std::string::npos || status.find("_6B_6C_")!=std::string::npos)) {
      passTrig = passTrig || m_trigDec->isPassed(trig_name);
    }
  }
  else if(run >= 302737 && run <= 303560){
    if( run <= 302872 && status.find("2016") != std::string::npos && status.find(matcher) != std::string::npos &&
        (status.find("_6ALL")!=std::string::npos || status.find("_6D_")!=std::string::npos||status.find("DESMALLER_302872") != std::string::npos)) {
      // D1 - D3
      passTrig = passTrig || m_trigDec->isPassed(trig_name);
    } else if (run>302872&&status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
        (status.find("_6ALL")!=std::string::npos || status.find("_6D_")!=std::string::npos||status.find("DBIGGER_302872")!=std::string::npos)) {
      // D4 - D8
      passTrig = passTrig || m_trigDec->isPassed(trig_name);
    }
  }
  else if(run>=303638&&run<=304494){
    if(status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
        (status.find("_6ALL")!=std::string::npos || status.find("_6E_6F_")!=std::string::npos)) {
      passTrig = passTrig || m_trigDec->isPassed(trig_name);
    }

  }
  else if(run>=305291&&run<=306714){
    if(run<305293&&status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
        (status.find("_6ALL")!=std::string::npos || status.find("_6G_")!=std::string::npos||status.find("GESMALLER_305293")!=std::string::npos)) {
      passTrig = passTrig || m_trigDec->isPassed(trig_name);
    }
    if(run>=305293&&status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
        (status.find("_6ALL")!=std::string::npos || status.find("_6G_")!=std::string::npos)) {
      passTrig = passTrig || m_trigDec->isPassed(trig_name);
    }

  }
  else if(run==305359||run==309314||run==309346||run==310216){
    if(status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
        (status.find("_6ALL")!=std::string::npos || status.find("_6H_")!=std::string::npos)) {
      passTrig = passTrig || m_trigDec->isPassed(trig_name);
    }

  }
  else if(run>=307124&&run<=308084){
    if(run<=307601&&status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
        (status.find("_6ALL")!=std::string::npos || status.find("_6I_")!=std::string::npos)) {
      passTrig = passTrig || m_trigDec->isPassed(trig_name);
    }
    if(run>307601&&status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
        (status.find("_6ALL")!=std::string::npos || status.find("_6I_")!=std::string::npos||status.find("IBIGGER_307601")!=std::string::npos)) {
      passTrig = passTrig || m_trigDec->isPassed(trig_name);
    }

  }
  else if((run>=308979&&run<=309166)||(run>=309311&&run<=309759)||(run>=310015&&run<=311481)){
    // J K L all mixed together
    if(status.find("2016")!=std::string::npos && status.find(matcher)!=std::string::npos &&
        (status.find("_6ALL")!=std::string::npos || status.find("_6I_")!=std::string::npos||status.find("IBIGGER_307601")!=std::string::npos)) {
      passTrig = passTrig || m_trigDec->isPassed(trig_name);
    }

  }
  else if(run>=324320&&run<=341649){
    if(status.find("2017")!=std::string::npos && status.find(matcher)!=std::string::npos &&
        (status.find("_7ALL")!=std::string::npos || status.find("_7A")!=std::string::npos)) {
      // HLT_2e17_lhvloose_nod0_L12EM15VHI was prescaled, not use
      if (!(status.find("SPECIAL1") != std::string::npos && 
            run >= 326834 && run <= 328393)) 
        passTrig = passTrig || m_trigDec->isPassed(trig_name);
    }
  }
  // TODO: check if it fail sometimes for our trigger matching?
  else if( run >= 348885 && run <= 364292){
    if(status.find("2018")!=std::string::npos && status.find(matcher)!=std::string::npos &&
        (status.find("_8ALL")!=std::string::npos || status.find("_8A")!=std::string::npos)) {
      // 2018 trigger SF not available
      try{
        passTrig = passTrig || m_trigDec->isPassed(trig_name);
      } catch(std::exception& e) {}
    }
  }
}



// TODO: make it even smaller
// TODO: weight variation should be looping inside together with NOMINAL.





// constructor
MyxAODAnalysis::MyxAODAnalysis(const std::string& name, 
    ISvcLocator *pSvcLocator) 
  : EL::AnaAlgorithm (name, pSvcLocator), 

  m_eventCounter(0),

  m_trigDec("Trig::TrigDecisionTool/TrigDecision", this),
  m_trigConfigTool("TrigConf::xAODConfigTool/trigConfigTool", this),

  m_grl ("GoodRunsListSelectionTool/grl", this),
  m_pileReweight({
      {"CP::PileupReweightingTool/prwTool16a", this},
      {"CP::PileupReweightingTool/prwTool16d", this},
      {"CP::PileupReweightingTool/prwTool16e", this}
      }),

  m_muonSelection("CP::MuonSelectionTool/muSeletion", this),
  m_muonCalib({ 
      {"CP::MuonCalibrationAndSmearingTool/mucalib16", this},
      {"CP::MuonCalibrationAndSmearingTool/mucalib17", this},
      {"CP::MuonCalibrationAndSmearingTool/mucalib18", this}
      }),
  m_muonSF({
      {"CP::MuonEfficiencyScaleFactors/muRecoSF", this},
      {"CP::MuonEfficiencyScaleFactors/muTTVASF", this},
      {"CP::MuonEfficiencyScaleFactors/muIsoSF", this},
      {"CP::MuonEfficiencyScaleFactors/muIsoSF_VarRad", this}
      }),

  m_muonSelLowpt("CP::MuonSelectionTool/muSelLowpt", this),
  m_muonLowptSF("CP::MuonEfficiencyScaleFactors/muRecoLowptSF", this),

  m_eleSelection("AsgElectronLikelihoodTool/ElLoose", this),
  m_eleSelectionTight("AsgElectronLikelihoodTool/ElTight", this),
  m_eleCalib("CP::EgammaCalibrationAndSmearingTool/egamcalib", this),
  m_eleSF({
      {"AsgElectronEfficiencyCorrectionTool/eleRecoSF", this},
      {"AsgElectronEfficiencyCorrectionTool/eleIDSF", this},
      {"AsgElectronEfficiencyCorrectionTool/eleIsoSF", this},
      {"AsgElectronEfficiencyCorrectionTool/eleIDTightSF", this}
      }),

  m_eleSelectionFwd("AsgForwardElectronLikelihoodTool/ElForward", this),
  m_eleFwdIDSF("AsgElectronEfficiencyCorrectionTool/elFwdIDSF", this),

  m_phoSelection("AsgPhotonIsEMSelector/phoSel", this),
  m_phoCalib("CP::EgammaCalibrationAndSmearingTool/phoCalib", this),
  m_phoFudgeMC("ElectronPhotonShowerShapeFudgeTool/fudgeMC", this),
  m_phoSF({
          {"AsgPhotonEfficiencyCorrectionTool/phoIDSF", this},
          {"AsgPhotonEfficiencyCorrectionTool/phoIsoSF", this}
          }),

  m_jetCalib("JetCalibrationTool/jetcalib", this),
  m_jes("JetUncertaintiesTool/jetuncert", this),
  m_jvt("JetVertexTaggerTool/UpdateJVT"),
  m_jvtEff("CP::JetJvtEfficiency/jvteff"),
  m_fjvt("JetForwardJvtTool/FJVT"),
  m_bTagging({ // 60, 70, 77, 85
      {"BTaggingSelectionTool/bTag60", this},
      {"BTaggingSelectionTool/bTag70", this},
      {"BTaggingSelectionTool/bTag77", this},
      {"BTaggingSelectionTool/bTag85", this},
      {"BTaggingSelectionTool/bTagCont", this}
      }),
  m_bTaggingEff({
    {"BTaggingEfficiencyTool/bTagEff60", this},
    {"BTaggingEfficiencyTool/bTagEff70", this},
    {"BTaggingEfficiencyTool/bTagEff77", this},
    {"BTaggingEfficiencyTool/bTagEff85", this},
    {"BTaggingEfficiencyTool/bTagCont", this}
    }),

  m_metMaker("met::METMaker/metmaker", this),
  m_metSys("met::METSystematicsTool/metsys", this),
  m_metSigni("met::METSignificance/metsig", this),
  m_accessor ("dummy"),

  m_isolation("CP::IsolationSelectionTool/isosel", this),
  // for gamma
  m_isoCorr("CP::IsolationCorrectionTool/isocorr", this),

  m_orFlags("OverlapRemovalTool", "selected", "overlaps"),
  m_util(std::make_unique<GenUtilities>("genUtil")), 
  m_isolationVar({ 
      "ptvarcone30_TightTTVA_pt1000",
      "ptvarcone30_TightTTVA_pt500",
      "ptcone20_TightTTVA_pt1000",
      "ptcone20_TightTTVA_pt500",
      "neflowisol20"
      })
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().

  StatusCode::enableFailure();

  declareProperty("TreeName", 
      m_treename = "physics", 
      "an obsolete tree name?");
  declareProperty("fillIsoVar", m_fillIsoVar = false, 
      "fill isolation variables");
  declareProperty("isMC", m_isMC, "isMC");
  declareProperty("noTruthAlg", m_noTruthAlg = false, "no truth alg that follows this");

  // tools
  // declareProperty("trigDec", m_trigDec,      "the trigger decision tool");
  declareProperty("grlTool", m_grl,          "the GRL tool");
  declareProperty("prwTool16a", m_pileReweight[mc16a], "the pile up reweighting tool");
  declareProperty("prwTool16d", m_pileReweight[mc16d], "the pile up reweighting tool");
  declareProperty("prwTool16e", m_pileReweight[mc16e], "the pile up reweighting tool");

  declareProperty("muSel",          m_muonSelection, "the muon selection tool");
  declareProperty("muCalib16",      m_muonCalib[0],  "the muon calibration tool 1516");
  declareProperty("muCalib17",      m_muonCalib[1],  "the muon calibration tool 17");
  declareProperty("muCalib18",      m_muonCalib[2],  "the muon calibration tool 18");
  declareProperty("muRecoSF",       m_muonSF[0],     "the muon reconstruction efficiency scale factor");
  declareProperty("muTTVASF",       m_muonSF[1],     "the muon track to vertexAssociation (TTVA) scale factor");
  declareProperty("muIsoSF",        m_muonSF[2],     "the muon isolation scale factor");
  declareProperty("muIsoSFVarRad",  m_muonSF[3],     "the muon isolation scale factor");
  declareProperty("muSelLowpt",     m_muonSelLowpt,  "the muon selection tool for low pt wp");
  declareProperty("muRecoLowptSF",  m_muonLowptSF, "the muon reconstruction efficiency scale factor for low pt wp");

  declareProperty("eleSel",      m_eleSelection,        "the electron selection tool");
  declareProperty("eleSelTight", m_eleSelectionTight,   "the electron selection tool");
  declareProperty("eleSelFwd",   m_eleSelectionFwd,     "the forward electron selection tool");
  declareProperty("eleCalib",    m_eleCalib,            "the electron calibration tool");
  declareProperty("eleRecoSF",    m_eleSF[0],           "the electron reconstruction efficiency scale factor");
  declareProperty("eleIDSF",      m_eleSF[1],           "the electron identification efficiency scale factor");
  declareProperty("eleIsoSF",     m_eleSF[2],           "the electron isolation scale factor");
  declareProperty("eleIDTightSF", m_eleSF[3],           "the electron identification(tight) efficiency scale factor");
  declareProperty("eleFwdIDSF",  m_eleFwdIDSF,          "the electron identification efficiency scale factor");

  declareProperty("phoCalib",   m_phoCalib,   "the photon calibration tool");
  declareProperty("phoFudgeMC", m_phoFudgeMC, "the photon calibration tool");
  declareProperty("phoIDSF",    m_phoSF[0],   "the photon identification efficiency scale factor");
  declareProperty("phoIsoSF",   m_phoSF[1],   "the photon isolation efficiency scale factor");

  declareProperty("jetCalib",         m_jetCalib, "the jet calibration tool");
  declareProperty("jetSmearing",      m_jes,      "the jet smearing tool");
  declareProperty("jvtTool",          m_jvt,      "Jet Vertext Tagger tool");
  declareProperty("jetJvtEfficiency", m_jvtEff,   "Jet Vertext Tagger efficiency tool");
  declareProperty("fjvtTool",         m_fjvt,     "Forward Jet Vertext Tagger tool");
  declareProperty("bTagging60",       m_bTagging[0],    "the jet btagging tool");
  declareProperty("bTagging70",       m_bTagging[1],    "the jet btagging tool");
  declareProperty("bTagging77",       m_bTagging[2],    "the jet btagging tool");
  declareProperty("bTagging85",       m_bTagging[3],    "the jet btagging tool");
  declareProperty("bTaggingCont",     m_bTagging[4],    "the jet btagging tool");
  declareProperty("bTaggingEff60",    m_bTaggingEff[0], "the jet btagging efficiency Tool");
  declareProperty("bTaggingEff70",    m_bTaggingEff[1], "the jet btagging efficiency Tool");
  declareProperty("bTaggingEff77",    m_bTaggingEff[2], "the jet btagging efficiency Tool");
  declareProperty("bTaggingEff85",    m_bTaggingEff[3], "the jet btagging efficiency Tool");
  declareProperty("bTaggingEffCont",  m_bTaggingEff[4], "the jet btagging efficiency Tool");

  declareProperty("metMaker",    m_metMaker, "the missingET maker");
  declareProperty("metSysTool",  m_metSys,   "the missingET systemactics tool");
  declareProperty("metSigni",    m_metSigni, "the missingET significance tool");

  declareProperty("isoTool",         m_isolation, "the isolation tool");
  declareProperty("isoToolVarRad",   m_isolationVarRad, "the isolation tool");
  declareProperty("isoCorrTool", m_isoCorr,   "the isolation correction tool");



}

EL::StatusCode MyxAODAnalysis::initialize() {
  ATH_CHECK( m_util->initialize());
 
  ATH_CHECK (ASG_MAKE_ANA_TOOL (m_trigConfigTool, TrigConf::xAODConfigTool));
  ATH_CHECK (m_trigConfigTool.initialize());

  ATH_CHECK (ASG_MAKE_ANA_TOOL (m_trigDec, Trig::TrigDecisionTool));
  ATH_CHECK (m_trigDec.setProperty ("ConfigTool", m_trigConfigTool.getHandle()));
  ATH_CHECK (m_trigDec.setProperty ("TrigDecisionKey", "xTrigDecision"));
  ATH_CHECK (m_trigDec.initialize ());

  // ANA_CHECK (m_trigDec.retrieve());
  ANA_CHECK (m_grl.retrieve());
  ANA_CHECK (m_pileReweight[0].retrieve());
  ANA_CHECK (m_pileReweight[1].retrieve());
  ANA_CHECK (m_pileReweight[2].retrieve());

  ANA_CHECK (m_muonSelection.retrieve());
  ANA_CHECK (m_muonCalib[0].retrieve());
  ANA_CHECK (m_muonCalib[1].retrieve());
  ANA_CHECK (m_muonCalib[2].retrieve());
  ANA_CHECK (m_muonSF[0].retrieve());
  ANA_CHECK (m_muonSF[1].retrieve());
  ANA_CHECK (m_muonSF[2].retrieve());
  ANA_CHECK (m_muonSF[3].retrieve());
  ANA_CHECK (m_muonSelLowpt.retrieve());
  ANA_CHECK (m_muonLowptSF.retrieve());

  ANA_CHECK (m_eleSelection.retrieve());
  ANA_CHECK (m_eleSelectionTight.retrieve());
  ANA_CHECK (m_eleSelectionFwd.retrieve());
  ANA_CHECK (m_eleFwdIDSF.retrieve());
  ANA_CHECK (m_eleCalib.retrieve());
  ANA_CHECK (m_eleSF[0].retrieve());
  ANA_CHECK (m_eleSF[1].retrieve());
  ANA_CHECK (m_eleSF[2].retrieve());
  ANA_CHECK (m_eleSF[3].retrieve());

  ANA_CHECK (m_phoCalib.retrieve());
  ANA_CHECK (m_phoFudgeMC.retrieve());
  ANA_CHECK (m_phoSF[0].retrieve());
  ANA_CHECK (m_phoSF[1].retrieve());
  ANA_CHECK (ASG_MAKE_ANA_TOOL (m_phoSelection, AsgPhotonIsEMSelector));
  ANA_CHECK (m_phoSelection.setProperty("isEMMask", egammaPID::PhotonTight));
  ANA_CHECK (m_phoSelection.setProperty("ConfigFile", 
              "ElectronPhotonSelectorTools/offline/20180116/PhotonIsEMTightSelectorCutDefs.conf"));
  ANA_CHECK (m_phoSelection.initialize());

  ANA_CHECK (m_isolation.retrieve());
  ANA_CHECK (m_isoCorr.retrieve());

  ANA_CHECK (m_jetCalib.retrieve());
  ANA_CHECK (m_jvt.retrieve());
  ANA_CHECK (m_jvtEff.retrieve());
  ANA_CHECK (m_fjvt.retrieve());

  ANA_CHECK (m_metMaker.retrieve());
  ANA_CHECK (m_metSys.retrieve());
  //ANA_CHECK( m_metSigni.setProperty("SoftTermParam", met::Random) );
  ANA_CHECK( m_metSigni.retrieve() );

  for (auto & tool : m_bTagging)
    ANA_CHECK (tool.retrieve());
  for (auto & tool : m_bTaggingEff)
    ANA_CHECK (tool.retrieve());

  // Do overlap removal only for muons, electrons and jets
  m_orFlags.doElectrons = true;
  m_orFlags.doMuons = true;
  m_orFlags.doJets = true;
  m_orFlags.doTaus = false;
  m_orFlags.doPhotons = false;

  ANA_MSG_DEBUG("Start flags configuration" );
  m_toolBox.msg().setLevel( msg().level() );
  ANA_CHECK( ORUtils::recommendedTools(m_orFlags, m_toolBox) );
  ANA_CHECK( m_toolBox.initialize() );

  // Configure the FSR tool
  ANA_MSG_DEBUG("Start fsr tool configuration" );
  ANA_CHECK (ASG_MAKE_ANA_TOOL (m_fsrTool, FSR::FsrPhotonTool));
  ANA_CHECK( m_fsrTool.setProperty( "egCalibToolName", m_phoCalib->name()) );
  ANA_CHECK( m_fsrTool.setProperty( "OutputLevel", msg().level()) );
  ANA_CHECK( m_fsrTool.initialize() );



  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files. You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  time(&m_start);

  ANA_MSG_INFO( "initialize() Number of events = " << evtStore()->event()->getEntries() );



  m_eventInfo = 0;
  // xAOD::TEvent* m_Event = wk()->xaodEvent();
  // EL_RETURN_CHECK( "initialize()", m_Event->retrieve( m_EventInfo, "EventInfo" ) );
  // ANA_CHECK (m_Event->retrieve(m_eventInfo, "EventInfo"));
  // m_isMC = m_eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );


  std::string TRIG_list = "";
  ReadTriggers(TRIG_list, "MyAnalysis/Triggers.txt");


  CP::SystematicSet recommendedSystematics;
  m_sysList = CP::make_systematics_vector(recommendedSystematics);
  CP::SystematicCode::enableFailure();


  ANA_MSG_DEBUG("Start output tree intialization" );
  SETNAME.push_back(m_treename);

  // TODO: docorr seems to always be true... maybe can remove it ?
  ANA_CHECK( InitSetting(m_treename, "dosys=0,doweight=1") );
  int Nsyst = 0;
  if (m_isMC) {
    if (!SETTING["physics"]["dosys"]) {
      SYSNAME.push_back("NOMINAL");
    } else {
      for (auto sysListItr : m_sysList) {
        std::cout << "name sys is : " << sysListItr.name() << std::endl;
        //if(Nsyst > 1) break;
        if(sysListItr.name()=="") SYSNAME.push_back("NOMINAL");
        else if(!m_isMC) SYSNAME.push_back(sysListItr.name());
        ++Nsyst;
      }
    }
  } else {
    SYSNAME.push_back("NOMINAL");
  }

  for (const auto &sys : SYSNAME) {
    cutflow(sys, true)
      .regFlow("mu", "Muon Object")
      .regCut("All")
      .regCut("Eta")
      .regCut("Pt")
      .regCut("LooseWP")
      .regCut("Z0")
      .regCut("D0", "D0 (WVZLoose)")
      .regCut("Iso", "Iso (WVZTight)")
      .regCut("OR_L", "OverLap (WVZLoose)").refresh()
      .regCut("OR_T", "OverLap (WVZTight)").refresh() ;

    cutflow(sys, true)
      .regFlow("muLow", "Low PT Muon Object")
      .regCut("LowPtWP")
      .regCut("Z0")
      .regCut("D0", "D0 (WVZLoose)")
      .regCut("Iso", "Iso (WVZTight)")
      .regCut("OR_L", "OverLap (WVZLoose)").refresh()
      .regCut("OR_T", "OverLap (WVZTight)").refresh() ;


    cutflow(sys, true)
      .regFlow("ele", "Electron Object")
      .regCut("All")
      .regCut("Pt")
      .regCut("OQ")
      .regCut("Eta")
      .regCut("LLH", "LooseBLayerLLH")
      .regCut("Z0")
      .regCut("D0", "D0 (WVZLoose)")
      .regCut("Iso", "Iso (WVZTight)")
      .regCut("OR_L", "OverLap (WVZLoose)").refresh()
      .regCut("OR_T", "OverLap (WVZTight)").refresh() ;

    cutflow(sys, true)
      .regFlow("eleFwd", "Forward Electron Object")
      .regCut("All")
      // .regCut("Eta")
      .regCut("Pt")
      .regCut("OQ")
      .regCut("Eta")
      .regCut("LLH", "FwdMediumLLH (WVZLoose)");
      // .regCut("Z0")
      // .regCut("D0", "D0 (WVZLoose)")
      // .regCut("Iso", "Iso (WVZTight)")
      // .regCut("OR_L", "OverLap (WVZLoose)").refresh()
      // .regCut("OR_T", "OverLap (WVZTight)").refresh() ;

    cutflow(sys, true)
      .regFlow("jet", "Jet Object")
      .regCut("All")
      .regCut("Eta")
      .regCut("Pt")
      .regCut("OverLap").refresh()
      .regCut("JVT")
      .regCut("bVeto60", "", true)
      .regCut("bVeto70", "", true)
      .regCut("bVeto77", "", true)
      .regCut("bVeto85", "", true)
      .regCut("bTag60", "", true)
      .regCut("bTag70", "", true)
      .regCut("bTag77", "", true)
      .regCut("bTag85", "", true);

    cutflow(sys, true)
      .regFlow("evt", "Event Selection")
      .regCut("All")
      .regCut("GRL")
      .regCut("PV")
      .regCut("BadBatMan")
      .regCut("EventJetCleaning")
      .regCut("Trigger")
      .regCut("ThreeLeptons")
      ;


  }

  // TODO
  // use this SYS_leaf to generate
  // extra weight branch in NOMINAL tree
  // fill to: SYS_weight

  for(int i = 0; i < (int) SYSNAME.size(); ++i) {

    std::string sys = SYSNAME[i];

    // don't create a tree of scale sys
    if (SYS_leaf.find(sys) != SYS_leaf.end()) continue;

    std::string tree_name = "tree_" + sys;

    ANA_CHECK(book( TTree(tree_name.c_str(), "output tree")));
    Tree[sys] = tree( tree_name );
    AddVarIntoTreeFile(Tree[sys], "MyAnalysis/MiniTree.txt", 
        sys, m_isMC);

    if(m_fillIsoVar) {
      for (auto& varName: m_isolationVar) {
        AddVarIntoTree(Tree[sys], "vector<F>", "v_e_" + varName);
        AddVarIntoTree(Tree[sys], "vector<F>", "v_m_" + varName);
        AddVarIntoTree(Tree[sys], "vector<F>", "v_m_" + varName + "_fsr");
      }
    }

  }

  for (const auto & p : std::vector<std::pair<std::string, std::string>> { 
      // do not use ;
      {"run",        "for MC, it's DSID. for data, it's run number."},
      {"event",      "event number"},
      {"weight",     "= xs * lumi * w_sf_jvt * w_prw * generator wgtg"},
      {"w_sf_jvt",   "jvt scale factor(per-event weight)"},
      {"w_prw",      "pile up reweighting weight"},
      {"Support",    "author: Rongkun Wang <rowang at cern.ch>"}
      }) {
    ANA_CHECK(book(TH1D (("BranchMeaning_" + p.first).c_str(), p.second.c_str(), 1, 0, 1)));
    m_v_hist.push_back("BranchMeaning_" + p.first);
  }



  if(m_isMC) {

    // ANA_MSG_DEBUG("sum of wgt: " << m_sumOfWeights);

    // file2->cd()
    // auto * h_wgt = new TH1D("sumWeight", "sum of weight", 5, 0, 5);
    // h_wgt->GetXaxis()->SetBinLabel(1, "Raw xAOD Events");
    // h_wgt->GetXaxis()->SetBinLabel(2, "Sum of Wgt.");
    // h_wgt->GetXaxis()->SetBinLabel(3, "Sum of Wgt. Sq.");

    ANA_CHECK(book(TH1D ("sumWeight", "sum of weight", 5, 0, 5)));
    hist("sumWeight")->GetXaxis()->SetBinLabel(1, "Raw xAOD Events");
    hist("sumWeight")->GetXaxis()->SetBinLabel(2, "Sum of Wgt.");
    hist("sumWeight")->GetXaxis()->SetBinLabel(3, "Sum of Wgt. Sq.");
    m_v_hist.push_back("sumWeight");
  }


  ANA_CHECK( requestBeginInputFile() );

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysis::beginInputFile() {
  ANA_MSG_INFO("beginInputFile");
  // const xAOD::EventInfo* m_eventInfo = 0;
  ANA_CHECK (evtStore()->retrieve(m_eventInfo, "EventInfo"));

  bool check_isMC = m_eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );
  if(m_isMC != check_isMC) return EL::StatusCode::FAILURE;

  if( m_isMC ) {
    if ( m_eventInfo->runNumber() == 284500 ) m_mcPeriod = mc16a;
    else if ( m_eventInfo->runNumber() == 300000 ) m_mcPeriod = mc16d;
    else if ( m_eventInfo->runNumber() == 310000 ) m_mcPeriod = mc16e; 
    m_lumi = m_pileReweight[m_mcPeriod]->GetIntegratedLumi() / (1.e3) ;
    double sumLumi = m_pileReweight[mc16a]->GetIntegratedLumi() +
          m_pileReweight[mc16d]->GetIntegratedLumi() +
          m_pileReweight[mc16e]->GetIntegratedLumi();
    double resLumi = 0.;
    switch( m_eventInfo->mcChannelNumber() ) { 
      case 343365:
      case 410503:
      case 410013:
      case 410014:
      case 410025:
        resLumi = m_pileReweight[mc16a]->GetIntegratedLumi() +
          m_pileReweight[mc16d]->GetIntegratedLumi();
        m_lumi *= sumLumi / resLumi;
        break;
      case 410011:
        ;
      case 410012:
      case 410026:
        sumLumi = m_pileReweight[mc16a]->GetIntegratedLumi() +
          m_pileReweight[mc16d]->GetIntegratedLumi() +
          m_pileReweight[mc16e]->GetIntegratedLumi();
        resLumi = m_pileReweight[mc16a]->GetIntegratedLumi();
        m_lumi *= sumLumi / resLumi;
        break;
      default:
        break;
    }
    ANA_MSG_INFO( "Integrated Lumonisity = " << m_lumi << " fb^-1" );
    m_xs = m_util->getXS(m_eventInfo->mcChannelNumber(), m_mcPeriod) * (1.e3);
    ANA_MSG_INFO( "xs is " << m_xs << " fb" );

    if (m_eventCounter == 0) {
      ANA_CHECK(book(TH1D ("lumi", "GetBinCenter(1) is lumi", 1, m_lumi - 1, m_lumi+1)));
      m_v_hist.push_back("lumi");

      ANA_CHECK(book(TH1D ("xs", "GetBinCenter(1) is cross section", 1, m_xs - 1, m_xs + 1)));
      m_v_hist.push_back("xs");
    }
  } else {
    m_lumi = 1.;
    m_xs = 1.;
  }



  m_sumOfWeights = 1.;
  m_sumOfWeightsSquared = 1.;

  // MetaData: TODO: do I need this for data?
  {
    const xAOD::CutBookkeeperContainer* bks = 0;
    ANA_CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );
    const xAOD::CutBookkeeper* all = 0; int maxCycle=-1; //need to find the max cycle where input stream is StreamAOD and the name is AllExecutedEvents
    for(auto cbk : *bks) { 
      ANA_MSG_DEBUG(cbk->name());
      if(cbk->inputStream( ) == "StreamAOD" && cbk->name() == "AllExecutedEvents" && cbk->cycle() > maxCycle) { 
        maxCycle=cbk->cycle(); all = cbk; 
      }
    }

    m_sumOfWeights = all->sumOfEventWeights(); //also have all->nAcceptedEvents() which is simple event count, and all->sumOfEventWeightsSquared()
    m_sumOfWeightsSquared = all->sumOfEventWeightsSquared();


    hist("sumWeight")->Fill(0.5, evtStore()->event()->getEntries());
    hist("sumWeight")->Fill(1.5, m_sumOfWeights);
    hist("sumWeight")->Fill(2.5, m_sumOfWeightsSquared);
    ANA_MSG_INFO("sum of wgt: " << m_sumOfWeights);
  }


  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysis::execute() {

  m_eventCounter++;

  if(m_eventCounter == 1 || m_eventCounter % 5000==0) {
    ANA_MSG_INFO( "event counter " << m_eventCounter );
  }
  
  ANA_MSG_DEBUG( "**************************************************"  );
  ANA_MSG_DEBUG( "event counter " << m_eventCounter );
  ANA_MSG_DEBUG( "**************************************************"  );

  ANA_CHECK (evtStore()->retrieve(m_eventInfo, "EventInfo"));
  ANA_MSG_DEBUG( "eventInfo: " << m_eventInfo );
  m_util->setEvtInfo(m_eventInfo);

  //  float aveIntPC = m_eventInfo->actualInteractionsPerCrossing() + m_eventInfo->averageInteractionsPerCrossing();
  m_ave_mu = m_eventInfo->averageInteractionsPerCrossing();
  // uint32_t lb = m_eventInfo->lumiBlock();
  // m_mcEvtWts.clear();
  m_gen_weight = 1.0;
  m_PUWgt = 1.0;


  m_radNum = -1;
  if(m_isMC) {
    m_run = m_eventInfo->mcChannelNumber();
    m_event = m_eventInfo->mcEventNumber();
    if(SETTING[SETNAME[0]]["doweight"] == 1) {
      // TODO: hardcoded 0 is the default !?
      m_gen_weight = 
        m_eventInfo->mcEventWeights().size()>0 ? 
        m_eventInfo->mcEventWeights()[0] : 1.0;
    }
    cutflow("NOMINAL").pass("evt", "All", m_gen_weight);
  } else {
    cutflow("NOMINAL").pass("evt", "All");
    // it's data!
    m_run = m_eventInfo->runNumber();
    m_event = m_eventInfo->eventNumber();
    if(!m_grl->passRunLB(*m_eventInfo)){
      return EL::StatusCode::SUCCESS; 
    }
    if(!(m_eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error)){
      return EL::StatusCode::SUCCESS; 
    }
    if(!(m_eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error)){
      return EL::StatusCode::SUCCESS; 
    }
    if(!(m_eventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error)){
      return EL::StatusCode::SUCCESS; 
    }
    if(! (m_eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) )){
      return EL::StatusCode::SUCCESS; 
    }
  }
  cutflow("NOMINAL").pass("evt", "GRL", m_gen_weight);

  const xAOD::VertexContainer* vertices = 0;
  ANA_CHECK (evtStore()->retrieve(vertices, "PrimaryVertices"));
  ANA_MSG_DEBUG ( "VtxContainer:" << vertices );
  m_util->setVtx(vertices);
  const xAOD::Vertex *pv(0);
  for ( const auto* const vtx_itr : *vertices ) {
    if (vtx_itr->vertexType() == xAOD::VxType::VertexType::PriVtx) { 
      pv = vtx_itr; 
      break;
    }
  }
  if(!pv) return EL::StatusCode::SUCCESS;
  cutflow("NOMINAL").pass("evt", "PV", m_gen_weight);

  // TODO: check if it's 0 or 1 that's pass in data
  if(!m_isMC){
    if(m_run <= 311481){
      if(m_eventInfo->auxdata<char>("DFCommonJets_isBadBatman")) 
        return EL::StatusCode::SUCCESS;
    }
  }
  cutflow("NOMINAL").pass("evt", "BadBatMan", m_gen_weight);

  if(!m_eventInfo->auxdata<char>("DFCommonJets_eventClean_LooseBad"))  {
    return EL::StatusCode::SUCCESS;
  }
  cutflow("NOMINAL").pass("evt", "EventJetCleaning", m_gen_weight);


  // loop over all syst
  for (auto sysListItr : m_sysList) {
    ANA_CHECK ( eachSys(sysListItr) );
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysis::finalize() {

  wk()->getOutputFile("ANALYSIS")->cd();
  for (const auto & s : m_v_hist) {
    hist(s)->Write();
  }


  for (const auto &sys : SYSNAME) 
    cutflow(sys).print(std::cout);



  printf("Finalize : %i total events have been processed !\n", m_eventCounter);
  printf("Finalize MyxAODAnalysis !");

  time(&m_end);
  double timedif = difftime(m_end, m_start);
  if(timedif > 3600) { 
    printf("Finalize : System Time: %f hours\n", timedif/3600.); 
  } else if(timedif > 60) { printf("Finalize : System Time: %f minutes\n", timedif/60.); 
  } else { 
    printf("Finalize : System Time: %f second\n", timedif); 
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysis::preSelection(std::string sysname) {

  ANA_MSG_DEBUG("presel");

  ////////////////////////////////////////////////////////////////////////////////////
  // muons
  ////////////////////////////////////////////////////////////////////////////////////
  const xAOD::MuonContainer *allMuons = nullptr;
  ANA_CHECK (evtStore()->retrieve (allMuons, "Muons"));
  auto shallowCopyM = xAOD::shallowCopyContainer (*allMuons);
  std::unique_ptr<xAOD::MuonContainer> correctedMuons (shallowCopyM.first);
  std::unique_ptr<xAOD::ShallowAuxContainer> correctedMuonsAux (shallowCopyM.second);
  // links between shallow copy and original container for MET
  ANA_CHECK( xAOD::setOriginalObjectLink( *allMuons, *correctedMuons ) );
  for (xAOD::Muon *muon : *correctedMuons) {
    muon->auxdata<char>("selected")   = 0;
    // muon->auxdata<char>("Lowpt_muon") = 0;

    cutflow(sysname).pass("mu", "All");

    int calI = -1;
    uint32_t runNum = m_run;
    if (m_isMC) runNum = m_radNum;

    // different period have different muon calibration
    if(runNum > 342000) calI = 2;
    else if(runNum > 320000 && runNum < 342000) calI = 1;
    else if(runNum < 320000) calI = 0;
    else {
      ANA_MSG_ERROR("Wrong run number!");
      return EL::StatusCode::FAILURE;
    }

    // xAOD::Muon *muon = NULL;
    ANA_MSG_DEBUG("");
    ANA_MSG_DEBUG("muon : " << muon); 
    ANA_MSG_DEBUG("muon pt before calib " << muon->p4().Pt()); 
    ANA_CHECK( m_muonCalib[calI]->applyCorrection(*muon));
    ANA_MSG_DEBUG("muon pt after calib " << muon->p4().Pt()); 
    ANA_MSG_DEBUG("muon eta " << muon->eta());


    ANA_MSG_DEBUG("type is "<< muon->muonType() << " author is " << muon->author()); 

    if(!(std::abs(muon->eta()) < 2.7)) continue;
    cutflow(sysname).pass("mu", "Eta");

    std::string cfName = "mu";
    if (muon->pt() < 3.e3) {
      continue;
    } else if ( muon->pt() < 5.e3 ) {
      ANA_MSG_DEBUG("lower muon pt check"); 
      if (!m_muonSelLowpt->accept(*muon))  continue;
      ANA_MSG_DEBUG("accepted low pt muon"); 
      muon->auxdecor<char>("Lowpt_muon") = 1;
      cutflow(sysname).pass("muLow", "LowPtWP");
      cfName = "muLow";
    } else {
      if (muon->muonType() == xAOD::Muon::CaloTagged ) {
        if (muon->author() == xAOD::Muon::CaloTag 
            || muon->author() == xAOD::Muon::CaloLikelihood) {
          if (!(muon->pt() > 15.e3)) continue; 
        } else {
          ANA_MSG_WARNING("CaloTagged, but not written by CaloTag,   skipped!");
          continue;
        }
      } 
      cutflow(sysname).pass("mu", "Pt");

      if (!m_muonSelection->accept(*muon)) continue;
      cutflow(sysname).pass("mu", "LooseWP");
    }


    // electron can use sth similar for a different WP
    // int qual = m_muonSelection->getQuality(*muon);
    // muon->auxdata<int>("Quality") = qual;

    
    double d0sigV = -999;
    double d0V    = -999;
    double z0s    = -999;

    if (muon->muonType() == xAOD::Muon::MuonStandAlone) {
      cutflow(sysname).pass(cfName, "Z0");
      cutflow(sysname).pass(cfName, "D0");
    } else {
      // if(!muon->auxdecor<char>("Lowpt_muon")) 
        // cutflow(sysname).pass("mu", "Pt");

      d0sigV = std::abs(m_util->d0sig(*muon));
      d0V    = std::abs(m_util->d0(*muon));
      z0s    = std::abs(m_util->z0sintheta(*muon));
      if (z0s < 0.5) cutflow(sysname).pass(cfName, "Z0");
      ANA_MSG_DEBUG("d0 sig: " << d0sigV);
      if (d0sigV < 3.) cutflow(sysname).pass(cfName, "D0");
    }

    muon->auxdata<double>("d0sig") = d0sigV;
    muon->auxdata<double>("d0val") = d0V;
    muon->auxdata<double>("z0s")   = z0s;
    // }


    if (m_isolation->accept(*muon)) {
      muon->auxdata<char>("passIso") = 1;
      cutflow(sysname).pass(cfName, "Iso");
    }

    if (m_isolationVarRad->accept(*muon)) {
      muon->auxdata<char>("passIsoVarRad") = 1;
    }


    // if (cutflow(sysname).isPass("mu", "Pt")) {
      // muon->auxdata<char>("WVZBaseline") = 1;
    // }

    if(cutflow(sysname).isPass(cfName, "D0")) {
      muon->auxdecor<char>("WVZLoose") = 1;
      muon->auxdata<char>("selected") = 1;
    } 

    if(cutflow(sysname).isPass(cfName, "Iso")) {
      muon->auxdecor<char>("WVZTight") = 1;
    }


    // TODO: MVA PLV


    // does OR need to consider looseMuons? yes..!
    // prepare for OR
  }

  ANA_CHECK (evtStore()->record( correctedMuons.get(), "CorrectedMuons"));
  ANA_CHECK (evtStore()->record( correctedMuonsAux.get(), "CorrectedMuonsAux"));

  ////////////////////////////////////////////////////////////////////////////////////
  // electrons
  ////////////////////////////////////////////////////////////////////////////////////
  const xAOD::ElectronContainer *allElectrons = nullptr;
  ANA_CHECK (evtStore()->retrieve (allElectrons, "Electrons"));
  auto shallowCopyE = xAOD::shallowCopyContainer (*allElectrons);
  std::unique_ptr<xAOD::ElectronContainer> correctedElectrons (shallowCopyE.first);
  std::unique_ptr<xAOD::ShallowAuxContainer> correctedElectronsAux (shallowCopyE.second);
  // links between shallow copy and original container for MET
  ANA_CHECK( xAOD::setOriginalObjectLink( *allElectrons, *correctedElectrons ) );
  for (xAOD::Electron *electron : *correctedElectrons) {
    ANA_CHECK( elSel(electron, "ele", regular, sysname));
  }

  const xAOD::ElectronContainer *allElectronsFwd = nullptr;
  ANA_CHECK (evtStore()->retrieve (allElectronsFwd, "ForwardElectrons"));
  auto shallowCopyElFwd = xAOD::shallowCopyContainer (*allElectronsFwd);
  std::unique_ptr<xAOD::ElectronContainer> correctedElectronsFwd (shallowCopyElFwd.first);
  std::unique_ptr<xAOD::ShallowAuxContainer> correctedElectronsFwdAux (shallowCopyElFwd.second);
  // links between shallow copy and original container for MET
  ANA_CHECK( xAOD::setOriginalObjectLink( *allElectronsFwd, *correctedElectronsFwd ) );
  for (xAOD::Electron *electron : *correctedElectronsFwd) { 
    ANA_CHECK( elSel(electron, "eleFwd", fwd, sysname));
  }
  
  ANA_CHECK (evtStore()->record( correctedElectrons.get(), "CorrectedElectrons"));
  ANA_CHECK (evtStore()->record( correctedElectronsAux.get(), "CorrectedElectronsAux"));

  ANA_CHECK (evtStore()->record( correctedElectronsFwd.get(), "CorrectedElectronsFwd"));
  ANA_CHECK (evtStore()->record( correctedElectronsFwdAux.get(), "CorrectedElectronsFwdAux"));


  ////////////////////////////////////////////////////////////////////////////////////
  // photon
  ////////////////////////////////////////////////////////////////////////////////////
  const xAOD::PhotonContainer *allPhotons = nullptr;
  ANA_CHECK( evtStore()->retrieve (allPhotons, "Photons") );
  auto shallowCopyP = xAOD::shallowCopyContainer (*allPhotons);
  std::unique_ptr<xAOD::PhotonContainer> correctedPhotons (shallowCopyP.first);
  std::unique_ptr<xAOD::ShallowAuxContainer> correctedPhotonsAux (shallowCopyP.second);
  // links between shallow copy and original container for MET
  ANA_CHECK( xAOD::setOriginalObjectLink(*allPhotons, *correctedPhotons) );

  // ConstDataVector<xAOD::PhotonContainer> metphotons(SG::VIEW_ELEMENTS);
  // auto metphotons = std::make_unique<ConstDataVector<xAOD::PhotonContainer>> (SG::VIEW_ELEMENTS);

  // TODO: Should we apply photon selection?
  // Currently, we have only calibration
  ANA_MSG_DEBUG("start to process photons");
  for (xAOD::Photon *photon : *correctedPhotons) {
    // TODO: What does this condition mean?
    if (photon->author() & (xAOD::EgammaParameters::AuthorPhoton + xAOD::EgammaParameters::AuthorAmbiguous)){
        ANA_CHECK( m_phoCalib->applyCorrection(*photon) );
    } else continue;

    ANA_CHECK( m_phoFudgeMC->applyCorrection(*photon) );

    ANA_CHECK( m_isoCorr->applyCorrection(*photon) );
  }

  ANA_CHECK (evtStore()->record( correctedPhotons.get(), "CorrectedPhotons"));
  ANA_CHECK (evtStore()->record( correctedPhotonsAux.get(), "CorrectedPhotonsAux"));

  ////////////////////////////////////////////////////////////////////////////////////
  // jet
  ////////////////////////////////////////////////////////////////////////////////////
  ANA_MSG_DEBUG("start to process jets");
  const xAOD::JetContainer *allJets = nullptr;
  ANA_CHECK (evtStore()->retrieve (allJets, "AntiKt4EMPFlowJets_BTagging201903"));
  auto shallowCopyJ = xAOD::shallowCopyContainer (*allJets);
  std::unique_ptr<xAOD::JetContainer> correctedJets (shallowCopyJ.first);
  std::unique_ptr<xAOD::ShallowAuxContainer> correctedJetsAux (shallowCopyJ.second);
  // auto corrJetsVec = std::make_unique<ConstDataVector<xAOD::JetContainer>> (SG::VIEW_ELEMENTS);
  for (xAOD::Jet *jet : *correctedJets) {
    jet->auxdata<char>("selected") = 0;
    ANA_CHECK( m_jetCalib->applyCalibration(*jet));
    ANA_CHECK( m_jes->applyCorrection(*jet));

    cutflow(sysname).pass("jet", "All");

    if (std::abs(jet->eta()) > 2.5) continue;
    cutflow(sysname).pass("jet", "Eta");

    if (jet->pt() < 20.e3) continue;
    cutflow(sysname).pass("jet", "Pt");


    float newjvt = m_jvt->updateJvt(*jet); 
    jet->auxdata<float>("Jvt") = newjvt;

    jet->auxdata<char>("selected") = 1;
    // corrJetsVec->push_back(jet);
  }


  ANA_CHECK (evtStore()->record( correctedJets.get(), "CorrectedJets"));
  ANA_CHECK (evtStore()->record( correctedJetsAux.get(), "CorrectedJetsAux"));

  ////////////////////////////////////////////////////////////////////////////////////
  // FSR
  ////////////////////////////////////////////////////////////////////////////////////
  ANA_MSG_DEBUG("start to process FSR");
  auto shallowCopyMCor = xAOD::shallowCopyContainer (*correctedMuons);
  std::unique_ptr<xAOD::MuonContainer> FSRRecoveredMuons (shallowCopyMCor.first);
  std::unique_ptr<xAOD::ShallowAuxContainer> FSRRecoveredMuonsAux (shallowCopyMCor.second);
  ANA_CHECK( xAOD::setOriginalObjectLink( *allMuons, *FSRRecoveredMuons ) );

  std::vector<FsrComposite> candidateVec;
  // std::vector<FSR::FsrCandidate *> candidateVecAux;

  FSR::FsrCandidate fsrCandidate;
  // apply fsr tool for all muon
  for (xAOD::Muon *muon : *FSRRecoveredMuons) {
    ANA_MSG_DEBUG( "Processing FSR Muon: " << muon );
    // TODO: do we consider putting fsr before the pt cut??
    // FSR::FsrCandidate *fsrCandidate = new FSR::FsrCandidate();
    if (muon->auxdecor<char>("selected") && CP::CorrectionCode::Ok == m_fsrTool->getFsrPhoton(muon, fsrCandidate, correctedPhotons.get(), correctedElectrons.get())) {
        ANA_MSG_DEBUG( " FSR fsrCandidate found !!!!!!!! " );
        candidateVec.emplace_back(muon, fsrCandidate);
        // candidateVecAux.push_back(fsrCandidate);
    } else {
        ANA_MSG_DEBUG( " No FSR Candidate for this muon!");
        // candidateVecAux.push_back(0);
        // candidateVec.push_back(FsrComposite(0, 0));
        continue;
    }
    ANA_MSG_VERBOSE( muon );
    ANA_MSG_VERBOSE( "muon pt: " << muon->p4().Pt() );
    ANA_MSG_VERBOSE( fsrCandidate.particle );
    ANA_MSG_VERBOSE( "fsr cand pt: " << fsrCandidate.particle->p4().Pt() );
    ANA_MSG_VERBOSE( fsrCandidate.container );
    if (fsrCandidate.container == "photon" ) {
        ANA_MSG_DEBUG( "The fsr fsrCandidate for this muon is photon" );
    } else if (fsrCandidate.container == "electron" ) {
        ANA_MSG_DEBUG( "The fsr fsrCandidate for this muon is electron" );
    }
  }

  // apply fsr tool for all ele
  {
  // for (xAOD::Electron *electron : *correctedElectrons) {
      // if (CP::CorrectionCode::Ok == m_fsrTool->getFsrPhoton(electron, fsrCandidate, correctedPhotons.get(), correctedElectrons.get())){
          // ANA_MSG_DEBUG( " FSR fsrCandidate found !!!!!!!! " );
      // } else {
          // ANA_MSG_DEBUG( " No FSR Candidate for this electron!");
          // continue;
      // }

      // ANA_MSG_DEBUG( electron );
      // if (fsrCandidate.container == "photon" ) {
          // ANA_MSG_DEBUG( "The fsr fsrCandidate for this electron is photon" );
          // // xAOD::IParticle::FourMom_t newp4 = electron->p4() + fsrCandidate.particle->p4();
          // // electron->setP4(newp4.Pt(), newp4.Eta(), newp4.Phi(), newp4.M());
          // // const xAOD::Photon* photon = dynamic_cast<const xAOD::Photon*>(fsrCandidate.particle);
          // // metphotons->push_back(dynamic_cast<const xAOD::Photon*>(fsrCandidate.particle));
          // xAOD::Photon* fsrphoton = new xAOD::Photon();
          // // FsrPhotons->push_back( fsrphoton );
          // *fsrphoton = *(dynamic_cast<const xAOD::Photon*>(fsrCandidate.particle));
          // ANA_MSG_DEBUG("    pt is " << fsrphoton->p4().Pt() << std::endl); 
      // }
      // else if (fsrCandidate.container == "electron" ) {
          // ANA_MSG_DEBUG( "The fsr fsrCandidate for this electron is electron" );
      // } else {
          // ANA_MSG_DEBUG( "The fsr fsrCandidate for this electron is unknown " << typeid(fsrCandidate.particle).name() );
      // }
  // }
  }


  // remove duplicates (just in case)
  // and find the minimum dR
  for(size_t iCand = 0; iCand < candidateVec.size(); ++iCand) {
    ANA_MSG_WARNING("check duplicate fsr cand" << iCand);
    auto& cand_1 = candidateVec[iCand];
    if (cand_1.fsr.deltaR == -1) continue;
    // if (!cand_1.fsr->particle) continue;
    for(size_t jCand = iCand + 1; jCand < candidateVec.size(); ++jCand) {
      ANA_MSG_WARNING("check duplicate fsr cand" << jCand);
      auto& cand_2 = candidateVec[jCand];
      if (cand_2.fsr.deltaR == -1) continue;
      // if (!cand_2.fsr) continue;
      // if (!cand_2.fsr->particle) continue;
      if (cand_2.fsr.particle == cand_1.fsr.particle && cand_2.part != cand_1.part) {
        ANA_MSG_WARNING("the same fsr candidate is attached to two muons");
        // if(cand_1.fsr.deltaR < cand_2.fsr.deltR) 
        if(cand_1.fsr.deltaR > cand_2.fsr.deltaR) {
          // remove it for future consideration

          cand_1.fsr = cand_2.fsr;
          cand_1.part = cand_2.part;

        }
        else {
        }

        cand_2.fsr.deltaR = -1;
      }
    }
  }



  // auto FsrPhotons = std::make_unique<xAOD::PhotonContainer>();
  // auto FsrPhotonsAux = std::make_unique<xAOD::PhotonAuxContainer>();
  // FsrPhotons->setStore (FsrPhotonsAux.get());
  
  auto metphotons = std::make_unique<ConstDataVector<xAOD::PhotonContainer>> (SG::VIEW_ELEMENTS);

  for(size_t iCand = 0; iCand < candidateVec.size(); ++iCand) {
    auto& cand_1 = candidateVec[iCand];
    if (cand_1.fsr.deltaR == -1) continue;
    // if (!cand_1.fsr) continue;
    // if (!cand_1.fsr->particle) continue;

    const auto muon = dynamic_cast<xAOD::Muon*>(cand_1.part);
    if (muon) { 
      xAOD::IParticle::FourMom_t newp4 = muon->p4() + cand_1.fsr.particle->p4();
      ANA_MSG_VERBOSE( "muon pt before fsr: " << muon->p4().Pt() );
      muon->setP4(newp4.Pt(), newp4.Eta(), newp4.Phi());
      ANA_MSG_VERBOSE( "muon pt after  fsr: " << muon->p4().Pt() );
    }
    // const xAOD::Photon* photon = dynamic_cast<const xAOD::Photon*>(fsrCandidate.particle);
    // metphotons->push_back(dynamic_cast<const xAOD::Photon*>(fsrCandidate.particle));

    if (cand_1.fsr.container == "photon" ) {
      // xAOD::Photon* FSRPhoton = new xAOD::Photon();
      // FsrPhotons->push_back( FSRPhoton );
      // *FSRPhoton = *(dynamic_cast<const xAOD::Photon*>(cand_1.fsr.particle));
      // ANA_CHECK( xAOD::setOriginalObjectLink( *xAOD::getOriginalObject(*cand_1.fsr.particle), *FSRPhoton ) );
      
      metphotons->push_back( dynamic_cast<const xAOD::Photon*>(cand_1.fsr.particle) );
      ANA_MSG_DEBUG(" fsr photon pt is " << metphotons->back()->p4().Pt() << std::endl); 
    }
  }

  // for (auto & _: candidateVec) delete _;

  // ANA_CHECK (evtStore()->record( FsrPhotons.get(), "FsrPhotons"));
  // ANA_CHECK (evtStore()->record( FsrPhotonsAux.get(), "FsrPhotonsAux"));

  // decorate the photon to the minimum energy
  // for (xAOD::Muon * muon : *FSRRecoveredMuons) {
      // if (fsrCandidate.container == "photon" ) {
          // ANA_MSG_DEBUG( "The fsr fsrCandidate for this muon is photon" );


    // metphotons.push_back(ph);
          // // muon->setP4(newp4.Pt(), newp4.Eta(), newp4.Phi());
          // // ANA_MSG_VERBOSE( "muon pt: " << muon->p4().Pt() );
      // }
      // else if (fsrCandidate.container == "electron" ) {
          // ANA_MSG_DEBUG( "The fsr fsrCandidate for this muon is electron" );
      // } else {
          // ANA_MSG_DEBUG( "The fsr fsrCandidate for this muon is unknown " << typeid(fsrCandidate.particle).name() );
      // }
  // }


  ////////////////////////////////////////////////////////////////////////////////////
  // MissingET
  ////////////////////////////////////////////////////////////////////////////////////
  // let framework handle the memory, needed for MET


  const xAOD::MissingETContainer *metcore = nullptr;
  ANA_CHECK( evtStore()->retrieve(metcore, "MET_Core_AntiKt4EMPFlow") );

  const xAOD::MissingETAssociationMap *metMap = nullptr;
  ANA_CHECK( evtStore()->retrieve(metMap, "METAssoc_AntiKt4EMPFlow") );

  ANA_MSG_DEBUG("Start process missingET");
  auto met = std::make_unique<xAOD::MissingETContainer> ();
  auto metAux = std::make_unique<xAOD::MissingETAuxContainer> ();
  met->setStore(metAux.get());

  metMap->resetObjSelectionFlags();
  m_fjvt->modify( *correctedJets );
  met::addGhostMuonsToJets( *correctedMuons, *correctedJets );

  // Rebuild muon term
  // TODO: How to select met muons and electrons?
  // Just use correctedObjects or after object selection?
  // xAOD::PhotonContainer metphotons(SG::VIEW_ELEMENTS);
  ANA_MSG_DEBUG("rebuild muon term");
  xAOD::MuonContainer metMuons(SG::VIEW_ELEMENTS);
  for (xAOD::Muon *muon : *correctedMuons) {
    if (muon->auxdecor<char>("selected") ) {
      metMuons.push_back(muon);
    }
  }
  ANA_CHECK (m_metMaker->rebuildMET("RefMuon", xAOD::Type::Muon, met.get(), &metMuons, metMap));
 
  // Rebuild electron term
  ANA_MSG_DEBUG("rebuild electron term");
  xAOD::ElectronContainer metelectrons(SG::VIEW_ELEMENTS);
  for (xAOD::Electron *el : *correctedElectrons) {
    //if (m_accessor (*el)) metelectrons.push_back(el);
    if (el->auxdecor<char>("selected")) metelectrons.push_back(el);
  }
  // should not give FwdElectron to MET
  // for (xAOD::Electron *el : *correctedElectronsFwd) {
    // //if (m_accessor (*el)) metelectrons.push_back(el);
    // if (el->auxdecor<char>("selected")) metelectrons.push_back(el);
  // }
  ANA_CHECK (m_metMaker->rebuildMET("RefEle", xAOD::Type::Electron, met.get(), &metelectrons, metMap));

  // Rebuild photon term
  ANA_MSG_DEBUG( "rebuild photon term" );
  // xAOD::PhotonContainer metphotons(SG::VIEW_ELEMENTS);
  // for (auto ph : *FsrPhotons) {
    // metphotons.push_back(ph);
  // }
  ANA_CHECK( m_metMaker->rebuildMET("RefGamma", xAOD::Type::Photon, met.get(), metphotons.get()->asDataVector(), metMap) );
  // ANA_CHECK( m_metMaker->rebuildMET("RefGamma", xAOD::Type::Photon, met.get(), &metphotons, metMap) );

  // Rebuild jet and soft term
  ANA_MSG_DEBUG("rebuild jet and soft term");
  const xAOD::JetContainer *allJetsXAOD = nullptr;
  ANA_CHECK( evtStore()->retrieve(allJetsXAOD, "AntiKt4EMPFlowJets"));
  ANA_CHECK( xAOD::setOriginalObjectLink( *allJetsXAOD, *correctedJets ) );

  std::string softTerm = "PVSoftTrk";
  ANA_CHECK (m_metMaker->rebuildJetMET("RefJet", softTerm, met.get(), correctedJets.get(), metcore, metMap, true));

  if (m_isMC && sysname != "NOMINAL") {
    if ( m_metSys->applyCorrection( *(*met.get())[softTerm] ) != 
        CP::CorrectionCode::Ok ) { 
      ANA_MSG_WARNING("GetMET: Failed to apply MET soft term systematics.");
    }
  }

  ANA_MSG_DEBUG("Rebuild final MissingET!");
  ANA_CHECK( m_metMaker->buildMETSum("Final", met.get(), (*met.get())[softTerm]->source()) );
  ANA_CHECK( m_metSigni->varianceMET(met.get(), m_eventInfo->averageInteractionsPerCrossing(), "RefJet", softTerm, "Final"));
  std::string met_signi = "met_signi_AntiKt4EMPFlow";

  m_eventInfo->auxdecor<float>(met_signi) = m_metSigni->GetSignificance();

  ANA_MSG_DEBUG( " The missing Et significance for this event is " << m_eventInfo->auxdecor<float>(met_signi) );
  for (xAOD::MissingET *MET : *met) {
    ANA_MSG_DEBUG( " The missingEt "<< MET->name() << " kinematics, Pt " << MET->met() );
  }

  ////////////////////////////////////
  // Overlap Removal
  ////////////////////////////////////
  ANA_CHECK( m_toolBox.masterTool->removeOverlaps(correctedElectrons.get(), correctedMuons.get(), correctedJets.get()) );
  for (xAOD::Electron *el : *correctedElectronsFwd) {
    if (!el->auxdecor<char>("selected")) continue;
    for (xAOD::Jet *jet : *correctedJets) {
      if (!jet->auxdecor<char>("selected") || jet->auxdata<char>("overlaps") ) continue;
      if (jet->p4().DeltaR(el->p4()) > 0.4) continue;

      ANA_MSG_DEBUG("DeltaR is " << jet->p4().DeltaR(el->p4()) );
      jet->auxdata<char>("overlaps") = 1;
    }
  }

  auto overlappedJets = std::make_unique<ConstDataVector<xAOD::JetContainer>> (SG::VIEW_ELEMENTS);
  for (const xAOD::Jet *jet : *correctedJets) {
    // Pass overlap !!
    if (!jet->auxdecor<char>("selected") || jet->auxdata<char>("overlaps") ) continue;
    overlappedJets->push_back(jet);
  }


  auto goodMuons = std::make_unique<ConstDataVector<xAOD::MuonContainer>> (SG::VIEW_ELEMENTS);
  auto goodFsrMuons = std::make_unique<ConstDataVector<xAOD::MuonContainer>> (SG::VIEW_ELEMENTS);
  auto goodElectrons = std::make_unique<ConstDataVector<xAOD::ElectronContainer>> (SG::VIEW_ELEMENTS);
  auto goodJets = std::make_unique<ConstDataVector<xAOD::JetContainer>> (SG::VIEW_ELEMENTS);


  for (const xAOD::Jet *jet : *overlappedJets) {
    // xAOD::Jet * jet = *jetObj;
    cutflow(sysname).pass("jet", "OverLap");
    ANA_MSG_DEBUG("JET jvt: " << jet->auxdata<float>("Jvt"));
    if(!m_jvtEff->passesJvtCut(*jet)) continue;
    ANA_MSG_DEBUG("JET pass jvt pt:" << jet->p4().Pt() << " " << jet->pt());

    cutflow(sysname).pass("jet", "JVT");
    jet->auxdecor<char>("passJvt") = 1;

    if(m_bTagging[0]->accept(*jet)) jet->auxdecor<char>("bTag60") = 1;
    if(m_bTagging[1]->accept(*jet)) jet->auxdecor<char>("bTag70") = 1;
    if(m_bTagging[2]->accept(*jet)) jet->auxdecor<char>("bTag77") = 1;
    if(m_bTagging[3]->accept(*jet)) jet->auxdecor<char>("bTag85") = 1;
    jet->auxdecor<int>("bTagCont") = m_bTagging[4]->getQuantile(*jet);

    goodJets->push_back(jet);
  }

  size_t iMuon = 0;
  for (xAOD::Muon *muon : *correctedMuons) {
    // Pass overlap !!
    if (!muon->auxdecor<char>("selected") || muon->auxdata<char>("overlaps") ) {
      iMuon++;
      continue;
    }

    std::string cfName = "mu";
    if (muon->auxdecor<char>("Lowpt_muon")) cfName = "muLow";

    cutflow(sysname).pass(cfName, "OR_L");
    if (muon->auxdecor<char>("WVZTight"))
      cutflow(sysname).pass(cfName, "OR_T");

    goodMuons->push_back(muon);
    goodFsrMuons->push_back((*FSRRecoveredMuons)[iMuon++]);
  }

  // for (xAOD::Muon *muon : *FsrCoveredMuons) {
    // // Pass overlap !!
    // if (!muon->auxdata<char>("selected") || muon->auxdata<char>("overlaps") ) continue;

    // // cutflow(sysname).pass("mu", "OR_L");
    // if (muon->auxdata<char>("WVZTight"))
      // // cutflow(sysname).pass("mu", "OR_T");
    // goodFsrMuons->push_back(muon);
  // }


  for (xAOD::Electron *electron : *correctedElectrons) {
    // Pass overlap !!
    if (!electron->auxdecor<char>("selected") || electron->auxdata<char>("overlaps") ) continue;

    cutflow(sysname).pass("ele", "OR_L");
    if (electron->auxdecor<char>("WVZTight"))
      cutflow(sysname).pass("ele", "OR_T");
    goodElectrons->push_back(electron);
  }

  for (xAOD::Electron *electron : *correctedElectronsFwd) {
    if (electron->auxdecor<char>("selected")) goodElectrons->push_back(electron);
  }


  // sorting lepton and jets
  std::sort(goodElectrons->begin(), goodElectrons->end(), [](const xAOD::IParticle* a, const xAOD::IParticle* b) {return a->pt() > b->pt();});
  std::sort(goodMuons->begin(),     goodMuons->end(), [](const xAOD::IParticle* a, const xAOD::IParticle* b) {return a->pt() > b->pt();});
  std::sort(goodFsrMuons->begin(),  goodFsrMuons->end(), [](const xAOD::IParticle* a, const xAOD::IParticle* b) {return a->pt() > b->pt();});
  std::sort(goodJets->begin(),      goodJets->end(), [](const xAOD::IParticle* a, const xAOD::IParticle* b) {return a->pt() > b->pt();});
  
  correctedMuons.release();
  correctedMuonsAux.release();
                                  
  correctedElectrons.release();
  correctedElectronsAux.release();
                                  
  correctedElectronsFwd.release();
  correctedElectronsFwdAux.release();
                                  
  correctedPhotons.release();
  correctedPhotonsAux.release();

  ANA_MSG_VERBOSE("released corrected photons");

  // FsrPhotons.release();
  // FsrPhotonsAux.release();

  ANA_MSG_VERBOSE("released fsr photons");

  correctedJets.release();
  correctedJetsAux.release();


  ANA_CHECK (evtStore()->record( FSRRecoveredMuons.release(), "FsrCoveredMuons"));
  ANA_CHECK (evtStore()->record( FSRRecoveredMuonsAux.release(), "FsrCoveredMuonsAux"));

  ANA_CHECK (evtStore()->record( met.release(), "MissingET"));
  ANA_CHECK (evtStore()->record( metAux.release(), "MissingETAux"));
  // ANA_CHECK (evtStore()->record( metphotons.release(), "CandidatePhotons"));

  // those are good for selection
  ANA_CHECK (evtStore()->record( goodMuons.release(),     "SelectedMuons"));
  ANA_CHECK (evtStore()->record( goodFsrMuons.release(),  "SelectedMuons_FSR"));
  ANA_CHECK (evtStore()->record( goodElectrons.release(), "SelectedElectrons"));
  ANA_CHECK (evtStore()->record( goodJets.release(),      "SelectedJets"));

  ANA_CHECK (evtStore()->record( overlappedJets.release(), "overlappedJets"));

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysis::elSel(
    xAOD::Electron *electron, 
    std::string cfName, 
    int type,
    std::string sysname 
    ) {
  // type 0 is regular electron
  // type 1 is forward electron
  
  electron->auxdata<char>("selected")     = 0;
  // electron->auxdata<char>("Fwd_LHMedium") = 0;
  // electron->auxdata<char>("LHTight") =  0;
  cutflow(sysname).pass(cfName, "All");

  // xAOD::Electron *electron = NULL;

  //TODO: why not apply calibration to low pt electrons
  if (!(electron->pt() < 4.e3)) {
    ANA_MSG_DEBUG("");
    ANA_MSG_DEBUG("electron : " << electron); 
    ANA_MSG_DEBUG("electron pt before calib " << electron->pt() << " " << electron->p4().Pt()); 
    ANA_CHECK( m_eleCalib->applyCorrection(*electron));
    ANA_MSG_DEBUG("electron pt after calib " << electron->pt() << " " << electron->p4().Pt()); 
    ANA_MSG_DEBUG("electron eta " << electron->caloCluster()->etaBE(2) << "p4 eta: " << electron->p4().Eta() );
  }

  // Add forward electron, whose eta > 2.47
  // if (!(std::abs(electron->caloCluster()->etaBE(2)) < 2.47)) 
    // continue;
  // cutflow(sysname).pass(cfName, "Eta");

  if (!(electron->pt() > 7.e3)) return EL::StatusCode::SUCCESS;
  cutflow(sysname).pass(cfName, "Pt");

  if (!((bool)electron->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON))) return EL::StatusCode::SUCCESS;
  cutflow(sysname).pass(cfName, "OQ");

  // TODO: modify for different working point
  if (type == fwd &&
      std::abs(electron->caloCluster()->etaBE(2)) > 2.5 &&
      std::abs(electron->caloCluster()->etaBE(2)) < 4.9) {
    cutflow(sysname).pass(cfName, "Eta");
    ANA_MSG_DEBUG("Fwd electron " << electron->caloCluster()->etaBE(2)); 
    electron->auxdecor<char>("Fwd_LHMedium") = 
      electron->isAvailable<char>("ForwardElectronPassLHMedium") ?
      electron->auxdata<char>("ForwardElectronPassLHMedium") :
      m_eleSelectionFwd->accept(electron);

    if (!electron->auxdecor<char>("Fwd_LHMedium")) return EL::StatusCode::SUCCESS;
    cutflow(sysname).pass(cfName, "LLH");
    ANA_MSG_DEBUG("Fwd electron pass LLH"); 
    // electron->auxdata<char>("WVZLoose") = 1;
    // electron->auxdata<char>("selected") = 1;
    // return EL::StatusCode::SUCCESS;
  } else if (type == regular && 
      std::abs(electron->caloCluster()->etaBE(2)) < 2.47) {
    cutflow(sysname).pass(cfName, "Eta");

    bool passID = electron->isAvailable<char>("DFCommonElectronsLHLooseBL") ?
      static_cast<bool>(electron->auxdata<char>("DFCommonElectronsLHLooseBL")) :
      static_cast<bool>(m_eleSelection->accept(electron));

    if (!passID) return EL::StatusCode::SUCCESS;
    cutflow(sysname).pass(cfName, "LLH");

    electron->auxdecor<char>("LHTight") = 
      electron->isAvailable<char>("DFCommonElectronsLHTight") ?
      electron->auxdata<char>("DFCommonElectronsLHTight") :
      m_eleSelectionTight->accept(electron);
  } else {
    return EL::StatusCode::SUCCESS;
  }



  if (type == regular) {
    double d0sigV = -999;
    double d0V    = -999;
    double z0s    = -999;

    d0sigV = std::abs(m_util->d0sig(*electron));
    d0V    = std::abs(m_util->d0(*electron));
    z0s    = std::abs(m_util->z0sintheta(*electron));

    if (!(z0s < 0.5))
      return EL::StatusCode::SUCCESS;
    cutflow(sysname).pass(cfName, "Z0");

    if (!(d0sigV < 5.)) {
      return EL::StatusCode::SUCCESS;
    }
    cutflow(sysname).pass(cfName, "D0");

    electron->auxdata<double>("d0sig") = d0sigV;
    electron->auxdata<double>("d0val") = d0V;
    electron->auxdata<double>("z0s")   = z0s;

    // TODO: MVA PLV
    // TODO: charge misID
    electron->auxdecor<char>("ambiguous") = electron->author() & xAOD::EgammaParameters::AuthorAmbiguous;

    if ((bool)m_isolation->accept(*electron)) {
      electron->auxdata<char>("passIso") = 1;
      cutflow(sysname).pass(cfName, "Iso");
    }

    if (cutflow(sysname).isPass(cfName, "D0")) {
      electron->auxdecor<char>("WVZLoose") = 1;
      electron->auxdata<char>("selected") = 1;
    }

    if (cutflow(sysname).isPass(cfName, "Iso")) {
      electron->auxdecor<char>("WVZTight") = 1;
    }

  } else if (type == fwd && cutflow(sysname).isPass(cfName, "LLH") ) {
    electron->auxdecor<char>("WVZLoose") = 1;
    electron->auxdecor<char>("WVZTight") = 1;
    electron->auxdata<char>("passIso") = 1;
    electron->auxdata<char>("selected") = 1;
  }
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysis::getSFs(std::string sysname) {
  ANA_MSG_DEBUG("getSF");
  // do applysystematics here for all tools

  const xAOD::MuonContainer     *Muons = nullptr;
  ANA_CHECK (evtStore()->retrieve (Muons, "SelectedMuons"));
  for (const xAOD::Muon *muon: *Muons) {

    // TODO: not needed for future derivations, 
    // now use inclusive isolation SF
    // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisGuidelinesMC16#How_to_setup_a_tool_instance_AN1
    muon->auxdecor<float>("dRJet") = -2;
    // ANA_MSG_DEBUG("Muon");
    // if (!muon->auxdata<char>("WVZBaseline")) continue;
    // muon scale factor
    float muon_weight = 1.;
    float muon_iso_weight = 1.;
    float reco_weight=1., ttva_weight=1., iso_weight = 1.;

    // Reconstruction efficiency scale factor
    if (muon->auxdecor<char>("Lowpt_muon") && 
        m_muonLowptSF->getEfficiencyScaleFactor(*muon, reco_weight) == 
        CP::CorrectionCode::Ok) { 
      muon_weight *= reco_weight;
    }
    else if (!muon->auxdecor<char>("Lowpt_muon") &&
        m_muonSF[0]->getEfficiencyScaleFactor(*muon, reco_weight) == 
        CP::CorrectionCode::Ok) {
      muon_weight *= reco_weight;
    }
    // Track to vertex association efficiency scale factor
    if (m_muonSF[1]->getEfficiencyScaleFactor(*muon, ttva_weight) == 
        CP::CorrectionCode::Ok) {
      muon_weight *= ttva_weight; 
    }

    muon->auxdecor<float>("weight_loose") = muon_weight;

    // Isolation efficiency scale factor
    if (m_muonSF[2]->getEfficiencyScaleFactor(*muon, iso_weight) == 
        CP::CorrectionCode::Ok
        && (bool) muon->auxdecor<char>("passIso")
        ) {
      muon_iso_weight = iso_weight;
    }
    muon->auxdecor<float>("weight_iso") = muon_iso_weight;
    muon_iso_weight = 1.;

    if (!muon->auxdecor<char>("passIso"))
      ANA_MSG_DEBUG("CHECK ISO muon:" << iso_weight);

    if (m_muonSF[3]->getEfficiencyScaleFactor(*muon, iso_weight) == 
        CP::CorrectionCode::Ok
        && (bool) muon->auxdecor<char>("passIsoVarRad")
        ) {
      muon_iso_weight = iso_weight;
    }
    muon->auxdecor<float>("weight_iso_varRad") = muon_iso_weight;

  }

  const xAOD::ElectronContainer *Electrons = nullptr;
  ANA_CHECK (evtStore()->retrieve (Electrons, "SelectedElectrons"));
  for (const xAOD::Electron *electron: *Electrons) {
    // ANA_MSG_DEBUG("Ele");
    // if (!electron->auxdata<char>("WVZBaseline")) continue;
    float ele_weight = 1.;
    double reco_weight = 1., id_weight = 1., iso_weight = 1., id_tight_weight = 1.;

    // for forward electron, only ID SF
    // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/LatestRecommendationsElectronIDRun2#Forward_electrons_2_5_4_9 as of 01082020
    if (electron->auxdecor<char>("Fwd_LHMedium")){
      if (m_eleFwdIDSF->getEfficiencyScaleFactor(*electron, id_weight) 
          == CP::CorrectionCode::Ok)
        ele_weight *= id_weight;
      // only apply identification scale factor to forward electron
      electron->auxdecor<float>("weight_loose") = ele_weight;
      electron->auxdecor<float>("weight_tight") = ele_weight;
      electron->auxdecor<float>("weight_iso") = 1.;
      continue;
    }

    if (m_eleSF[0]->getEfficiencyScaleFactor(*electron, reco_weight) 
        != CP::CorrectionCode::Ok) {
      ANA_MSG_WARNING("failed to retrieve ele reco wgt");
      reco_weight = 1.;
    }
    if (m_eleSF[1]->getEfficiencyScaleFactor(*electron, id_weight)
        != CP::CorrectionCode::Ok) {
      ANA_MSG_WARNING("failed to retrieve ele id wgt");
      id_weight = 1.;
    }
    if (m_eleSF[3]->getEfficiencyScaleFactor(*electron, id_tight_weight)
        != CP::CorrectionCode::Ok) {
      ANA_MSG_WARNING("failed to retrieve ele tight id wgt");
      id_tight_weight = 1.;
    }

    electron->auxdecor<float>("weight_loose") = reco_weight * id_weight;
    electron->auxdecor<float>("weight_tight") = reco_weight * id_tight_weight;
    if (m_eleSF[2]->getEfficiencyScaleFactor(*electron, iso_weight) 
        == CP::CorrectionCode::Ok
        && (bool) electron->auxdecor<char>("passIso")
        )  {
      electron->auxdecor<float>("weight_iso") = iso_weight;
    } else {
      electron->auxdecor<float>("weight_iso") = 1.;
    }

    if (!electron->auxdecor<char>("passIso"))
      ANA_MSG_DEBUG("CHECK ISO ele:" << iso_weight);
  }

  // do JVT SF  (per event)
  {
    // ANA_MSG_DEBUG("jvt SF");
    const xAOD::JetContainer      *Jets = nullptr;
    ANA_CHECK (evtStore()->retrieve (Jets, "overlappedJets"));
    float jvtSF = 1.;
    CP::CorrectionCode ret = m_jvtEff->applyAllEfficiencyScaleFactor(Jets, jvtSF);
    switch (ret) {
      case CP::CorrectionCode::Error:
        ANA_MSG_ERROR( "Failed to retrieve SF for jet in JetJvtEfficiency::JVT_SF" );
        return EL::StatusCode::FAILURE;
        break;
      case CP::CorrectionCode::OutOfValidityRange:
        jvtSF = 1.;
        ANA_MSG_VERBOSE( "No valid SF for jet in JetJvtEfficiency::JVT_SF" );
        break;
      default:
        ANA_MSG_VERBOSE( " Retrieve SF for jet container for JVT_SF with value " << jvtSF );
        break;
    }

    // const xAOD::EventInfo* m_eventInfo = 0;
    // ANA_CHECK (evtStore()->retrieve(m_eventInfo, "EventInfo"));
    m_eventInfo->auxdecor<float>("JVT_SF") = jvtSF;
    // ANA_MSG_DEBUG("jvt SF " << jvtSF);
  }

  const xAOD::JetContainer      *Jets = nullptr;
  ANA_CHECK (evtStore()->retrieve (Jets, "SelectedJets"));

  for (const xAOD::Jet *jet: *Jets) {
    CP::CorrectionCode result;
    // regular b-tagging
    for (auto &iter : std::vector<std::pair<int, std::string>> {
        {0, "60"},
        {1, "70"},
        {2, "77"},
        {3, "85"}
        }) {
      auto& id = iter.first;
      auto& wp = iter.second;
      float sf = 1.;
      if (jet->auxdecor<char>("bTag" + wp)) {
        result = m_bTaggingEff[id]->getScaleFactor(*jet, sf);
      } else {
        result = m_bTaggingEff[id]->getInefficiencyScaleFactor(*jet, sf);
      }
      if (result == CP::CorrectionCode::Error) {
        ANA_MSG_ERROR("Cannot Get b-tagging SF!");
        return EL::StatusCode::FAILURE;
      } else if (result == CP::CorrectionCode::OutOfValidityRange) {
        ANA_MSG_DEBUG("b-tagging " << wp << "sf out of range" << sf);
        sf = 1.;
      }
      if (jet->auxdecor<char>("bTag" + wp)) {
        cutflow(sysname).pass("jet", "bTag" + wp, sf);
      } else {
        cutflow(sysname).pass("jet", "bVeto" + wp, sf);
      }
      jet->auxdecor<float>("bTag" + wp + "wgt") = sf;
    } 

    // pseudo-continuous b-tagging
    {
      float sf = 1.;
      if (jet->auxdecor<int>("bTagCont") >= 0) {
        result = m_bTaggingEff[4]->getScaleFactor(*jet, sf);
      } else {
        result = m_bTaggingEff[4]->getInefficiencyScaleFactor(*jet, sf);
      }
      if (result == CP::CorrectionCode::Error) {
        ANA_MSG_ERROR("Cannot Get b-tagging SF!");
        return EL::StatusCode::FAILURE;
      } else if (result == CP::CorrectionCode::OutOfValidityRange) {
        ANA_MSG_DEBUG("b-tagging sf out of range" << sf);
        sf = 1.;
      }
      jet->auxdecor<float>("bTagContwgt") = sf;
    }
  }
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysis::eachSys(CP::SystematicSet& sysListItr) {
  std::string sysname = sysListItr.name();
  if(sysname == "") sysname = "NOMINAL";
  if(SETTING[SETNAME[0]]["dosys"]==0) {
    if(sysname != "NOMINAL") return EL::StatusCode::SUCCESS;
  }

  // TODO: do sth about variation (apply?)
  if (m_isMC) {
    // update it with potential systematics!
    ANA_CHECK( m_pileReweight[m_mcPeriod]->apply( *m_eventInfo ) );
    m_PUWgt = m_eventInfo->auxdata<float>("PileupWeight");
    m_radNum = m_eventInfo->auxdata<unsigned int>("RandomRunNumber");
    ANA_MSG_DEBUG("MC Run number:" << m_radNum);
  } else {
    ANA_MSG_DEBUG("Run number:" << m_run);
  }


  ANA_MSG_DEBUG("checkTRIG ");
  m_passTrig = false;
  // check TRIGGER
  for(const auto & it : m_TRIG_vec) {
    std::string hltname(it.first);
    std::string status(it.second);

    ANA_MSG_DEBUG("  trig name: " << hltname);

    if(m_isMC) {
      // AnaLocalHelper::checkTrigger(m_radNum, status, passTrig, m_eventInfo, hltname+nm1, "MC");
      checkTrigger(m_radNum, status, m_passTrig, hltname, "MC");
    } else {
      // AnaLocalHelper::checkTrigger((int)m_run, status, passTrig, m_eventInfo, hltname+nm1, "Data");
    }

    // early stop
    if (m_passTrig) break;
  }

  if (m_passTrig) cutflow(sysname).pass("evt", "Trigger", m_gen_weight);
  else ANA_MSG_DEBUG("Trig Failed!");




  // fail to apply syst, then pass
  // if (!(quickAna->applySystematicVariation (sysListItr) == 
        // CP::SystematicCode::Ok))
    // return EL::StatusCode::SUCCESS;


  clearAll();

  

  // all muon/elecron/jet selection
  ANA_CHECK( preSelection(sysname) );

  // TODO: sys weight loop should be here
  if (m_isMC) { 
    ANA_CHECK( getSFs(sysname) ); 
  } else { 
    m_eventInfo->auxdecor<float>("JVT_SF") = 1.;
  }

  // TODO: generator weight loop here

  // TODO: add channel/category alg here later

  // Filling
  const xAOD::MuonContainer        *SelectedMuons = nullptr;
  const xAOD::MuonContainer        *SelectedFsrMuons = nullptr;
  const xAOD::ElectronContainer    *SelectedElectrons = nullptr;
  const xAOD::JetContainer         *SelectedJets = nullptr;
  const xAOD::MissingETContainer   *met = nullptr;

  ANA_CHECK (evtStore()->retrieve(SelectedMuons, "SelectedMuons"));
  ANA_CHECK (evtStore()->retrieve(SelectedFsrMuons, "SelectedMuons_FSR"));
  ANA_CHECK (evtStore()->retrieve(SelectedElectrons, "SelectedElectrons"));
  ANA_CHECK (evtStore()->retrieve(SelectedJets, "SelectedJets"));

  ANA_CHECK (evtStore()->retrieve( met, "MissingET"));

  int nMuons = SelectedMuons->size();
  int nElectrons = SelectedElectrons->size();
  // int nJets = SelectedJets->size();

  // Fill tree
  bool fillReco = (nElectrons + nMuons) >= 3;
  if (fillReco) cutflow(sysname).pass("evt", "ThreeLeptons", m_gen_weight);
  m_weight = 1.0;

  if(fillReco) {
    m_eventInfo->auxdecor<char>("FillReco") = 1;
    m_weight = m_PUWgt * m_gen_weight * m_eventInfo->auxdecor<float>("JVT_SF") * m_lumi * m_xs;

    // TODO: 
    // read lumi Xs at the beginning

    // MiniTree
    // Event level
    TreeFltVar["mu"]["Value"] = m_ave_mu;
    TreeIntVar["run"]["Value"] = m_run;
    TreeBoolVar["passTrig"]["Value"] = m_passTrig;
    TreeLngVar["event"]["Value"] = m_event;
    TreeDouVar["weight"]["Value"] = m_weight;
    TreeFltVar["w_sf_jvt"]["Value"] = m_eventInfo->auxdecor<float>("JVT_SF");
    TreeFltVar["w_prw"]["Value"] = m_PUWgt;

    for(const xAOD::Electron *e : *SelectedElectrons) {
      TreeTLVVVar["v_e_tlv"]["Value"].push_back( e->p4() );
      if(e->charge() < 0)
        TreeIntVVar["v_e_pid"]["Value"].push_back(11);
      else 
        TreeIntVVar["v_e_pid"]["Value"].push_back(-11);
      TreeBoolVVar["v_e_LHTight"]["Value"].push_back(
          e->auxdecor<char>("LHTight")
          );
      TreeBoolVVar["v_e_ambiguous"]["Value"].push_back(
          e->auxdecor<char>("ambiguous")
          );
      TreeBoolVVar["v_e_fwd"]["Value"].push_back(
          e->auxdecor<char>("Fwd_LHMedium")
          );
      TreeBoolVVar["v_e_passIso"]["Value"].push_back(
          e->auxdecor<char>("passIso")
          );

      if(m_fillIsoVar) fillLeptonIso(e, "v_e_", "");

      if (m_isMC) {
        TreeFltVVar["v_e_wgtLoose"]["Value"].push_back(
            e->auxdecor<float>("weight_loose")
            );
        TreeFltVVar["v_e_wgtTight"]["Value"].push_back(
            e->auxdecor<float>("weight_tight")
            );
        TreeFltVVar["v_e_wgtIso"]["Value"].push_back(
            e->auxdecor<float>("weight_iso")
            );
      } else {
        TreeFltVVar["v_e_wgtLoose"]["Value"].push_back(
            1.
            );
        TreeFltVVar["v_e_wgtTight"]["Value"].push_back(
            1.
            );
        TreeFltVVar["v_e_wgtIso"]["Value"].push_back(
            1.
            );
      }
    }

    fillMuon(SelectedMuons,    "");
    fillMuon(SelectedFsrMuons, "_fsr");

    for(const xAOD::Jet *jet : *SelectedJets) {
      TreeTLVVVar["v_j_tlv"]["Value"].push_back(jet->p4());
      for (auto &wp : std::vector<std::string> {
          "60",
          "70",
          "77",
          "85"
          }) {
        TreeBoolVVar["v_j_btag" + wp]["Value"].push_back(jet->auxdecor<char>("bTag" + wp));
        if (m_isMC) {  
          TreeFltVVar["v_j_wgt_btag" + wp]["Value"].push_back(jet->auxdecor<float>("bTag" + wp + "wgt"));
        } else {
          TreeFltVVar["v_j_wgt_btag" + wp]["Value"].push_back(1.);
        }
      }
      TreeIntVVar["v_j_btagCont"]["Value"].push_back(jet->auxdecor<int>("bTagCont"));
      if (m_isMC) {  
        TreeFltVVar["v_j_wgt_btagCont"]["Value"].push_back(jet->auxdecor<float>("bTagContwgt"));
      } else {
        TreeFltVVar["v_j_wgt_btagCont"]["Value"].push_back(1.);
      }
      // TreeFltVVar["v_wgt_j"]["Value"].push_back();
    }

    TreeDouVar["MET"]["Value"] = (*met)["Final"]->met();
    TreeDouVar["METSig"]["Value"] = m_eventInfo->auxdecor<float>("met_signi_AntiKt4EMPFlow");
  }
  else {
  }


  if((fillReco && m_noTruthAlg)
      || m_eventInfo->auxdecor<char>("FillTruth")) { 
    ANA_MSG_DEBUG("FillTree");
    Tree[sysname]->Fill();
  }
  return EL::StatusCode::SUCCESS;
}


void MyxAODAnalysis::fillMuon(const xAOD::MuonContainer* container, 
    std::string postfix) {

  for(const xAOD::Muon *m : *container) {
    TreeTLVVVar["v_m_tlv" + postfix]["Value"].push_back(m->p4());
    if(m->charge() < 0) {
      TreeIntVVar["v_m_pid" + postfix]["Value"].push_back(13);
    } else {
      TreeIntVVar["v_m_pid" + postfix]["Value"].push_back(-13);
    }
    TreeFltVVar["v_m_d0" + postfix]["Value"].push_back(
        m->auxdata<double>("d0val")
        );
    TreeBoolVVar["v_m_passIso" + postfix]["Value"].push_back(
        m->auxdecor<char>("passIso")
        );
    TreeBoolVVar["v_m_passIsoVarRad" + postfix]["Value"].push_back(
        m->auxdecor<char>("passIsoVarRad")
        );
    TreeBoolVVar["v_m_lowpt" + postfix]["Value"].push_back(
        m->auxdecor<char>("Lowpt_muon")
        );

    if(m_fillIsoVar) fillLeptonIso(m, "v_m_", postfix);

    if (m_isMC) {
      TreeFltVVar["v_m_wgtLoose" + postfix]["Value"].push_back(
          m->auxdecor<float>("weight_loose")
          );
      TreeFltVVar["v_m_wgtIso" + postfix]["Value"].push_back(
          m->auxdecor<float>("weight_iso")
          );
      TreeFltVVar["v_m_wgtIsoVarRad" + postfix]["Value"].push_back(
          m->auxdecor<float>("weight_iso_varRad")
          );
    } else {
      TreeFltVVar["v_m_wgtLoose" + postfix]["Value"].push_back(
          1.
          );
      TreeFltVVar["v_m_wgtIso" + postfix]["Value"].push_back(
          1.
          );
      TreeFltVVar["v_m_wgtIsoVarRad" + postfix]["Value"].push_back(
          1.
          );
    }

    ANA_MSG_DEBUG("muon wgt loose" << m->auxdata<float>("weight_loose"));
    // ANA_MSG_DEBUG("muon wgt tight" << m->auxdata<float>("muon_weight_tight"));
  }
}

void MyxAODAnalysis::fillLeptonIso(const xAOD::IParticle* part,
    std::string prefix,
    std::string postfix) {
  for (auto& varName: m_isolationVar) {
    fillTreeVVar<float>(prefix + varName + postfix,
        part->auxdecor<float>(varName));
  }

}
