#include "TSystem.h"

#include <TH1F.h>
#include <TH1D.h>
#include <EventLoop/IWorker.h>
// #include <EventLoop/Job.h>
// #include <EventLoop/StatusCode.h>
// #include <EventLoop/Worker.h>
// #include <EventLoopAlgs/NTupleSvc.h>
// #include <EventLoopAlgs/AlgSelect.h>

#include <GenAnaUtilities/TruthUtilities.h>

#include "MyAnalysis/MyxAODTruthAnalysis.h"
#include "MyAnalysis/OBJ_Base.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODJet/JetContainer.h"
#include <xAODMuon/Muon.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODMuon/MuonAuxContainer.h>
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthVertexContainer.h"

// #include <QuickAna/QuickAna.h>


// #include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowCopy.h"

#include "PileupReweighting/PileupReweightingTool.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "AssociationUtils/EleMuSharedTrkOverlapTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"



// #include "Initialize.h"
// #include "Counting.h"

#include "PathResolver/PathResolver.h"
#include "AsgTools/MessageCheck.h"

// define my own electron
// #include <QuickAna/DefinitionMaker.h>
// #include <QuickAna/HZZMuonTool.h>
// #include <QuickAna/HZZElectronTool.h>


// this is needed to distribute the algorithm to the workers
// ClassImp(MyxAODTruthAnalysis)


// helpers

// constructor
MyxAODTruthAnalysis::MyxAODTruthAnalysis(const std::string& name, 
    ISvcLocator *pSvcLocator) 
  : EL::AnaAlgorithm (name, pSvcLocator)
{

  StatusCode::enableFailure();
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().

  //m_setSysList=new std::vector<std::string>();
}

EL::StatusCode MyxAODTruthAnalysis::initialize() {


  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files. You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  time(&start);




  // const xAOD::EventInfo* eventInfo = 0;
  // if(!evtStore()->retrieve( eventInfo, "EventInfo").isSuccess()) {
    // ANA_MSG_ERROR("execute () Failed to retrieve EventInfo. Exiting." );
    // return EL::StatusCode::FAILURE;
  // }
  // isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );


  std::string tree_name = "tree_NOMINAL";

  // check if only Truth is possible
  // ANA_CHECK(book( TTree(tree_name.c_str(), "output tree")));
  Tree["NOMINAL"] = tree( tree_name );

  ANA_MSG_INFO( Tree["NOMINAL"] );
  AddVarIntoTreeFile(Tree["NOMINAL"], 
      "MyAnalysis/TruthMiniTree.txt", "NOMINAL", true);

  
  m_eventCounter = 0;

  ANA_CHECK( requestBeginInputFile() );

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode MyxAODTruthAnalysis::beginInputFile() {
  ANA_MSG_INFO("beginInputFile");
  ANA_CHECK (evtStore()->retrieve(m_eventInfo, "EventInfo"));
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODTruthAnalysis::execute() {
  ClearVariables(VVar);
  ClearVariables(TreeIntVar);
  ClearVariables(TreeIntVVar);
  ClearVariables(TreeDouVar);
  ClearVariables(TreeLngVar);
  ClearVariables(TreeFltVar);
  ClearVariables(TreeBoolVVar);
  ClearVariables(TreeFltVVar);
  ClearVariables(TreeDouVVar);
  ClearVariables(TreeTLVVar);
  ClearVariables(TreeTLVVVar);
  m_eventCounter++;

  ANA_MSG_DEBUG("Processing truth " );

  
  // const xAOD::TruthEventContainer* xTruthEventContainer = NULL;
  // ANA_CHECK( evtStore()->retrieve( xTruthEventContainer, "TruthEvents").isSuccess());

  // TruthEvent_v1 https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Event/xAOD/xAODTruth/Root/TruthEvent_v1.cxx?v=21.2 
  // TruthEventBase_v1 https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Event/xAOD/xAODTruth/Root/TruthEventBase_v1.cxx?v=21.2
  // if (xTruthEventContainer->size() != 1) {
    // ANA_MSG_WARNING("Wrong truth event info!?");
    // return EL::StatusCode::SUCCESS;
  // }

  // const xAOD::TruthEvent *evt = (*xTruthEventContainer)[0];
  // ANA_MSG_INFO("truth particle size:" << evt->nTruthParticles());

  xAOD::TruthParticleContainer const * truthParticles = 0;
  ANA_CHECK( evtStore()->retrieve( truthParticles, "TruthParticles").isSuccess());

  if(m_eventCounter == 1 || m_eventCounter % 5000==0) {
    ANA_MSG_INFO( "event counter (truth) " << m_eventCounter );
  }


  bool fillTruth = false;
  // not doing any condition right now
  if(fillTruth) {
    ANA_MSG_INFO(m_eventInfo << "Checking FillTruth");
    m_eventInfo->auxdecor<char>("FillTruth") = 1;
  }
  
  // status code
  // https://home.fnal.gov/~mrenna/HCPSS/HCPSShepmc.html

  bool tauEvent = false;

  for (const xAOD::TruthParticle* particle: *truthParticles) {
    // bool ProdVx = particle->hasProdVtx();
    // bool DecayVx = particle->hasDecayVtx();
    if(!particle) {
      ANA_MSG_WARNING("not valid truth particle");
      continue;
    }
    
    ANA_MSG_DEBUG("Truth Part: PDG = " << particle->pdgId() << 
        ", status = " << particle->status() << 
        ", barcode = " << particle->barcode() <<
        ", pt = " << particle->pt()
        );
    for (size_t iParent = 0; iParent < particle->nParents(); iParent++)  {
      const xAOD::TruthParticle* parent = particle->parent(iParent);
      if(!parent) {
        ANA_MSG_WARNING("not valid truth parent");
        continue;
      }
      ANA_MSG_DEBUG("\t parent: PDG = " << parent->pdgId() << 
        ", status = " << parent->status() << 
        ", barcode = " << parent->barcode() <<
        ", pt = " << parent->pt()
        );
    }

    // works for Sherpa (signal & major bkg)
    if (!(particle->barcode() < 200000 && particle->status() == 1)) { 
      continue;
    } 

    if (std::abs(particle->pdgId()) == 11 ||
          std::abs(particle->pdgId()) == 13) {
      TreeTLVVVar["v_truth_l_tlv"]["Value"].push_back(particle->p4());
      TreeIntVVar["v_truth_l_pdgId"]["Value"].push_back(particle->pdgId());
      if(TruthUtilities::hasParent(particle, {15, -15})) {
        TreeBoolVVar["v_truth_l_fromTau"]["Value"].push_back(1);
        tauEvent = true;
      } else { 
        TreeBoolVVar["v_truth_l_fromTau"]["Value"].push_back(0);
      }
    }



    if (std::abs(particle->pdgId()) == 16) {
      tauEvent = true;
    }

  }

  TreeBoolVar["tauEvent"]["Value"] = tauEvent; 

  ANA_MSG_DEBUG("To process jet " );
  const xAOD::JetContainer* truthJetCont = 0;
  ANA_CHECK( evtStore()->retrieve( truthJetCont, "AntiKt4TruthWZJets" ).isSuccess());
  for (const xAOD::Jet_v1* jet: *truthJetCont) {
      TreeTLVVVar["v_truth_j_tlv"]["Value"].push_back(jet->p4()); 
  }
  ANA_MSG_DEBUG("Processed truth jet " );


  ANA_MSG_DEBUG("to fill Truth" );
  if(fillTruth || m_eventInfo->auxdecor<char>("FillReco")) {
    Tree["NOMINAL"]->Fill();
  }
  ANA_MSG_DEBUG("filled Truth" );

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODTruthAnalysis::finalize() {

  return EL::StatusCode::SUCCESS;
}
