#!/bin/bash
matching_stringr="user.rowang.WVZ_v1.2*"
echo '
from cStringIO import StringIO;
import sys;
old_stdout = sys.stdout; sys.stdout = mystdout = StringIO(); 
show(taskname="'$matching_string'");
sys.stdout = old_stdout;
fout = open("rucio_download_finished.sh", "w");
for i in mystdout.getvalue().split("\n")[2:-1]: 
    if i.split()[2] != "done":
        print i; 
        continue;
    fout.write("rucio download " + i.split()[4][:-1] + "_ANALYSIS.root\n");
'| pbook

mkdir -p data-ANALYSIS
