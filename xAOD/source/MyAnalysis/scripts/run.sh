# runTest.py -s -i signal -o ${tag}_Signal -C
# runTest.py -s -i datac_bkg -o ${tag}_BkgDataC -C
# runTest.py -s -i dataf -o ${tag}_DataF -C
# runTest.py -R -i grid_sample.txt -o ${tag}_gridMC --tag WVZ_$tag

tag=v1.2
# runTest.py -s -i signal -o Signal_$tag -C
# runTest.py -s -i datac_bkg -o BkgDataC_$tag -C
# runTest.py -s -i dataf -o DataF_$tag -C
# will be switched to grid
runTest.py -s -i ustc02 -o USTC02_batch2_$tag -C --optCondorConf "requirements            = (substr(Machine,0,7) =!= \"gpuatum\")"
# runTest.py -R -i grid_sample.txt -o gridMC_$tag --tag WVZ_${tag}
# runTest.py -R -i grid_sample.txt -o gridMC2_${tag} --tag WVZ_${tag}

# tag=v1.1
# runTest.py -s -i signal -o Signal_$tag -C
# runTest.py -s -i signal -o Signal_$tag -C -r
# runTest.py -s -i datac_bkg -o BkgDataC_$tag -C
# runTest.py -s -i datac_bkg -o BkgDataC_$tag -C -r
# runTest.py -s -i dataf -o DataF_$tag -C
# runTest.py -s -i dataf -o DataF_$tag -C -r
# runTest.py -s -i ustc02 -o USTC02_$tag -C


# tag=v1
# runTest.py -s -i signal -o Signal_$tag -C
# runTest.py -s -i datac_bkg -o BkgDataC_$tag -C
# runTest.py -s -i datac_bkg -o BkgDataC_$tag -C -r
# runTest.py -s -i datac --cherry-pick 363358,363356 -o BkgDataC_${tag}_cherrypick_36335X -C

# runTest.py -s -i signal -o Signal_${tag}_test -C -r
# runTest.py -s -i ustc02 -o USTC02_$tag -C
# runTest.py -s -i dataf -o DataF_$tag -C
