# can be changed
tag=$1

if (( $# < 1 )); then
    echo "please give <tag> as first input"
    echo "put d/download as the second"
    exit 1
fi

thisUser=$USER

rucio list-dids user.${USER}.WVZ_${tag}.*_ANALYSIS.root 

if [[ $2 == "download" || $2 == "d" ]]; then
    echo "downloading of sample will start"
    rucio download user.${USER}.WVZ_${tag}.*_ANALYSIS.root 
fi

# in case you source it
unset thisUser tag
