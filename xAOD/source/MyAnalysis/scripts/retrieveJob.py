#!/usr/bin/env python

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-o', '--outdir',
        type = str,
        default = 'output/TestDir',
        help = 'Submission directory for EventLoop' )
options = parser.parse_args()

import ROOT
ROOT.EL.Driver.wait(options.outdir)
