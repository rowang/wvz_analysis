#!/usr/bin/env python
from __future__ import print_function

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-v', '--verbose',
        action = "store_true",
        help = 'verbose mode' )
parser.add_argument('-d', '--dbg',
        action = "store_true",
        help = 'debug mode' )
parser.add_argument('-i', '--in',
        type = str,
        dest = "input",
        default = 'input.list_local',
        help = 'Input list of files, or file type: all signal bkg datac_bkg datac dataf ustc02 ' )
parser.add_argument('-c', "--cherry-pick",
        type = str,
        dest = "cherry_pick",
        default = "",
        help = "--cherry-pick dsid1,...,dsidN to run only a selective of DSID. Should not be used with bkg")
parser.add_argument('-o', '--outdir',
        type = str,
        default = 'output/TestDir',
        help = 'Submission directory for EventLoop' )
parser.add_argument('-m', '--maxEvt',
        type = int,
        default = -1,
        help = 'max evt to process' )

parser.add_argument('--AFII',
        action = "store_true",
        help = 'run on AFII samples only(WIP)' )

parser.add_argument('--nosubmit',
        action = "store_true",
        help = 'do not submit' )
parser.add_argument('-r', '--resubmit',
        action = "store_true",
        help = 'resubmit the jobs that failed' )

# what sample?
#  parser.add_argument()

# what scan?
parser.add_argument('-R', '--scanRucio',
        action = "store_true",
        help = 'scan rucio[WIP]' )
parser.add_argument('-t', '--tag',
        type = str,
        default = 'test',
        help = 'the tag when submitting to grid')
parser.add_argument('-s', '--scanDir',
        action = "store_true",
        help = 'scan dir instead of input link, all local' )

# what driver
parser.add_argument('-C', '--CondorDriver',
        action = "store_true",
        help = 'use Local Driver' )
parser.add_argument('--optCondorConf',
        type = str,
        default = "",
        help = 'specify the additional configuration for condor' )

parser.add_argument('--data',
        action = "store_true",
        help = 'run on Data (grid driver)' )
parser.add_argument('--noTruth',
        action = "store_true",
        help = 'Do not do truth for MC' )
options = parser.parse_args()

import ROOT
ROOT.xAOD.Init().ignore()

import os
sh_all = ROOT.SH.SampleHandler()
sh_selected = ROOT.SH.SampleHandler()

server = "none"
if "lxplus" in os.environ["HOSTNAME"]:
    server = "lxplus"
    print("running on lxplus")
elif "atustc" in os.environ["HOSTNAME"]:
    server = "atustc"
    print("running on atustc")

def skip_sample(full_name, l_wanted, veto = False):
    """
    return True if the sample should be skipped over
    if not specifying l_wanted, always not skip
    """
    if len(l_wanted) == 0: return False
    for wanted in l_wanted:
        if wanted in full_name:
            return veto
    return not veto

d_sample_collection = {
        "signal":[
            "364242",
            "364243",
            "364244",
            "364245",
            "364246",
            "364247",
            "364248",
            "364249",
            "363507",
            "363508",
            "363509", 
            ],
        # those below exist locally, but will instead submit grid jobs for them
        "grid":[
            ],
        # those samples below are obsolete, skip
        "obsolete":[
            "410217",
            "410503",
            "410011",
            "410012",
            "410013",
            "410014",
            "410025",
            "410026",
            "410080",
            ],
        }

if options.scanDir:
    datac  = "/net/s3_datac/rowang/WVZ_xAOD/"
    ustc02 = "/net/ustc_02/users/rowang/WVZ_xAOD/"
    dataf  = "/net/s3_dataf/rowang/WVZ_xAOD/"
    l_to_scan = []

    veto = False
    sample_selection = []
    if options.input == parser.get_default('input'):
        print("please pass in an input!")
        exit(0)
        if server == "lxplus":
            l_to_scan.append( "/eos/user/r/rowang/public/WVZ_xAOD/" )
        elif server == "atustc":
            l_to_scan.append( datac  )
        pass
    elif options.input == "signal":
        l_to_scan.append( datac  )
        sample_selection = d_sample_collection["signal"]
    elif options.input == "datac":
        l_to_scan.append( datac  )
    elif options.input == "ustc02":
        l_to_scan.append( ustc02  )
    elif options.input == "dataf":
        l_to_scan.append( dataf )
    elif options.input == "all" or options.input == "bkg":
        l_to_scan.append( datac )
        l_to_scan.append( ustc02 )
        l_to_scan.append( dataf )
        if options.input == "bkg":
            sample_selection = d_sample_collection["signal"]
            veto = True
    elif options.input == "datac_bkg":
        l_to_scan.append( datac )
        sample_selection = d_sample_collection["signal"]
        veto = True
    else:
        print("scan the input itself!")
        l_to_scan.append(options.input)

    if options.cherry_pick != "":
        sample_selection = options.cherry_pick.split(",")
        print("Cherry-picking:", sample_selection)


    for _dir in l_to_scan:
        print("scanning this directory:", _dir)
        sh = ROOT.SH.SampleHandler()
        ROOT.SH.ScanDir().scan(sh, _dir)
        for f in sh:
            print(f.name())
            sh_all.add(f)

    for f in sh_all:
        # list of obsolete samples but downloaded
        if skip_sample(f.name(), d_sample_collection["obsolete"], veto = True):
            continue
        if skip_sample(f.name(), d_sample_collection["grid"], veto = True):
            continue
        if skip_sample(f.name(), sample_selection, veto): 
            print("skipping", f.name())
            continue
        print(f.name())
        #  if "364250" not in f.name() and \
                #  "345706" not in f.name(): continue
        sh_selected.add(f)
        #  print(f.getNumEntries ())
        #  print (type(f), f.getFileName(0))
        pass
    #  exit(0)
    pass
elif options.scanRucio:
    #  raise Exception("work in progress!")
    if options.input == parser.get_default('input'):
        print("PLEASE DO NOT USE RUCIO FOR RUNNING AT LOCAL!")
        exit(1)
    with open(options.input, "r") as sample_file:
        for sample in sample_file:
            sample = sample.rstrip()
            if not sample.strip(): continue
            if sample[0] == "#": continue
            if sample[-1] != "/": sample += "/"
            ROOT.SH.scanRucio(sh_all, sample)
    pass
else:
    try:
        ROOT.SH.readFileList (sh_selected, "sample", options.input)
    except:
        ROOT.SH.ScanDir().scan(sh_selected, options.input)
    pass


if options.scanRucio:
    sh_selected = sh_all

sh_selected.setMetaString( 'nc_tree', 'CollectionTree' )
#  print sh.getMetaString("lumi")
sh_selected.printContent();
#  for f in sh_selected:
    #  print(f.doMakeFileList())
    #  #  print(f.name())
    #  if "_r9364_" in f.name():
        #  period = "16a"
    #  elif "_r10201_" in f.name():
        #  period = "16d"
    #  elif "_r10724_" in f.name():
        #  period = "16e"
    #  break

job = ROOT.EL.Job()
job.sampleHandler(sh_selected);
job.options().setDouble( ROOT.EL.Job.optMaxEvents, options.maxEvt )
# learn the read xAOD feature
job.options().setDouble (ROOT.EL.Job.optCacheSize, 10*1024*1024);
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# TODO:
isAFII = options.AFII
isMC = not options.data

# output
job.outputAdd (ROOT.EL.OutputStream("ANALYSIS"));
#  job.outputAdd (ROOT.EL.OutputStream("hist_output"));

from AnaAlgorithm.DualUseConfig import createAlgorithm
from AnaAlgorithm.DualUseConfig import addPrivateTool

alg = createAlgorithm ( 'MyxAODAnalysis',           'WVZAlg' )
algTruth = createAlgorithm ( 'MyxAODTruthAnalysis', 'WVZTruthAlg' )
alg.isMC = isMC
alg.noTruthAlg = options.noTruth

# developmental things
alg.fillIsoVar = True

# for dual use..
#  addPrivateTool( alg, 'trigDec',          'Trig::TrigDecisionTool' )
addPrivateTool( alg, 'grlTool',          'GoodRunsListSelectionTool' )
addPrivateTool( alg, 'prwTool16a',       'CP::PileupReweightingTool' )
addPrivateTool( alg, 'prwTool16d',       'CP::PileupReweightingTool' )
addPrivateTool( alg, 'prwTool16e',       'CP::PileupReweightingTool' )
addPrivateTool( alg, 'isoTool',          'CP::IsolationSelectionTool')
addPrivateTool( alg, 'isoToolVarRad',          'CP::IsolationSelectionTool')
addPrivateTool( alg, 'isoCorrTool',      'CP::IsolationCorrectionTool')
#addPrivateTool( alg, 'isoCloseByTool',   'CP::IsolationCloseByCorrectionTool')

# Muon tools
addPrivateTool( alg, 'muSel',            'CP::MuonSelectionTool')
addPrivateTool( alg, 'muSelLowpt',       'CP::MuonSelectionTool')
addPrivateTool( alg, 'muCalib18',        'CP::MuonCalibrationAndSmearingTool')
addPrivateTool( alg, 'muCalib17',        'CP::MuonCalibrationAndSmearingTool')
addPrivateTool( alg, 'muCalib16',        'CP::MuonCalibrationAndSmearingTool')
addPrivateTool( alg, 'muRecoSF',         'CP::MuonEfficiencyScaleFactors')
addPrivateTool( alg, 'muRecoLowptSF',    'CP::MuonEfficiencyScaleFactors')
addPrivateTool( alg, 'muTTVASF',         'CP::MuonEfficiencyScaleFactors')
addPrivateTool( alg, 'muIsoSF',          'CP::MuonEfficiencyScaleFactors')
addPrivateTool( alg, 'muIsoSFVarRad',    'CP::MuonEfficiencyScaleFactors')
#addPrivateTool( alg, 'muBadMuVeto',      'CP::MuonEfficiencyScaleFactors')
#addPrivateTool( alg, 'muRecoSF_HPt',     'CP::MuonEfficiencyScaleFactors')

# Electron tools
addPrivateTool( alg, 'eleSel',           'AsgElectronLikelihoodTool')
addPrivateTool( alg, 'eleSelTight',      'AsgElectronLikelihoodTool')
addPrivateTool( alg, 'eleSelFwd',        'AsgForwardElectronLikelihoodTool')
addPrivateTool( alg, 'eleCalib',         'CP::EgammaCalibrationAndSmearingTool')
addPrivateTool( alg, 'eleRecoSF',        'AsgElectronEfficiencyCorrectionTool')
addPrivateTool( alg, 'eleIDSF',          'AsgElectronEfficiencyCorrectionTool')
addPrivateTool( alg, 'eleIDTightSF',     'AsgElectronEfficiencyCorrectionTool')
addPrivateTool( alg, 'eleFwdIDSF',       'AsgElectronEfficiencyCorrectionTool')
addPrivateTool( alg, 'eleIsoSF',         'AsgElectronEfficiencyCorrectionTool')
# Photon tools
#addPrivateTool( alg, 'phoSel',           'AsgPhotonIsEMSelector')
addPrivateTool( alg, 'phoCalib',         'CP::EgammaCalibrationAndSmearingTool')
addPrivateTool( alg, 'phoFudgeMC',       'ElectronPhotonShowerShapeFudgeTool')
addPrivateTool( alg, 'phoIDSF',          'AsgPhotonEfficiencyCorrectionTool')
addPrivateTool( alg, 'phoIsoSF',         'AsgPhotonEfficiencyCorrectionTool')

# Jets tools
addPrivateTool( alg, 'jetCalib',         'JetCalibrationTool')
addPrivateTool( alg, 'jetSmearing',      'JetUncertaintiesTool')
addPrivateTool( alg, 'jvtTool',          'JetVertexTaggerTool')
addPrivateTool( alg, 'jetJvtEfficiency', 'CP::JetJvtEfficiency')
addPrivateTool( alg, 'fjvtTool',         'JetForwardJvtTool')
# MET tools
addPrivateTool( alg, 'metMaker',         'met::METMaker')
addPrivateTool( alg, 'metSysTool',       'met::METSystematicsTool')
addPrivateTool( alg, 'metSigni',         'met::METSignificance')


# grl + PRW
d_grl = {
        "16a": [
            "GoodRunsLists/data15_13TeV/20170619/physics_25ns_21.0.19.xml", 
            "GoodRunsLists/data16_13TeV/20180129/physics_25ns_21.0.19.xml",
            ],
        "16d": [ "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml", ],
        #  "16d": [ "GoodRunsLists/data17_13TeV/20171130/data17_13TeV.periodAllYear_DetStatus-v97-pro21-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml", ],
        "16e": [ "GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.xml" ],
        }
d_lumi = {
        "16a": [ 
            "GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root", 
            "GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root",
            ],
        "16d": [ "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root", ],
        #  "16d": [ "GoodRunsLists/data17_13TeV/20171130/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-001.root", ],
        "16e": [ "GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root", ],
        }

for period, flist in d_lumi.items():
    getattr(alg, "prwTool" + period).LumiCalcFiles = d_lumi[period]
    getattr(alg, "prwTool" + period).ConfigFiles   = [
        "MyAnalysis/mc{0}.prw.root".format(period)
    ]


alg.grlTool.PassThrough = 0
alg.grlTool.GoodRunsListVec = d_grl["16a"] + d_grl["16d"] + d_grl["16e"]


# Isolation
alg.isoCorrTool.IsMC      = isMC
alg.isoCorrTool.AFII_corr = isAFII

#  alg.isoTool.MuonWP     = "FixedCutPflowLoose"
alg.isoTool.MuonWP     = "PflowLoose_FixedRad"
alg.isoTool.ElectronWP = "FixedCutPflowLoose"
alg.isoTool.PhotonWP   = "FixedCutLoose"

alg.isoToolVarRad.MuonWP     = "PflowLoose_VarRad"



# MUON
# MuQuality can take values of {0, 1, 2, 3, 4, 5} which correspond to Tight, Medium, Loose, VeryLoose, HighPt and LowPtEfficiency muon qualities
alg.muSel.MuQuality = 2
alg.muSel.MaxEta    = 2.7

alg.muSelLowpt.MuQuality = 5
alg.muSelLowpt.MaxEta    = 2.7
# do it or not, need to be used after p4164
#  alg.muSelLowpt.UseMVALowPt = True

alg.muRecoSF.WorkingPoint = "Loose"
alg.muRecoLowptSF.WorkingPoint = "LowPt"
alg.muTTVASF.WorkingPoint = "TTVA"
alg.muIsoSF.WorkingPoint  = alg.isoTool.MuonWP + "Iso"
alg.muIsoSFVarRad.WorkingPoint  = alg.isoToolVarRad.MuonWP + "Iso"

for n in range(16, 19):
    name = "muCalib{0}".format(n)
    getattr(alg, name).Release = "Recs2019_10_12"
    getattr(alg, name).Year = "Data{0}".format(n)
    getattr(alg, name).StatComb = False
    getattr(alg, name).SagittaRelease = "sagittaBiasDataAll_03_02_19_Data{0}".format(n)
    getattr(alg, name).SagittaCorr = True
    getattr(alg, name).doSagittaMCDistortion = False
    getattr(alg, name).SagittaCorrPhaseSpace = True
    pass

#ELECTRON
# below are other working points for electron selection, they are VLooseLLH, LooseLLH, LooseAndBLayerLLH, MediumLLH, TightLLH
# alg.eleSel.ConfigFile = "ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodVeryLooseOfflineConfig2017_Smooth.conf"
# alg.eleSel.ConfigFile = "ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodLooseOfflineConfig2017_Smooth.conf"
alg.eleSel.ConfigFile = "ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodLooseOfflineConfig2017_CutBL_Smooth.conf"
# alg.eleSel.ConfigFile = "ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodMediumOfflineConfig2017_Smooth.conf"
alg.eleSelTight.ConfigFile = "ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodTightOfflineConfig2017_Smooth.conf"
# Forward electron LH identification: LooseLHForwardElectron, MediumLHForwardElectron, TightLHForwardElectron
alg.eleSelFwd.WorkingPoint = "MediumLHForwardElectron"

alg.eleRecoSF.MapFilePath      = "ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/map3.txt"
alg.eleRecoSF.RecoKey          = "Reconstruction"
alg.eleRecoSF.CorrelationModel = "TOTAL"
# ForceDataType, 1 for full-sim, 3 for AFII
alg.eleRecoSF.ForceDataType    = 3 if isAFII else 1

# TODO:
alg.eleIDTightSF.MapFilePath        = "ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/map3.txt"
alg.eleIDTightSF.IdKey              = "Tight"
alg.eleIDTightSF.CorrelationModel   = "SIMPLIFIED"
alg.eleIDTightSF.ForceDataType      = 1

alg.eleIDSF.MapFilePath        = "ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/map3.txt"
alg.eleIDSF.IdKey              = "LooseBLayer"
alg.eleIDSF.CorrelationModel   = "SIMPLIFIED"
alg.eleIDSF.ForceDataType      = 1

alg.eleFwdIDSF.MapFilePath        = "ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/map3.txt"
alg.eleFwdIDSF.IdKey              = "FwdMedium"
alg.eleFwdIDSF.CorrelationModel   = "SIMPLIFIED"
alg.eleFwdIDSF.ForceDataType      = 1

alg.eleIsoSF.CorrectionFileNameList = ["MyAnalysis/isolation/efficiencySF.Isolation.LooseAndBLayerLLH_d0z0_DataDriven_Rel21_Smooth_vTest_isolFixedCutPflowLoose.root"]
alg.eleIsoSF.IdKey             = "LooseBLayer"
alg.eleIsoSF.IsoKey            = "FixedCutPflowLoose"
alg.eleIsoSF.CorrelationModel  = "TOTAL"
alg.eleIsoSF.ForceDataType     = 1

alg.eleCalib.ESModel = "es2018_R21_v0"
alg.eleCalib.decorrelationModel = "1NP_v1"
alg.eleCalib.useAFII = isAFII

#PHOTON
alg.phoCalib.ESModel = "es2018_R21_v0"
alg.phoCalib.decorrelationModel = "1NP_v1"
alg.phoCalib.useAFII = isAFII

alg.phoFudgeMC.Preselection = 22
alg.phoFudgeMC.FFCalibFile  = "ElectronPhotonShowerShapeFudgeTool/v2/PhotonFudgeFactors.root"

alg.phoIDSF.MapFilePath     = "PhotonEfficiencyCorrection/2015_2017/rel21.2/Summer2018_Rec_v1/map4.txt"
alg.phoIDSF.ForceDataType   = 1
# Set isolation WP: Loose,Tight,TightCaloOnly
alg.phoIsoSF.MapFilePath    = "PhotonEfficiencyCorrection/2015_2017/rel21.2/Summer2018_Rec_v1/map4.txt"
alg.phoIsoSF.IsoKey         = "Loose"
alg.phoIsoSF.ForceDataType  = 1


#JET
alg.jetCalib.JetCollection = "AntiKt4EMPFlow"
alg.jetCalib.ConfigFile = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config"
alg.jetCalib.CalibSequence = "JetArea_Residual_EtaJES_GSC_Smear"
if isAFII:
    alg.jetCalib.ConfigFile = "JES_MC16Recommendation_AFII_PFlow_Apr2019_Rel21.config"
if not isMC:
    alg.jetCalib.CalibSequence = "JetArea_Residual_EtaJES_GSC_Insitu"
alg.jetCalib.CalibArea = "00-04-82"
alg.jetCalib.IsData = not isMC

alg.jetSmearing.JetDefinition = "AntiKt4EMPFlow"
alg.jetSmearing.MCType = "AFII" if isAFII else "MC16"
alg.jetSmearing.ConfigFile = "rel21/Summer2019/R4_CategoryReduction_FullJER.config"
alg.jetSmearing.IsData = not isMC

alg.jetJvtEfficiency.SFFile = "JetJvtEfficiency/Moriond2018/JvtSFFile_EMPFlow.root"
# default for JVT for pflowjet
alg.jetJvtEfficiency.MaxPtForJvt = 60e3

# FJVT for pflowjet, used for MET
alg.fjvtTool.UseTightOP = True
alg.fjvtTool.EtaThresh = 2.5
alg.fjvtTool.ForwardMaxPt = 120.e3

for wp in [60, 70, 77, 85, "Cont"]:
    nbtag    = "bTagging{0}".format(wp)
    nbtageff = "bTaggingEff{0}".format(wp)
    addPrivateTool( alg, nbtag, 'BTaggingSelectionTool')
    addPrivateTool( alg, nbtageff, 'BTaggingEfficiencyTool')
    for n in [nbtag, nbtageff]:
        getattr(alg, n).TaggerName = "DL1r"
        getattr(alg, n).MinPt = 20.e3
        if wp == "Cont":
            getattr(alg, n).OperatingPoint = "Continuous"
        else:
            getattr(alg, n).OperatingPoint = "FixedCutBEff_{0}".format(wp)
        getattr(alg, n).JetAuthor = "AntiKt4EMPFlowJets_BTagging201903"
    getattr(alg, nbtag).FlvTagCutDefinitionsFileName = "xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-03-11_v3.root"
    getattr(alg, nbtageff).ScaleFactorFileName = "xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-03-11_v3.root"


# MissingET
alg.metMaker.ORCaloTaggedMuons = True
alg.metMaker.DoSetMuonJetEMScale = True
alg.metMaker.DoPFlow = True
alg.metMaker.JetSelection = "Loose"
#alg.metMaker.OutputLevel = 1
#alg.metMaker.JetSelection = "Tight"

if isMC:
    alg.metSysTool.ConfigSoftCaloFile = ""
    alg.metSysTool.ConfigSoftTrkFile = "TrackSoftTerms-pflow.config"

# met::Random = 0, met::PthardParam = 1, met::TSTParam = 2
alg.metSigni.SoftTermParam = 0 
alg.metSigni.TreatPUJets = True
alg.metSigni.DoPhiReso = True
alg.metSigni.IsAFII = isAFII
alg.metSigni.JetCollection = "AntiKt4EMPFlow"

#  job.useXAOD()

#  ROOT.xAOD.Init("MyxAODAnalysis").ignore()
#  ROOT.xAOD.Init("MyxAODTruthAnalysis").ignore()


# msg level
if options.dbg:
    alg.OutputLevel = 2
    alg.metMaker.OutputLevel = 2
    algTruth.OutputLevel = 2
if options.verbose:
    alg.OutputLevel = 1
    alg.metMaker.OutputLevel = 1
    algTruth.OutputLevel = 1

job.algsAdd( alg )
if isMC and not options.noTruth:
    job.algsAdd( algTruth )
    pass


if options.CondorDriver or options.scanRucio:
    job.options().setDouble(ROOT.EL.Job.optFilesPerWorker, 10)
    if options.outdir == parser.get_default('outdir'):
        raise Exception("Please specify a name for your runDir")

if options.CondorDriver:
    driver = ROOT.EL.CondorDriver()
    driver.options().setString("nc_EventLoop_CondorConf", options.optCondorConf + "\n")
    if options.nosubmit: 
        exit(0)
    if options.resubmit:
        print("This is a RESUBMISSION job!")
        driver.resubmit(os.path.abspath(options.outdir), "ALL_MISSING")
        exit(0)
    driver.submitOnly( job, options.outdir )
    exit(0)

if options.scanRucio:
    driver = ROOT.EL.PrunDriver()
    driver.options().setString("nc_outputSampleName", "user.{0}.{1}.%in:name[2]%.%in:name[6]%".format(os.environ["USER"], options.tag))
    if options.nosubmit: 
        exit(0)
    driver.submitOnly(job, options.outdir)
    exit(0)

# usually just for local test
driver = ROOT.EL.DirectDriver()
if options.nosubmit: 
    exit(0)
driver.submit( job, options.outdir )
