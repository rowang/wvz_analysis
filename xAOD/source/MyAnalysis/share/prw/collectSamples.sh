#!/bin/bash

> mySamples.txt 
> nSamples.txt
> existingPRW.txt

function scan () {
    all=$(ls -d $1*)
    echo $all | wc -w >> nSamples.txt
    for sample in $all; do
        BN=$(basename $sample)
        DSID=$(echo $BN | cut -d . -f 2)
        rTAG=$(echo $BN |rev| cut -d _ -f 2 |rev)
        # echo $var
        if ! [[ $(awk "/$DSID/ && /$rTAG/ { print; }" existingPRW.txt) ]]; then
            echo $BN >> $2
        fi
    done
}

scan mc16_13TeV existingPRW.txt nSamples.txt
scan ~/s3_datac/WVZ_xAOD/mc16_13TeV mySamples.txt nSamples.txt
scan ~/s3_dataf/WVZ_xAOD/mc16_13TeV mySamples.txt nSamples.txt
scan ~/ustc_02/WVZ_xAOD/mc16_13TeV mySamples.txt nSamples.txt
