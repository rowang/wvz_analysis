#!/bin/bash

# clear everything
# > list_missing_AOD.txt

allFile=(
    # 364288
    # 345705
    # 364289
    # 410218
    # 410219
    # 410220
    # 410560
    # 410217
    # 342284
    # 342285
    # 343365
    # 364290
    # 364287
    # 410503
    # 410389
    # 410011
    # 410012
    # 410013
    # 410014
    # 410025
    # 410026
    # 410080
    # 364500
    # 364501
    # 364502
    # 364503
    # 364504
    # 364505
    # 364506
    # 364506
    # 364507
    # 364508
    # 364509
    # 3641

    # run them on grid?
    410156
    410157
    410472
    410389
    410082

    410658
    410659
    410646
    410647
    410644
    410645

    412043
)

rtag=(
    9364
    10201
    10724
)


for f in ${allFile[@]}; do
    echo $f
    # all_list=`rucio list-dids --short mc16_13TeV.${f}*recon.AOD*` 
    all_list=`rucio list-dids --short mc16_13TeV.${f}*deriv.DAOD_STDM3*` 
    # all_list="mc16_13TeV:mc16_13TeV.364288.Sherpa_222_NNPDF30NNLO_llll_lowMllPtComplement.recon.AOD.e6096_e5984_s3126_r9364 mc16_13TeV:mc16_13TeV.364288.Sherpa_222_NNPDF30NNLO_llll_lowMllPtComplement.recon.AOD.e6096_s3126_r9364"
    for r in ${rtag[@]}; do
        # only single!
        # regex="mc16_13TeV:mc16_13TeV\.[0-9]+.+recon\.AOD\.e[0-9]+_s[0-9]+_r${r}"
        regex="mc16_13TeV:mc16_13TeV\.[0-9]+.+deriv\.DAOD_STDM3\.e[0-9]+_s[0-9]+_r${r}.p40[0-9]7"
        for file in $all_list; do
            if [[ $file =~ $regex  ]]; then
                echo $file | tee -a list_missing_DxAOD.txt
            fi
        done
    done
done
