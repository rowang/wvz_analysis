# WVZ_Analysis
=======================================

## Basic info:

---------------------------------------
Codes to produce ntuples are under `xAOD/Code_Rel21`. 
DNN framework is under `DNN`, which is used in bbll analysis. 

There are several GPU machine you could used to MVA study: `umt3gpu01.aglt.org`,`gpu0(1-5)atum.cern.ch`(need to login via lxplus).


## Git techs
### Initial clone 
`git clone` this repository to get all files in the local repo, 
and then `git submodule update --init --recursive` to get the submodules.

Or directly do `git clone --recurse-submodules`

### Keep track of branch for submodules(develop from here)
Then, you want to keep track of the correct branch for the submodules,

simply go to the submodule-path, do `git checkout master`, 

Or, for a different branch, `git branch -u <origin>/<branch> <branch>`

if the branch doesn't exist, create a 
branch `git checkout -b <branch> --track <origin>/<branch>`

Later, do 
`git pull` for getting files from local repo up-to-date
`git submodule update --recursive` to keep submodules up-to-date!


