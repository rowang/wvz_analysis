#ifndef MAIN
#define MAIN

#include<iostream>
#include<stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <time.h>
#include <fstream>
using namespace std;           
                               
float reduce_min(float *input_vec, long long size);

float reduce_max(float *input_vec, long long size);

double reduce_sum(float *input_vec, long long size);
  
double reduce_var(float* input_vec, float* weight_vec, long long size, double mean);
  
double reduce_cor(float* input_vec_a, float *input_vec_b, float* weight_vec, long long size, double mean_a, double mean_b);
  
void histo(float *input_vec, int *output_vec, float min, float max, long long size, int bins); 


void sort(float *input_vec, float *output_vec, int *index_count, long long size);

int count_duplicate(float* input_vec, long long size);

void scan(float* input_vec, float* output_vec, long long size, int isExclusive);

void transpose(float *input_vec, float *output_vec, int n_col, int n_row);

int search(float *input_vec1, float *input_vec2, int size1, int size2);

#endif
