#include<iostream>
#include<stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <time.h>
#include <fstream>
#include"header.h"
using namespace std;


int main(){
	long long size = 150000000;
	float *a;
	float *weight;
	int *histoBin;
	int bins=20;
	float min_value;
	float max_value;
	float temp;
	double sum_value;
	double sum_var;
	double sum_cor;
	double sum_weight;
	double mean;
	double var;
	double cor;
	float *sorted;
	int *index_count;
	float *scaned;
	clock_t start_time, end_time;
	float temp1;
	float temp2;
	float temp3;
	int numbers=0;
	int num_duplicate=0;
	int isExclusive=0;
//	ifstream in;
//	in.open("number.txt");
	a = (float *)malloc(size*sizeof(float));
	weight = (float *)malloc(size*sizeof(float));
	histoBin = (int *)malloc(bins*sizeof(int));
	//const int num_threads = 1024;
	//int num_blocks =(int)(((double)size)/(2.0*((double)num_threads)));
	//cout<<"Total blocks needed: "<<num_blocks<<endl;
	if(a==NULL){
		cout<<"error in malloc"<<endl;
		return(0);
	}
	for(int i=0;i<size;i++){
		temp = (float)rand()/(float)RAND_MAX*3.0;
		//temp1 = (float)rand()/(float)RAND_MAX*3.0;
		//temp2 = (float)rand()/(float)RAND_MAX*3.0;
		//temp3 = (float)rand()/(float)RAND_MAX*3.0;
		a[i]=temp;
	//	in>>a[i];
		weight[i]=1.0;
				        
	}
	cout<<"random max is: "<<RAND_MAX<<endl;
	cout<<"Vector initialed"<<endl;
	min_value = reduce_min(a,size);
	max_value = reduce_max(a,size);
	sum_value = reduce_sum(a,size);
	mean = sum_value/(double)size;
	sum_weight = reduce_sum(weight,size);
	sum_var = reduce_var(a, weight, size, mean);
	sum_cor = reduce_cor(a, a, weight, size, mean, mean);
	var = sum_var*(double)size/(sum_weight*((double)size-1.0));
	cor = sum_cor*(double)size/(sum_weight*((double)size-1.0));
	cout<<"min value is: "<<min_value<<endl;
	cout<<"max value is: "<<max_value<<endl;
	cout<<"mean value is: "<<sum_value/(double)size<<endl;
	cout<<"variance is: "<<var<<endl;
	cout<<"corvariance is: "<<cor<<endl;
	histo(a, histoBin, min_value, max_value, size, bins);
	for(int i=0;i<bins;i++){
		cout<<"bin contents of "<<i<<" is: "<<histoBin[i]<<endl;
	}
	sorted = (float *)malloc(size*sizeof(float));
	index_count =  (int *)malloc(size*sizeof(int));
	start_time=clock();
	
	sort(a, sorted, index_count, size);
	end_time=clock();
	scaned = (float *)malloc(size*sizeof(float));
	//start_time=clock();
	scan(sorted, scaned, size, isExclusive);
	//end_time=clock();
	int count_zero=0;
	for(int i=0;i<size;i++){
		if(sorted[i]==0){
		//	cout<<"error location is: "<<i<<endl;
			count_zero=count_zero+1;	
		}
	}
	cout<<"Total error is: "<<count_zero<<endl;
	for(int i=0;i<10000;i++){
		cout<<"id: "<<i<<" "<<scaned[i]<<endl;
	}
	cout<<"sort time cost is: "<<(double)(end_time-start_time)<<endl;
	num_duplicate=count_duplicate(sorted,size);
	cout<<"total duplicates are: "<<num_duplicate<<endl;


		
		
		        
}
