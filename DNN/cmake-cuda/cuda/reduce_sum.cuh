#ifndef REDUCE_SUM       
#define REDUCE_SUM       

#include <stdio.h>

extern "C" 

double reduce_sum(float* input_vec, long long size);
    
#endif
