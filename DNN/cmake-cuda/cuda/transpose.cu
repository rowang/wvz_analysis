#include <stdio.h>
#include <math.h>
#include <cstdlib>

__global__ void gpu_transpose(float *input_vec, float *output_vec, int n_col, int n_row){
	__shared__ float temp[16][16];
	int x=threadIdx.x;
	int y=threadIdx.y;
	int in_x=blockDim.x*blockIdx.x;
	int in_y=blockDim.y*blockIdx.y;
	if((in_x+x)<n_col&&(in_y+y)<n_row){
		temp[y][x]=input_vec[(in_y+y)*n_col+in_x+x];
	}
	__syncthreads();
	if((in_y+x)<n_row&&(in_x+y)<n_col){
		output_vec[(in_x+y)*n_row+in_y+x]=temp[x][y];
	}
}


void transpose(float *input_vec, float *output_vec, int n_col, int n_row){
	float *a;
	float *out;
	int deviceId;
	int tile_size=16;
	dim3 block_shape(tile_size,tile_size,1);
	dim3 grid_shape(ceil((float)n_col/(float)tile_size),ceil((float)n_row/(float)tile_size),1);
	cudaGetDevice(&deviceId);
	cudaMallocManaged(&a,n_col*n_row*sizeof(float));
	cudaMemcpy(a,input_vec,sizeof(input_vec),cudaMemcpyHostToDevice);
	cudaMallocManaged(&out, n_col*n_row*sizeof(float));
	cudaMemPrefetchAsync(out, sizeof(out), deviceId);
	gpu_transpose<<<grid_shape,block_shape>>>(a,out,n_col,n_row);
	cudaDeviceSynchronize();
	cudaMemcpy(output_vec,out,sizeof(out),cudaMemcpyDeviceToHost);

}
