#ifndef REDUCE_VAR       
#define REDUCE_VAR       

#include <stdio.h>

extern "C" 

double reduce_var(float* input_vec, float* weight_vec, long long size, double mean);
    
#endif
