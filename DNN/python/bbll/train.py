import torch
from torch.utils.data import DataLoader, TensorDataset
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np



            

def init_weights(m):
    if type(m) == nn.Linear:
        torch.nn.init.xavier_normal_(m.weight)
        m.bias.data.fill_(0.01)

def train(model, device, train_loader, loss_function, optimizer, epoch, num_class):
    model.train()
    average_loss=0
    batch_n=0
    for batch_idx, (sample_weight, data, target) in enumerate(train_loader):
        batch_n=batch_n+1
        data, target,sample_weight = data.to(device), target.to(device), sample_weight.to(device)
        
        optimizer.zero_grad()
        data=data.squeeze()
        output = model(data)
        #my_weight=sample_weight/sample_weight.mean()
        if(num_class == 2):
            loss_function.weight = sample_weight/sample_weight.mean()
            output=output.reshape(-1)
            loss = loss_function(output, target)
            #loss = custom_loss(output, target,my_weight)
            loss.backward()
            average_loss=average_loss+loss.item()
        else:
            target = target.long()
            loss = loss_function(output, target)
            loss = loss * sample_weight/sample_weight.mean()
            loss = loss.mean()
            loss.backward()
            average_loss=average_loss+loss.item()
        optimizer.step()
        torch.cuda.empty_cache()
        if batch_idx % 10 == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))
    print("Average loss in epoch ",epoch," is: ",average_loss/batch_n)
    return(average_loss/batch_n)

def eval_train(model,device,input_loader,loss_function, num_class):
    average_loss=0
    batch_n=0
    model.eval()
    with torch.no_grad():
        for batch_idx, (sample_weight, data, target) in enumerate(input_loader):
            batch_n=batch_n+1
            data, target,sample_weight = data.to(device), target.to(device), sample_weight.to(device)
            output = model(data)
            if(num_class == 2):
                output=output.reshape(-1)
                loss_function.weight = sample_weight/sample_weight.mean()
                loss = loss_function(output, target)
                average_loss=average_loss+loss.item()
            else:
                target = target.long()
                loss = loss_function(output, target)
                loss = loss * sample_weight/sample_weight.mean()
                average_loss=average_loss+loss.mean().item()
    return(average_loss/batch_n)


def eval_pytorch(model,device,input_loader, num_class):
    i=0
    model.eval()
    with torch.no_grad():
        for batch_idx, (data1) in enumerate(input_loader):
            score=model(data1)
            if(num_class>2):
                score = nn.functional.softmax(score, dim = 1)
            del data1
            score_np=score.data.cpu().numpy()
            if(i==0):
                output_numpy=score_np
            if(i!=0):
                output_numpy=np.vstack((output_numpy,score_np))
            i=i+1
            torch.cuda.empty_cache()
    return(output_numpy)

def train_model(model, device, TrainLoader, TestLoader, loss_fn, optimizer, max_epoch, patient, num_class):
    lowest_loss=9999
    counter=0
    best_epoch=0
    pat=patient
    loss_np=np.zeros((max_epoch,1))
    loss_test_np=np.zeros((max_epoch,1))
    for epoch in range(1, max_epoch):
        loss_epoch=train(model, device, TrainLoader,loss_fn, optimizer, epoch, num_class)
        loss_test_epoch=eval_train(model, device, TestLoader,loss_fn, num_class)
        loss_np[epoch-1]=loss_epoch
        loss_test_np[epoch-1]=loss_test_epoch
        if(loss_test_epoch<lowest_loss):
            counter=0
            best_epoch=epoch
            lowest_loss=loss_test_epoch
            torch.save(model.state_dict(),'training-temp.pt')
        else:
            counter=counter+1
        if(counter==pat):
            print("Stopping, best epoch is: ",best_epoch," lowest loss is: ",lowest_loss)
            model.load_state_dict(torch.load('training-temp.pt'))
            break
    return(loss_np,loss_test_np,best_epoch)
